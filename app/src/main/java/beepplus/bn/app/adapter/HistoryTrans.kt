package beepplus.bn.app.adapter
import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import kotlinx.android.synthetic.main.orderview_item.view.*
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class HistoryTrans(val context: Context?, val data:ArrayList<HashMap<String,String>>): RecyclerView.Adapter<HistoryTrans.ViewHolder> () {

    lateinit var mClickListener: ClickListener
    var str_symbol = "$"
    var str_date_time = ""

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }
    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.i("context_context", "11111111111111111111" + data.size);
        val layoutInflater = LayoutInflater.from(p0.context)
        val cellForRow = layoutInflater.inflate(R.layout.orderview_item, p0, false)
        return ViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        Log.i("getItemCount", "2222222222222222222222");
        return data.size;
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        var result = java.util.HashMap<String, String>()
        result = data.get(p1)

            val id: String = result.get("id").toString()
            val created_at: String = result.get("created_at").toString()
            val customer_name: String = result.get("customer_name").toString()
            val customer_mobile: String = result.get("customer_mobile").toString()

            val amount: String = result.get("amount").toString()
            val coupon_code: String = result.get("coupon_code").toString()

            val points: String = result.get("points").toString()


      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = created_at
            val formatter = DateTimeFormatter.ofPattern("MMM dd yyyy HH:mma")
            var answer: String =  current.format(formatter)
            str_date_time = answer
            Log.d("answer",answer)


        } else {
            var date = created_at
            val formatter = SimpleDateFormat("MMM dd yyyy HH:mma")
            val answer: String = date.format(formatter)
            str_date_time = answer
            Log.d("answer",answer)
        }*/

        var format = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val newDate: Date = format.parse(created_at)

        format = SimpleDateFormat("MMM dd,yyyy hh:mm:ss")
        val date = format.format(newDate)

        Log.i("date_date","----------------------------"+date)
        p0.date_txt.setText(date)


        p0.cust_name.setText(customer_name)
        p0.pro_name.setText("+673"+customer_mobile)

        p0.pro_img.setImageResource(R.drawable.app_ic)
        p0.txt_user_id.setText("# " +id)

        p0.img_r.visibility = View.INVISIBLE

        /*if(!coupon_code.equals("null")){
        }else{
            p0.txt_user_id.setText(str_symbol+" "+amount)
        }*/

        p0.amt.setText("xs"+points)

        /* if(payment_status.equals("1")){
             //p0.img_pay_icon.setImageResource(R.drawable.ic_baseline_check_circle_24)
             p0.txt_trans_amt.setTextColor(Color.parseColor("#ff669900"));
         }else{
             //p0.img_pay_icon.setImageResource(R.drawable.ic_baseline_cancel_24)
             p0.txt_trans_amt.setTextColor(Color.parseColor("#ffcc0000"));
             p0.txt_trans_title.setTextColor(Color.parseColor("#ffcc0000"));
         }*/

        Log.i("data_date","---------------" + data.size)
        Log.i("data_date_log","++++++++++++++++++++++++++" + p1)



    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to

        val date_txt = view.date_txt
        val img_r = view.img_r
        val amt = view.amt
        val txt_user_id = view.txt_user_id
        val pro_img = view.pro_img
        val cust_name = view.cust_name
        val pro_name = view.pro_name

       /* val txt1 = view.txt1
        val txt2 = view.txt2
        val txt3 = view.txt3*/

    }

}