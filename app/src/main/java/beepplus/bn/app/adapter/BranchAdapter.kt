package beepplus.bn.app.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.scanner.AddOffer
import kotlinx.android.synthetic.main.loyalty_branch_list.view.*

class BranchAdapter(val context: Context?, val data: ArrayList<HashMap<String, String>>,val array_list_bool:ArrayList<Int>): RecyclerView.Adapter<BranchAdapter.ViewHolder> () {

    lateinit var mClickListener: ClickListener
    var str_symbol = "$"
    var str_date_time = ""
    lateinit var checked: BooleanArray

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }
    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.i("context_context", "11111111111111111111" + data.size);
        val layoutInflater = LayoutInflater.from(p0.context)
        val cellForRow = layoutInflater.inflate(R.layout.loyalty_branch_list, p0, false)
        return ViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        Log.i("getItemCount", "2222222222222222222222");
        return data.size;
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        var result = java.util.HashMap<String, String>()
        result = data.get(p1)

        val id: String = result.get("name").toString()
        p0.branch_name.setText(id)

        Log.i("data_date", "---------------" + data.size)
        Log.i("data_date_log", "++++++++++++++++++++++++++" + p1)

        if(array_list_bool[p1]==1){
            p0.image_rigth.setBackgroundResource(R.drawable.ic_baseline_check_box_24)
        }else{
            p0.image_rigth.setBackgroundResource(R.drawable.ic_baseline_check_box_outline_blank_24)
        }

        p0.itemView.setOnClickListener { v->

            val int_v = array_list_bool.get(p1)

            (context as AddOffer).add_to_branches_checking(p1,int_v);




           /* val hm = data[p1]
            Log.i("position_id_assdsd", "" + hm)
            var name = hm["name"].toString()
            var id = hm["id"].toString()
            Log.i(
                "name",
                "=========================>Testing purpose<=========================$name"
            )*/
            Log.i("id", "=========================>Testing purpose<=========================$id")
            //val list_qty_arranged: List<String> = ArrayList(Arrays.asList(*hm["product_items_cart_qty"]!!.split(",").toTypedArray()))
           // (context as AddOffer).add_to_branches(name,id)

        }


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to

        val branch_name = view.branch_name
        val image_rigth = view.image_rigth

    }

}