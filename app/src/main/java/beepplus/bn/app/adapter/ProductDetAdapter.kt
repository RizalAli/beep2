package beepplus.bn.app.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.details.ManageDetails
import beepplus.bn.app.home.Home
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.manage_pro_item.view.*
import kotlinx.android.synthetic.main.offer_items.view.*
import kotlinx.android.synthetic.main.pro_item.view.*
import seller.naruvis.com.retrofitcall.APIUrl
import java.util.ArrayList
import java.util.HashMap

class ProductDetAdapter (val context: Context?, val data: ArrayList<HashMap<String, String>>): RecyclerView.Adapter<ProductDetAdapter.ViewHolder> () {

    lateinit var mClickListener: ClickListener
    var str_symbol = "BND$"
    var output_start_on = ""
    var output_end_on = ""
    var str_dollar = "$"

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }
    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.i("context_context", "11111111111111111111" + data.size);
        val layoutInflater = LayoutInflater.from(p0.context)
        val cellForRow = layoutInflater.inflate(R.layout.pro_item, p0, false)
        return ViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        Log.i("getItemCount", "2222222222222222222222");
        return data.size;
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        var result = java.util.HashMap<String, String>()
        result = data.get(p1)

        val product_name = result.get("product_name");
        val unit_name = result.get("unit_name");
        val qty = result.get("qty");
        val total_price = result.get("total_price");
        val image = result.get("image")
        val category_id = result.get("category_id")

        if(image!="" && image!=null){

            var URL = ""
            if(Home.manage_fragment_orders==1){
                URL = "https://kontenapark.beepxs.solutions/uploads/"
            }else{
                URL = "http://onebrunei.market/uploads/"
            }

            if (context != null) {
                Glide.with(context)
                    .load(URL+image)
                    .into(p0.pro_img)
            };

        }else{

            if (context != null) {
                Glide.with(context)
                    .load("https://brpatrick.beepxs.solutions/assets/images/noimage.png")
                    .into(p0.pro_img)
            };

        }


        p0.cust_name.setText(product_name)
        p0.pro_name.setText(unit_name + " " +"x" + qty)

        p0.amt.setText(str_dollar + " " + total_price)


        Log.i("data_date","---------------" + data.size)
        Log.i("data_date_log","++++++++++++++++++++++++++" + p1)

        p0.itemView.setOnClickListener { v->
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to

        val amt = view.amt
        val pro_img = view.pro_img
        val cust_name = view.cust_name
        val pro_name = view.pro_name


    }

}