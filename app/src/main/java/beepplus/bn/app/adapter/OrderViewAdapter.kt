package beepplus.bn.app.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.details.ManageDetails
import beepplus.bn.app.home.Home
import beepplus.bn.app.home.Home.Companion.manage_fragment_orders
import beepplus.bn.app.home.ui.ecomcatalogue.OrdersDetailsEcom
import beepplus.bn.app.home.ui.marketplace.OrderDetailsAct
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.offer_items.view.*
import kotlinx.android.synthetic.main.orderview_item.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class OrderViewAdapter(val context: Context?, val data:ArrayList<HashMap<String,String>>): RecyclerView.Adapter<OrderViewAdapter.ViewHolder> () {

    lateinit var mClickListener: ClickListener
    var str_symbol = "$"
    var output_start_on = ""
    var output_end_on = ""

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }
    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.i("context_context", "11111111111111111111" + data.size);
        val layoutInflater = LayoutInflater.from(p0.context)
        val cellForRow = layoutInflater.inflate(R.layout.orderview_item, p0, false)
        return ViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        Log.i("getItemCount", "2222222222222222222222");
        return data.size;
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        var result = java.util.HashMap<String, String>()
        result = data.get(p1)

        val order_id = result.get("order_id");
        val seller_id = result.get("seller_id");
        val cus_name = result.get("cus_name");
        val product_name = result.get("product_name");
        val total_qty = result.get("total_qty");
        val total_price = result.get("total_price");
        var created_at = result.get("created_at");
        var image = result.get("image");
        var tbl_no = result.get("tbl_no");

        var format = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val newDate: Date = format.parse(created_at)

        format = SimpleDateFormat("MMM dd,yyyy hh:mm a")
        val date = format.format(newDate)


        if(image!="" && image!=null){

            if(manage_fragment_orders==1){
                if (context != null) {
                    Glide.with(context)
                        .load("https://kontenapark.beepxs.solutions/assets/images/logo.png")
                        .into(p0.pro_img)
                };
            }else{
                if (context != null) {
                    Glide.with(context)
                        .load("http://onebrunei.market/assets/images/new-logo.png")
                        .into(p0.pro_img)
                };
            }

        }else{

            if(manage_fragment_orders==1){
                if (context != null) {
                    Glide.with(context)
                        .load("https://kontenapark.beepxs.solutions/assets/images/logo.png")
                        .into(p0.pro_img)
                };
            }else{
                if (context != null) {
                    Glide.with(context)
                        .load("http://onebrunei.market/assets/images/new-logo.png")
                        .into(p0.pro_img)
                };
            }
        }

        Log.i("data_date","---------------" + data.size)
        Log.i("data_date_log","++++++++++++++++++++++++++" + p1)


          p0.cust_name.setText(cus_name)
          p0.table_no.setText("Table:"+" " + tbl_no)
          p0.pro_name.setText("Items:" +" " + total_qty)
          p0.txt_user_id.setText("# " + order_id)
          p0.amt.setText(str_symbol + "" + total_price)

          p0.date_txt.setText(date)

        p0.itemView.setOnClickListener { v->

            val hm = data[p1]

            val order_id = result.get("order_id");

            /*val order_id = hm["order_id"].toString()
            val title = hm["title"].toString()
            val merchant_name = hm["merchant_name"].toString()
            val start_on = hm["start_on"].toString()
            val end_on = hm["end_on"].toString()
            val offer_branch = hm["offer_branch"].toString()
            val branch_names = hm["branch_names"].toString()
            val coupon_count = hm["coupon_count"].toString()
            val code = hm["code"].toString()
            val coupon_grabbed = hm["coupon_grabbed"].toString()
            val used_coupon = hm["used_coupon"].toString()
            val status = hm["status"].toString()
            val img = hm["img"].toString()


            val intent = Intent(context,ManageDetails::class.java)
            intent.putExtra("id",id)
            intent.putExtra("title",title)
            intent.putExtra("merchant_name",merchant_name)
            intent.putExtra("start_on",start_on)
            intent.putExtra("end_on",end_on)
            intent.putExtra("offer_branch",offer_branch)
            intent.putExtra("branch_names",branch_names)
            intent.putExtra("coupon_count",coupon_count)
            intent.putExtra("code",code)
            intent.putExtra("coupon_grabbed",coupon_grabbed)
            intent.putExtra("used_coupon",used_coupon)
            intent.putExtra("status",status)
            intent.putExtra("img",img)
            context!!.startActivity(intent)*/

            /* val intent = Intent(this@Home, LoyaltyDrawer::class.java)
             startActivity(intent)*/

            Log.i("order-------------","=========================")

            val intent = Intent(context, OrdersDetailsEcom::class.java)
            intent.putExtra("order_id",order_id)
            context!!.startActivity(intent)


        }



    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to


        val date_txt = view.date_txt

        val amt = view.amt
        val txt_user_id = view.txt_user_id
        val pro_img = view.pro_img
        val cust_name = view.cust_name
        val table_no =view.table_no
        val pro_name = view.pro_name


    }

}