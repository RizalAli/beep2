package beepplus.bn.app.adapter
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.details.TransDetails
import beepplus.bn.app.qrcode.QRcode
import kotlinx.android.synthetic.main.transcation_list.view.*

class TransAdapter(val context: Context?, val data:ArrayList<HashMap<String,String>>): RecyclerView.Adapter<TransAdapter.ViewHolder> () {

    lateinit var mClickListener: ClickListener
    var str_symbol = "BND$"

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }
    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.i("context_context", "11111111111111111111" + data.size);
        val layoutInflater = LayoutInflater.from(p0.context)
        val cellForRow = layoutInflater.inflate(R.layout.transcation_list, p0, false)
        return ViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        Log.i("getItemCount", "2222222222222222222222");
        return data.size;
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        var result = java.util.HashMap<String, String>()
        result = data.get(p1)

        val payment_status: String = result.get("payment_status").toString()
        val trans_amount: String = result.get("trans_amount").toString()
        val trans_title: String = result.get("trans_title").toString()
        val trans_note: String = result.get("trans_note").toString()
        val trans_date: String = result.get("date").toString()

        if(payment_status.equals("1")){
            //p0.img_pay_icon.setImageResource(R.drawable.ic_baseline_check_circle_24)
            p0.txt_trans_amt.setTextColor(Color.parseColor("#ff669900"));
        }else{
            //p0.img_pay_icon.setImageResource(R.drawable.ic_baseline_cancel_24)
            p0.txt_trans_amt.setTextColor(Color.parseColor("#ffcc0000"));
            p0.txt_trans_title.setTextColor(Color.parseColor("#ffcc0000"));
        }

        Log.i("data_date","---------------" + data.size)
        Log.i("data_date_log","++++++++++++++++++++++++++" + p1)

        if(p1==2){
            p0.txt_day.visibility = View.GONE
        }else{
            p0.txt_day.visibility = View.VISIBLE
        }


        p0.txt_trans_amt.setText(str_symbol + "" + trans_amount)
        p0.txt_trans_title.setText(trans_title)
        p0.txt_trans_note.setText(trans_note)

        p0.txt_day.setText(trans_date)

        p0.itemView.setOnClickListener { V->
            val intent = Intent(context, TransDetails::class.java)
            context!!.startActivity(intent)
        }


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to

        val img_pay_icon = view.img_pay_icon
        val txt_trans_amt = view.txt_trans_amt
        val txt_trans_title = view.txt_trans_title
        val txt_trans_note = view.txt_trans_note
        val txt_day = view.txt_day

    }

}