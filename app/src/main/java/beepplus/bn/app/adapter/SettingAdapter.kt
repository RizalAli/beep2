package beepplus.bn.app.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.details.TransDetails
import beepplus.bn.app.settings.BankAccount
import beepplus.bn.app.settings.ChangePassword
import beepplus.bn.app.settings.PaymentMethods
import kotlinx.android.synthetic.main.settings_list.view.*

class SettingAdapter(val context: Context?, val items_ic: Array<Int>, val items: Array<String>): RecyclerView.Adapter<SettingAdapter.ViewHolder> () {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {

        Log.i("context_context", "11111111111111111111" + items.size);

        val layoutInflater = LayoutInflater.from(p0.context)
        val cellForRow = layoutInflater.inflate(R.layout.settings_list, p0, false)
        return ViewHolder(cellForRow)


    }

    override fun getItemCount(): Int {

        Log.i("1111111111111", "2222222222222222222222");

        return items.size;


    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {


        p0.text_view.setText(items.get(p1)) //order_delivereddate
        p0.img_left_ic.setImageResource(items_ic.get(p1))


        p0.itemView.setOnClickListener { v ->

            val str_action = items.get(p1)

            Log.i("str_action" , "" + str_action.toString())

            if(str_action.equals("Payment Methods")){
                val intent = Intent(context, PaymentMethods::class.java)
                context!!.startActivity(intent)
            }else if(str_action.equals("Bank Accounts")){
                val intent = Intent(context, BankAccount::class.java)
                context!!.startActivity(intent)
            }else if(str_action.equals("Change Password")){
                val intent = Intent(context, ChangePassword::class.java)
                context!!.startActivity(intent)
            } //  == == == if

            //(context as Dashboardc).navi_views(str_action)

        }


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to
        val img_left_ic = view.img_left_ic
        val text_view = view.text_title


    }
}