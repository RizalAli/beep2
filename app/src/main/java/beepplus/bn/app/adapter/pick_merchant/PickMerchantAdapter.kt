package beepplus.bn.app.adapter.pick_merchant
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.home.pick_merchant.PickMerchant
import kotlinx.android.synthetic.main.merchant_list_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class PickMerchantAdapter(val context: Context?, val data: ArrayList<HashMap<String, String>>): RecyclerView.Adapter<PickMerchantAdapter.ViewHolder> () {

    lateinit var mClickListener: ClickListener
    var str_symbol = "$"
    var str_date_time = ""

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }
    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.i("context_context", "11111111111111111111" + data.size);
        val layoutInflater = LayoutInflater.from(p0.context)
        val cellForRow = layoutInflater.inflate(R.layout.merchant_list_item, p0, false)
        return ViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        Log.i("getItemCount", "2222222222222222222222");
        return data.size;
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        var result = java.util.HashMap<String, String>()
        result = data.get(p1)

        val created_at: String = result.get("created_at").toString()
        val name: String = result.get("name").toString()
        val email_id: String = result.get("email_id").toString()
        val mobile_no: String = result.get("mobile_no").toString()

        var format = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val newDate: Date = format.parse(created_at)

        format = SimpleDateFormat("MMM dd,yyyy hh:mm:ss")
        val date = format.format(newDate)

        Log.i("date_date", "----------------------------" + date)
        p0.date_txt.setText(date)


        p0.m_name.setText(name)
        p0.m_email.setText(email_id)
        p0.m_mobile.setText("+673 " + mobile_no)


        Log.i("data_date", "---------------" + data.size)
        Log.i("data_date_log", "++++++++++++++++++++++++++" + p1)

        p0.itemView.setOnClickListener { v->

            val hm: HashMap<String, String> = data.get(p1)
            var merchant_id  = hm["id"].toString()

            (context as PickMerchant).update_merchant(merchant_id)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to

        val date_txt = view.date_txt

        val m_name = view.m_name
        val m_mobile = view.m_mobile
        val m_email = view.m_email


    }

}