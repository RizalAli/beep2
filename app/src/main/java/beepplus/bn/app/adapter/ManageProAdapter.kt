package beepplus.bn.app.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.details.ManageDetails
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.manage_pro_item.view.*
import kotlinx.android.synthetic.main.offer_items.view.*
import java.util.ArrayList
import java.util.HashMap

class ManageProAdapter (val context: Context?, val data: ArrayList<HashMap<String, String>>): RecyclerView.Adapter<ManageProAdapter.ViewHolder> () {

    lateinit var mClickListener: ClickListener
    var str_symbol = "BND$"
    var output_start_on = ""
    var output_end_on = ""
    var str_dollar = "$"
    var str_checker = 0

    fun setOnItemClickListener(aClickListener: ClickListener) {
        mClickListener = aClickListener
    }
    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.i("context_context", "11111111111111111111" + data.size);
        val layoutInflater = LayoutInflater.from(p0.context)
        val cellForRow = layoutInflater.inflate(R.layout.manage_pro_item, p0, false)
        return ViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        Log.i("getItemCount", "2222222222222222222222");
        return data.size;
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        var result = java.util.HashMap<String, String>()
        result = data.get(p1)


        val id = result.get("id");
        val name = result.get("name");
        val image = result.get("image");
        val opt_price = result.get("opt_price");
        val category_id = result.get("category_id")

        if(image!="" && image!=null){

            /*if (context != null) {
                Glide.with(context)
                    .load("https://kontenapark.beepxs.solutions/uploads/"+image)
                    .into(p0.img_pro)
            };*/

            if (context != null) {
                Glide.with(context)
                    .load("http://onebrunei.market/uploads/"+image)
                    .into(p0.img_pro)
            };

        }else{

            if (context != null) {
                Glide.with(context)
                    .load("https://brpatrick.beepxs.solutions/assets/images/noimage.png")
                    .into(p0.img_pro)
            };

        }

       /* if(p1==0){

            if(category_id=="3"){
                p0.txt_cat.setText("Burger")
            }else{
                p0.txt_cat.setText("Drinks")
            }
            p0.txt_cat.visibility = View.VISIBLE
        }else{
            val str_category_id = data.get(p1-1).get("category_id")

            if(str_category_id!=category_id){

                if(category_id=="3"){
                    p0.txt_cat.setText("Burger")
                }else{
                    p0.txt_cat.setText("Drinks")
                }
                p0.txt_cat.visibility = View.VISIBLE
            }else{
                p0.txt_cat.visibility = View.GONE
            }

        }*/


        if(category_id=="3"){

            if(p1==0){
                str_checker = 1
                p0.txt_cat.setText("Burger")
                p0.rel_cat.visibility = View.VISIBLE
            }else{
                if(str_checker==1){
                    p0.txt_cat.setText("")
                    p0.rel_cat.visibility = View.GONE
                }else{
                    str_checker = 1
                    p0.txt_cat.setText("Burger")
                }
            }
        }else if(category_id=="4"){

            if(p1==0){
                str_checker = 2
                p0.txt_cat.setText("Drinks")
                p0.rel_cat.visibility = View.VISIBLE
            }else{

                if(str_checker==2){
                    p0.txt_cat.setText("")
                    p0.rel_cat.visibility = View.GONE
                }else{
                    str_checker = 2
                    p0.txt_cat.setText("Drinks")
                    p0.rel_cat.visibility = View.VISIBLE
                }

            }
            //p0.txt_cat.setText("Drinks")

        }

        /*if(category_id!="" && category_id!=null){
            if(category_id=="3"){
                p0.txt_cat.setText("Burger")
            }else if(category_id=="4"){
                p0.txt_cat.setText("Drinks")
            }else{
                p0.txt_cat.setText("Default category")
            }
        }else{
            p0.txt_cat.setText("Default category")
        }*/


        p0.txt_pro_name.setText(name)
        p0.txt_price.setText(str_dollar + " " + opt_price)


        Log.i("data_date","---------------" + data.size)
        Log.i("data_date_log","++++++++++++++++++++++++++" + p1)

        p0.itemView.setOnClickListener { v->
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to

        val rel_cat = view.rel_cat
        val txt_cat = view.txt_cat
        val img_pro = view.img_pro
        val txt_pro_name = view.txt_pro_name
        val txt_price = view.txt_price


    }

}