package beepplus.bn.app.adapter
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.details.ManageDetails
import beepplus.bn.app.home.ui.loyalty.ManageOfferDetails
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.offer_items.view.*
import java.util.*

class ManageAdapter(val context: Context?, val data:ArrayList<HashMap<String,String>>): RecyclerView.Adapter<ManageAdapter.ViewHolder> () {

    lateinit var mClickListener: ClickListener

    var str_symbol = "BND$"
    var output_start_on = ""
    var output_end_on = ""
    lateinit var fragmentClass: Class<*>
    var fragment: Fragment? = null


    fun setOnItemClickListener(aClickListener: ManageAdapter.ClickListener) {
        mClickListener = aClickListener
    }
    interface ClickListener {
        fun onClick(pos: Int, aView: View)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        Log.i("context_context", "11111111111111111111" + data.size);
        val layoutInflater = LayoutInflater.from(p0.context)
        val cellForRow = layoutInflater.inflate(R.layout.offer_items, p0, false)
        return ViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        Log.i("getItemCount", "2222222222222222222222");
        return data.size;
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        var result = java.util.HashMap<String, String>()
        result = data.get(p1)

        val id = result.get("id");
        val title = result.get("title");
        val merchant_name = result.get("merchant_name");
        val start_on = result.get("start_on");
        val end_on = result.get("end_on");
        val offer_branch = result.get("offer_branch");
        var branch_names = result.get("branch_names");
        val coupon_count = result.get("coupon_count");
        val code = result.get("code");
        val coupon_grabbed = result.get("coupon_grabbed");
        val used_coupon = result.get("used_coupon");
        val status = result.get("status");
        val img = result.get("img");

        if(img!="" && img!=null){

            if (context != null) {
                Glide.with(context)
                    .load("https://adb.brpaccess.com/uploads/"+img)
                    .into(p0.img_merchant)
            };

        }else{

            if (context != null) {
                Glide.with(context)
                    .load("https://adb.brpaccess.com/assets/images/noimage.png")
                    .into(p0.img_merchant)
            };

        }



    /*    if(status=="0"){
            p0.status.setText("Pending")
            p0.status.setTextColor(Color.parseColor("#FF9800"));
        }else  if(status=="8"){
            p0.status.setText("Expired")
            p0.status.setTextColor(Color.parseColor("#E91E63"));
        }else if(status=="9"){
            p0.status.setText("Rejected")
            p0.status.setTextColor(Color.parseColor("#ffcc0000"));
        }else{
            p0.status.setText("Ongoing")
            p0.status.setTextColor(Color.parseColor("#ff669900"));
        }*/



                /** DEBUG dateStr = '2006-04-16T04:00:00Z' **/
              /*  val pattern = "dd-MM-yyyy"
                val formatter = SimpleDateFormat(pattern, Locale.ENGLISH)
                val mDate = formatter.parse(start_on) // this never ends while debugging
                Log.e("mDate-=-=-=-=-==--", mDate.toString())
*/
               // val input = "2012/01/20 12:05:10.321"
                /*var sdf: SimpleDateFormat? = SimpleDateFormat(
                    "EE MMM dd HH:mm:ss z yyyy",
                    Locale.ENGLISH
                )*/
               /* val inputFormatter: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val date: Date = inputFormatter.parse(output_start_on)

                val outputFormatter: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy")
                output_start_on = outputFormatter.format(date).toString()
                Log.i("mDate-=-=-=-=-==--", output_start_on.toString())*/


        //Log.i("branch_names-=-=-=-=-==--", branch_names.toString())
        if(branch_names.isNullOrEmpty()){
            branch_names = "All Branches"
        }

        p0.txt_merchant.setText(merchant_name)
        p0.txt_offer.setText(title)

        p0.coupon_code.setText(code);
        p0.start_on.setText(start_on)
        p0.end_on.setText(end_on)


        Log.i("data_date","---------------" + data.size)
        Log.i("data_date_log","++++++++++++++++++++++++++" + p1)

      /*  if(p1==2){
            p0.txt_day.visibility = View.GONE
        }else{
            p0.txt_day.visibility = View.VISIBLE
        }*/


      /*  p0.txt_trans_amt.setText(str_symbol + "" + trans_amount)
        p0.txt_trans_title.setText(trans_title)
        p0.txt_trans_note.setText(trans_note)

        p0.txt_day.setText(trans_date)*/

        p0.itemView.setOnClickListener { v->

            val hm = data[p1]

            val id = hm["id"].toString()
            val title = hm["title"].toString()
            val merchant_name = hm["merchant_name"].toString()
            val start_on = hm["start_on"].toString()
            val end_on = hm["end_on"].toString()
            val offer_branch = hm["offer_branch"].toString()
            val branch_names = hm["branch_names"].toString()
            val coupon_count = hm["coupon_count"].toString()
            val code = hm["code"].toString()
            val coupon_grabbed = hm["coupon_grabbed"].toString()
            val used_coupon = hm["used_coupon"].toString()
            val status = hm["status"].toString()
            val img = hm["img"].toString()


            val intent = Intent(context, ManageDetails::class.java)
            intent.putExtra("id",id)
            intent.putExtra("title",title)
            intent.putExtra("merchant_name",merchant_name)
            intent.putExtra("start_on",start_on)
            intent.putExtra("end_on",end_on)
            intent.putExtra("offer_branch",offer_branch)
            intent.putExtra("branch_names",branch_names)
            intent.putExtra("coupon_count",coupon_count)
            intent.putExtra("code",code)
            intent.putExtra("coupon_grabbed",coupon_grabbed)
            intent.putExtra("used_coupon",used_coupon)
            intent.putExtra("status",status)
            intent.putExtra("img",img)
            context!!.startActivity(intent)

           /* val intent = Intent(this@Home, LoyaltyDrawer::class.java)
            startActivity(intent)*/
           // fragmentClass_Menu(1)

            //mClickListener.onClick(p1,v);
        }



    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each animal to

        val img_merchant = view.img_merchant
        val txt_merchant = view.txt_merchant
        val txt_offer = view.txt_offer

        val coupon_code = view.coupon_code
        val start_on = view.start_on
        val end_on = view.end_on


    }

    fun fragmentClass_Menu(frag_check: Int){

        if(frag_check==1){
            fragmentClass = ManageOfferDetails::class.java
        }

        try {
            fragment = fragmentClass.newInstance() as Fragment
        } catch (e: Exception) {
            e.printStackTrace()
        }

        Log.i("Testing_nooooo","nooooooooo")

        // Insert the fragment by replacing any existing fragment
        /*val fragmentManager: FragmentManager = context.supportFragmentManager
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment!!).commit()
        }*/

    }

}