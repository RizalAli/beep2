package beepplus.bn.app.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import beepplus.bn.app.home.ui.loyalty.managefragment.FragmentExpired;
import beepplus.bn.app.home.ui.loyalty.managefragment.FragmentOngoing;
import beepplus.bn.app.home.ui.loyalty.managefragment.FragmentPending;

public class Pager extends FragmentStatePagerAdapter {

    int tabCount;

    public Pager(FragmentManager fragmentManager, int tabCount) {
        super(fragmentManager);
        this.tabCount= tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragmentOngoing todayObj = new FragmentOngoing();
                return todayObj;
            case 1:
                FragmentPending thisWeekObj = new FragmentPending();
                return thisWeekObj;
            case 2:
                FragmentExpired thisMonthObj = new FragmentExpired();
                return thisMonthObj;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }


    public void addFragment(FragmentOngoing today, String today1) {
    }
    public void addFragment(FragmentPending reminders, String reminders1) {
    }
    public void addFragment(FragmentExpired settings, String settings1) {
    }
}
