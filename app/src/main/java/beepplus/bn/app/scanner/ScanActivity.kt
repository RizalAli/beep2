package beepplus.bn.app.scanner

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.text.HtmlCompat
import beepplus.bn.app.R
import beepplus.bn.app.sql.DatabaseHandler
import com.google.zxing.integration.android.IntentIntegrator
import com.naruvis.ecom.pojo.AccessCusResponse
import com.naruvis.ecom.pojo.ClaimResponse
import com.naruvis.ecom.pojo.ScanResponse
import kotlinx.android.synthetic.main.content_scan_details.*
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Query
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl

class ScanActivity : AppCompatActivity(), View.OnClickListener {
    //qr code scanner object
    private var qrScan: IntentIntegrator? = null
    var bundle: Bundle? = null
    var str_from = "";
    var url = "";
    var str_mem = ""
    var cust_id = ""
    var str_coupon_code = ""
    var success_ = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)

        bundle = getIntent().getExtras();

        if (bundle != null) {
            str_from = bundle!!.getString("from").toString();
            Log.i("Testing_str_from1","str_from-------------------------" + str_from)
        }else {
            Log.i("Testing_str_from2","str_from-------------------------" + str_from)

        }

        val toolbar_scan: Toolbar = findViewById(R.id.toolbar_scan)
        setSupportActionBar(toolbar_scan)
        //toolbar_scan.setTitle("Coupon Details".toString())
        toolbar_scan.setNavigationIcon(R.drawable.ic_baseline_arrow_small_white_24)
        toolbar_scan.setNavigationOnClickListener { v->
            onBackPressed()
        }

        val isMem_His = DatabaseHandler(applicationContext)
        val mem = isMem_His.isMem_His
        val sb_mem = StringBuilder()
        sb_mem.append("")
        sb_mem.append(mem)
        str_mem = sb_mem.toString()

        Log.i("str_mem_str_mem",str_mem);

        if(str_from.equals("1")){
            url = "https://brpatrick.beepxs.solutions/merchant/addoffer?merchant_id=2"
            webview_load1.visibility = View.VISIBLE
            rel_values.visibility = View.GONE

        }else if(str_from.equals("2")){
            getSupportActionBar()!!.setTitle("MANAGE OFFER")
            url = "https://brpatrick.beepxs.solutions/merchant/viewoffer?merchant_id="+str_mem
            webview_load1.visibility = View.VISIBLE
            rel_values.visibility = View.GONE

        }else{
            getSupportActionBar()!!.setTitle("COUPON DETAILS")
            //toolbar_scan.setTitle("Coupon Details")
            webview_load1.visibility = View.GONE
            qrScan = IntentIntegrator(this)
            qrScan!!.initiateScan()
            rel_values.visibility = View.VISIBLE
        } // if

        //intializing scan object

        btn_ok.setOnClickListener { v->
            onBackPressed()
        }
        btn_cancel.setOnClickListener { v->
            onBackPressed()
        }
        btn_claim.setOnClickListener { v->
            //onBackPressed()
            var amount = Offer_amount_text.text.toString();

            if(amount!="" && amount!="0"){
                Offer_amount_input.error = null

                if(success_==1){
                    claimed(amount,cust_id,str_mem,str_coupon_code)

                }else{
                    onBackPressed()
                }

            }else{
                Offer_amount_input.error = "Enter the amount"
            }

               // claimed()
        }



        // Get the web view settings instance
        val settings = webview_load1.settings

        // Enable java script in web view
        settings.javaScriptEnabled = true

        // Enable and setup web view cache
        settings.setAppCacheEnabled(true)
        settings.cacheMode = WebSettings.LOAD_DEFAULT
        //settings.setAppCachePath(cacheDir.path)


        // Enable zooming in web view
        settings.setSupportZoom(true)
        settings.builtInZoomControls = true
        settings.displayZoomControls = true


        // Zoom web view text
        settings.textZoom = 125


        // Enable disable images in web view
        settings.blockNetworkImage = false
        // Whether the WebView should load image resources
        settings.loadsImagesAutomatically = true


        // More web view settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            settings.safeBrowsingEnabled = true  // api 26
            settings.setPluginState(WebSettings.PluginState.ON);
        }
        //settings.pluginState = WebSettings.PluginState.ON
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.mediaPlaybackRequiresUserGesture = false
        settings.setPluginState(WebSettings.PluginState.ON);

        // More optional settings, you can enable it by yourself
        settings.domStorageEnabled = true
        settings.setSupportMultipleWindows(true)
        settings.loadWithOverviewMode = true
        settings.allowContentAccess = true
        settings.setGeolocationEnabled(true)
        settings.allowUniversalAccessFromFileURLs = true
        settings.allowFileAccess = true


        // WebView settings
        webview_load1.fitsSystemWindows = true


        /*
            if SDK version is greater of 19 then activate hardware acceleration
            otherwise activate software acceleration
        */
        /*
        if SDK version is greater of 19 then activate hardware acceleration
        otherwise activate software acceleration
    */
        webview_load1.setLayerType(View.LAYER_TYPE_HARDWARE, null)


        // Set web view client
        webview_load1.webViewClient = object: WebViewClient(){
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                // Page loading started
                // Do something
//                context?.toast("Page loading.")

                // progress. visibility = View.VISIBLE

                // Enable disable back forward button
                //button_back.isEnabled = web_view.canGoBack()
                //button_forward.isEnabled = web_view.canGoForward()
            }

            override fun onPageFinished(view: WebView, url: String) {
                // Page loading finished
                // Display the loaded page title in a toast message
//              view.destroy()
//                context!!.toast("Page loaded: ${view.title}")

                //  progress. visibility = View.GONE

                // Enable disable back forward button
                //button_back.isEnabled = web_view.canGoBack()
                //button_forward.isEnabled = web_view.canGoForward()
            }
        }

        Log.i("Tested_mobile_network","oooooooo" + url)

        webview_load1.loadUrl(url)

        webview_load1.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action === KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_BACK && webview_load1.canGoBack()) {  //表示按返回键
                    webview_load1.goBack() //后退
                    //webview.goForward();//前进
                    return@OnKeyListener true //已处理
                }
            }
            false
        })

    }

    //Getting the scan results
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        val result =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            //if qrcode has nothing in it
            if (result.contents == null) {
            //    user_t.visibility = View.GONE
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show()
                onBackPressed()
            } else {
                //if qr contains data
                try {
                    //converting the data to json
                //    val obj = JSONObject(result.contents)
                    //setting values to textviews
                    //textViewName.setText(obj.getString("name"));
                    //textViewAddress.setText(obj.getString("address"));



                    val str_result = result.contents
                    val currentString = str_result
                    val separated: Array<String> = currentString.split("-").toTypedArray()
                   /* val separated =
                        currentString.split("-".toRegex()).toTypedArray()*/

                    if(separated[0].toString().trim()=="ACCESS"){
                        var str_customer_id = separated[1].toString().trim()
                        Log.i("str_customer_id", str_customer_id);
                //        user_t.visibility = View.GONE
                        str_coupon_code = ""
                        access_scanned(str_customer_id)
                    }else{
                        str_coupon_code = str_result
                        Log.i("coupon_code_done","" + "https://adb.brpaccess.com/api/couponvalid"+"?merchant_id="+str_mem+"&coupon_code="+str_coupon_code)
                        coupon_scanned(str_result)
                    }

                    Log.i("str_result_str_result",str_result);
                    //Toast.makeText(this, result.contents, Toast.LENGTH_LONG).show()

                } catch (e: JSONException) {
                    e.printStackTrace()
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
             //       user_t.visibility = View.GONE
                    Toast.makeText(this, result.contents, Toast.LENGTH_LONG).show()
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onClick(view: View) {
        //initiating the qr code scan
        qrScan!!.initiateScan()
    }


    fun coupon_scanned(coupon_code:String){
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("coupon_code_done","" + "https://adb.brpaccess.com/api/couponvalid"+"?merchant_id="+str_mem+"&coupon_code="+coupon_code)

        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        //var mem_id = "3"

        Log.i("aaaaaaaaaaaaaaaaaaaaa","" + APIUrl.BASE_BR_ACCESS_URL+"?")

        val service = retrofit.create(APIServices::class.java)
        val call = service.getCouponValid(str_mem,coupon_code)

        call.enqueue(object : Callback<ScanResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<ScanResponse>, response: Response<ScanResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                success_ = str_success;

                if(str_success==1){

                    val success_msg = response.body()!!.success_msg
                    val results = response.body()!!.data

                    for (i in 0 until results.size){
                        val jsonObject = results.get(i)

                        val name = jsonObject.name;
                        val mobile_1 = jsonObject.mobile_1;
                        val email = jsonObject.email;

                        val loyality_amount = jsonObject.loyality_amount;
                        val loyality_point = jsonObject.loyality_point;
                        var scan_msg = jsonObject.scan_msg;
                        cust_id = jsonObject.id;

                        //        user_t.visibility = View.VISIBLE
                        txt_customer_points_hint.setText(scan_msg)
                        Log.i("coupon_jsonObject"," 123_456_789se " + coupon_code)
                        txt_user.setText(name)
                        txt_mobile.setText("+673"+" "+mobile_1)
                        txt_email.setText(email)
                        rel_user.visibility = View.VISIBLE
                    }


                    txt_values.setText(HtmlCompat.fromHtml(success_msg, 0))


                    Log.i("str_success","123_456_789se" + success_msg)


                    progressDialog.dismiss()


                }else if(str_error==1){

                    val err_msg = response.body()!!.err_msg
                    val err_code = response.body()!!.err_code

                    txt_values.setText(HtmlCompat.fromHtml(err_msg, 0))
                    rel_user.visibility = View.GONE
                    //       user_t.visibility = View.GONE
                    progressDialog.dismiss()

                }// =================================================== str_success

                progressDialog.dismiss()

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<ScanResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()
                rel_user.visibility = View.GONE
                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------

    fun claimed(amount:String,cust_id:String,str_mem:String,str_coupon_code:String){

        Log.i("done_aaaaaaaaaa","" + "https://adb.brpaccess.com/api/amountinsert"+"?amount="+amount+"&mem_id="+cust_id+"&merchant_id="+str_mem+
                "&coupon_code="+str_coupon_code)

        // ======================================================== MEMBERACTION ==================================================================

        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        //var mem_id = "3"

//https://adb.brpaccess.com/api/amountinsert
        Log.i("donea","" + "https://adb.brpaccess.com/api/amountinsert"+"?amount="+amount+"&mem_id="+cust_id+"&merchant_id="+str_mem+
        "&coupon_code="+str_coupon_code)

        val service = retrofit.create(APIServices::class.java)
        val call = service.getClaimed(amount,cust_id,str_mem,str_coupon_code)


        call.enqueue(object : Callback<ClaimResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<ClaimResponse>, response: Response<ClaimResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    txt_points_added.setText(amount+" "+"xs points added")
                    getSupportActionBar()!!.setTitle("XS POINTS")
                    btn_ok.visibility = View.VISIBLE

                    rel_edit_claim.visibility = View.GONE
                    rel_success.visibility = View.VISIBLE
                    //submitted_success()
                    progressDialog.dismiss()


                }else if(str_error==1){

                    btn_ok.visibility = View.GONE
                    rel_edit_claim.visibility = View.VISIBLE
                    rel_success.visibility = View.GONE
                    progressDialog.dismiss()

                }// =================================================== str_success

                progressDialog.dismiss()

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<ClaimResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                rel_edit_claim.visibility = View.VISIBLE
                rel_success.visibility = View.GONE

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------

    fun access_scanned(str_mem_id:String){
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("wewewewewewe",""+"https://adb.brpaccess.com/api/customer"+"?mem_id="+str_mem_id)
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        //var mem_id = "3" customer


        Log.i("aaaaaaaaaaaaaaaaaaaaa","" + APIUrl.BASE_BR_ACCESS_URL+"?")

        val service = retrofit.create(APIServices::class.java)
        val call = service.getAcessCustomer(str_mem_id)


        call.enqueue(object : Callback<AccessCusResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<AccessCusResponse>, response: Response<AccessCusResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                    val results = response.body()!!.data


                        val name = results.name;
                        val mobile_1 = results.mobile_1;
                        val email = results.email;

                        val loyality_amount = results.loyality_amount;
                        val loyality_point = results.loyality_point;
                        cust_id = results.id;



                success_ = 1;



                        var scan_msg = results.scan_msg;

                //        user_t.visibility = View.VISIBLE
                        txt_customer_points_hint.setText(scan_msg)
                        //Log.i("coupon_jsonObject"," 123_456_789se " + coupon_code)
                        //txt_user.setText(name +"\n" +email)

                        txt_user.setText(name)
                        txt_mobile.setText("+673"+" "+mobile_1)
                        txt_email.setText(email)


                        txt_values.visibility = View.GONE

                    //txt_values.setText(HtmlCompat.fromHtml(success_msg, 0))

                    //Log.i("str_success","123_456_789se" + success_msg)

                    progressDialog.dismiss()

                progressDialog.dismiss()

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<AccessCusResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp reque

    fun submitted_success() {
        val alertDialog = AlertDialog.Builder(this@ScanActivity)
        alertDialog.setTitle("CLAIM")
        alertDialog.setMessage("Your claim submitted successfully.")
        alertDialog.setPositiveButton("OK") { dialog, id ->
            onBackPressed()
        }
        /*   alertDialog.setNegativeButton("Not Now") { dialog, id -> dialog.dismiss() }*/
        alertDialog.show()
    }
}