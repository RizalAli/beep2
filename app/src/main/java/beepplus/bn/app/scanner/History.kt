package beepplus.bn.app.scanner

import android.annotation.SuppressLint
import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import beepplus.bn.app.R
import beepplus.bn.app.adapter.HistoryTrans
import beepplus.bn.app.adapter.ManageAdapter
import beepplus.bn.app.sql.DatabaseHandler
import com.naruvis.ecom.pojo.HistoryResponse
import com.naruvis.ecom.pojo.ManageOffResponse
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.activity_history.toolbar
import kotlinx.android.synthetic.main.activity_manage_order.*
import kotlinx.android.synthetic.main.content_history.*
import kotlinx.android.synthetic.main.content_history.recycler_his
import kotlinx.android.synthetic.main.content_manage_offer.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl

class History : AppCompatActivity() {

    private var mLayoutManager: LinearLayoutManager? = null
    var str_mem = "";
    var str_from_date = ""
    var str_to_date = ""
    var str_status = "1"
    lateinit var historyTrans: HistoryTrans
    var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        toolbar.setTitle("Transaction History")

        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_small_white_24)
        toolbar.setNavigationOnClickListener { v->
            onBackPressed()
        }

        val isMem_His = DatabaseHandler(applicationContext)
        val mem = isMem_His.isMem_His
        val sb_mem = StringBuilder()
        sb_mem.append("")
        sb_mem.append(mem)
        str_mem = sb_mem.toString()

        Log.i("str_mem_str_mem",str_mem)


        mLayoutManager = LinearLayoutManager(this)
        recycler_his!!.layoutManager = LinearLayoutManager(this@History, LinearLayoutManager.VERTICAL, false)
        recycler_his!!.addItemDecoration(DividerItemDecoration(this@History, LinearLayoutManager.VERTICAL))
        recycler_his!!.layoutManager = mLayoutManager

        history_loaded(str_mem)


    }

    fun history_loaded(mem_id:String){
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("manage_loaded"," 123_456_789se " + mem_id)
        Log.i("manage_loaded"," 123_456_789se " + "https://brpatrick.beepxs.solutions/api/manageoffers?merchant_id="+mem_id)


        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getHistory(mem_id,str_from_date,str_to_date,str_status)


        call.enqueue(object : Callback<HistoryResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<HistoryResponse>, response: Response<HistoryResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    //val data = response.body()!!.data
                    val results = response.body()!!.data.results

                    Log.i("coupon_jsonObject"," 123_456_789se " + results)
                    for (i in 0 until results.size){
                        val jsonObject = results.get(i)
                        val created_at = jsonObject.created_at;
                        val customer_name = jsonObject.customer_name;
                        val customer_mobile = jsonObject.customer_mobile;

                        val coupon_code = jsonObject.coupon_code;
                        val amount = jsonObject.amount;

                        val points = jsonObject.points;


                        val map = java.util.HashMap<String, String>()
                        map["created_at"] = created_at
                        map["customer_name"] = customer_name
                        map["customer_mobile"] = customer_mobile

                        map["coupon_code"] = coupon_code
                        map["amount"] = amount

                        map["points"] = points

                        arrayListTrans.add(map)

                        //arrayListTrans.add()

                        Log.i("coupon_jsonObject"," 123_456_789se " + coupon_code)

                    }


                    Log.i("data_data"," 123_456_789se " + results)

                    progressDialog.dismiss()

                    historyTrans = HistoryTrans(this@History ,arrayListTrans)
                    recycler_his.adapter = historyTrans


                }else if(str_error==1){

                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<HistoryResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------
}