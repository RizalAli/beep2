package beepplus.bn.app.scanner

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.adapter.BranchAdapter
import beepplus.bn.app.auth_screen.LoginAuth
import beepplus.bn.app.common.FilePath
import beepplus.bn.app.sql.DatabaseHandler
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.naruvis.ecom.pojo.MerchantBranchResponse
import com.naruvis.ecom.pojo.UploadResponse
import kotlinx.android.synthetic.main.content_add_offer.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*


class AddOffer : AppCompatActivity() {

    var PERMISSION_CODE = 1000;
    var IMAGE_CAPTURE_CODE = 1001 // id_userprof_dateOfBirth_txtVw!!.text
    var IMAGE_PICK_CODE = 1000;
    //var PERMISSION_IMAGE_CODE = 1001;
    var image_uri: Uri? = null
    var alertDialog: AlertDialog? = null
    var mYear = 0
    var mMonth = 0
    var mDay = 0
    var str_radio = "1"
    var str_type = 0
    var mediaPath = ""
    var str_date_checking = ""
    var offer_branch_str = ""
    var offer_branch_list = java.util.ArrayList<String>()
    var offer_branch_list_id = java.util.ArrayList<String>()
    var offer_branch_list_string = java.util.ArrayList<String>()
    lateinit var recycler_branch_list: RecyclerView
    lateinit var mLayoutManager: LinearLayoutManager
    var bitmap: Bitmap? = null
    var str_mem = "";
    var is_image : InputStream ? = null
    var imageBytes:ByteArray? = null
    lateinit var branchAdapter: BranchAdapter
    var arrayListBranches = java.util.ArrayList<java.util.HashMap<String, String>>()
    var array_list_bool = java.util.ArrayList<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_add_offer)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

       toolbar.setTitle("ADD OFFER")

       toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_small_white_24)
       toolbar.setNavigationOnClickListener { v->
           onBackPressed()
       }


        val isMem_His = DatabaseHandler(applicationContext)
        val mem = isMem_His.isMem_His
        val sb_mem = StringBuilder()
        sb_mem.append("")
        sb_mem.append(mem)
        str_mem = sb_mem.toString()

        Log.i("str_mem_str_mem", str_mem)

        btn_upload.setOnClickListener {

            showCustomDialog();

        } // btn_upload

        // Get radio group selected item using on checked change listener
        radioGroup.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                val radio: RadioButton = findViewById(checkedId)

                if (radio.text == "Percentage(%)") {

                    //str_radio =R.string.Percentage.toString()

                    str_radio = "1"

                } else if (radio.text == "Amount($)") {

                    //str_radio =R.string.Amount_offer.toString()

                    str_radio = "2"

                } else if (radio.text == "Buy X & Get X") {

                    // str_radio =R.string.buy_get.toString()

                    str_radio = "3"

                } // if

                Log.i("str_345", "+++++" + str_radio)

                Toast.makeText(
                    applicationContext, " On checked change : ${radio.text}",
                    Toast.LENGTH_SHORT
                ).show()

            })

        radioGroup_offers.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                val radio1: RadioButton = findViewById(checkedId)

                if (radio1.text == "Unlimited") {

                    //str_radio =R.string.Percentage.toString()
                    No_of_coupon_input.visibility = View.GONE
                    No_of_coupon_text.visibility = View.GONE
                    str_type = 0

                } else if (radio1.text == "Limited") {

                    Log.i("str_radio-----", "+++++" + "testing")

                    //str_radio =R.string.Amount_offer.toString()
                    No_of_coupon_input.visibility = View.VISIBLE
                    No_of_coupon_text.visibility = View.VISIBLE
                    str_type = 1

                } // if

                Log.i("str_radio-----", "+++++" + str_radio)

                Toast.makeText(
                    applicationContext, " On checked change : ${radio1.text}",
                    Toast.LENGTH_SHORT
                ).show()

            })


        Offer_For_input.setOnClickListener{ v->
            Log.i("Offer_For_input_", "-------------------------------" + Offer_For_input)
            showBranchDialog()
        }
        Offer_For_text.setOnClickListener{ v->
            Log.i("Offer_For_input_", "-------------------------------" + Offer_For_input)
            showBranchDialog()
        }


        Start_ON_text.setOnClickListener { v->

            Log.i("Start_on_text", "=========================")

            val c = Calendar.getInstance()
            mYear = c[Calendar.YEAR]
            mMonth = c[Calendar.MONTH]
            mDay = c[Calendar.DAY_OF_MONTH]
            val datePickerDialog = DatePickerDialog(
                this@AddOffer,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    Start_ON_text.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                }, mYear, mMonth, mDay
            )

            datePickerDialog.show()


        }

        Expire_ON_text.setOnClickListener { v->

            /*var str_v = Start_ON_text.text.toString()
            if(!str_v.isEmpty() && str_v!=null){
            }*/

            val c = Calendar.getInstance()
            mYear = c[Calendar.YEAR]
            mMonth = c[Calendar.MONTH]
            mDay = c[Calendar.DAY_OF_MONTH]
            val datePickerDialog = DatePickerDialog(
                this@AddOffer,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    Expire_ON_text.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
                }, mYear, mMonth, mDay
            )

       /*     try {
                val sdf = SimpleDateFormat("dd-MM-yyyy") //edit here
                val date1 = sdf.parse("31-12-2009")
                val date2 = sdf.parse("31-01-2010")
                Log.i("wwesssdsd", "00000000000000000000000000")
                println(sdf.format(date1))
                println(sdf.format(date2))
                if (date1.compareTo(date2) > 0) {
                    println("Date1 is after Date2")
                } else if (date1.compareTo(date2) < 0) {
                    println("Date1 is before Date2")
                } else if (date1.compareTo(date2) === 0) {
                    println("Date1 is equal to Date2")
                } else {
                    println("How to get here?")
                }
            } catch (ex: ParseException) {
                ex.printStackTrace()
            }*/

            datePickerDialog.show()

        }

        btn_save.setOnClickListener { v->

            val str_offer_title = offer_edit_text.text.toString();

            val str_Offer_desc = Offer_desc_text.text.toString();
            var str_Offer_For = Offer_For_text.text.toString();

            val str_Start_ON = Start_ON_text.text.toString();
            val str_Expire_ON = Expire_ON_text.text.toString();

            val str_No_of_coupon = No_of_coupon_text.text.toString();
            val str_Coupon_Code = Coupon_Code_text.text.toString();

            val str_offer_terms = offer_terms_text.text.toString();


            Log.i("test1", str_offer_title)
            Log.i("test2", str_Offer_desc)
            Log.i("test3", str_Offer_For)

            Log.i("str_Start_ON", str_Start_ON)
            Log.i("str_Expire_ON", str_Expire_ON)

            Log.i("test1", str_No_of_coupon)
            Log.i("test1", str_Coupon_Code)

            //str_Offer_For = "";

            print("qqqqqqqqqqqqqqqqqqqqqq")
            print(image_uri)

            //getBytes(is_image!!)


            //getBytes(is_image!!)?.let { uploadImage(str_offer_title,str_Offer_desc,str_Offer_For,str_Start_ON,str_Expire_ON,str_No_of_coupon,str_Coupon_Code,it) }

           // uploadImage(str_offer_title,str_Offer_desc,str_Offer_For,str_Start_ON,str_Expire_ON,str_No_of_coupon,str_Coupon_Code,str_offer_terms)

            //uploadImage(getBytes(is_image!!))

            /*image_uri?.let {
                uploadImage(str_offer_title,str_Offer_desc,str_Offer_For,str_Start_ON,str_Expire_ON,str_No_of_coupon,str_Coupon_Code,
                    it
                )
            }*/

            //image_uri?.let { uploadImage(it) }


            if(!str_offer_title.isEmpty()){
                if(str_offer_title.length>=3){
                    offer_text_input.error = null
                }else{
                    offer_text_input.error = "This value length is invalid. It should be between 3 and 100 characters long."
                }
            }else{
                offer_text_input.error = "Enter Offer Title"
            }

            if(!str_Start_ON.isEmpty()){
                if(!str_Start_ON.equals("dd/mm/yyyy")){
                    Start_ON_input.error = null
                }else{
                    Start_ON_input.error = "This value is required."
                }
            }else{
                Start_ON_input.error = "This value is required."

            }

            if(!str_Expire_ON.isEmpty()){
                if(!str_Expire_ON.equals("dd/mm/yyyy")){
                    Expire_ON_input.error = null
                }else{
                    Expire_ON_input.error = "This value is required."
                }
            }else{
                Expire_ON_input.error = "This value is required."
            }

            if(str_type==1){
                if(!str_No_of_coupon.isEmpty()){
                    No_of_coupon_input.error = null
                }else{
                    No_of_coupon_input.error = "This value is required."
                }
            }


            if(!str_Coupon_Code.isEmpty()){

                if(str_Coupon_Code.length>=3){
                    Coupon_Code_input.error = null
                }else{
                    Coupon_Code_input.error = "This value length is invalid. It should be between 3 and 6 characters long."
                }

            }else{
                Coupon_Code_input.error = "This value is required."
            }


            if(!str_offer_title.isEmpty() && str_offer_title.length>=3 &&
                !str_Start_ON.isEmpty() && !str_Start_ON.equals("dd/mm/yyy") &&
                !str_Expire_ON.isEmpty() && !str_Expire_ON.equals("dd/mm/yyy") &&
                !str_Coupon_Code.isEmpty() && str_Coupon_Code.length>=3){

               // if(!mediaPath.isEmpty() && mediaPath!=null){

                    try {
                        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.US)
                        val date1: Date = sdf.parse(str_Start_ON)
                        val date2: Date = sdf.parse(str_Expire_ON)

                        if(date1<=date2){
                            str_date_checking = "1"

                            if(str_type==0){
                                uploadImage(
                                    str_offer_title,
                                    str_Offer_desc,
                                    str_Offer_For,
                                    str_Start_ON,
                                    str_Expire_ON,
                                    str_No_of_coupon,
                                    str_Coupon_Code,
                                    str_offer_terms
                                )
                            }else{

                                if(!str_No_of_coupon.isEmpty()){
                                    No_of_coupon_input.error = null
                                    uploadImage(
                                        str_offer_title,
                                        str_Offer_desc,
                                        str_Offer_For,
                                        str_Start_ON,
                                        str_Expire_ON,
                                        str_No_of_coupon,
                                        str_Coupon_Code,
                                        str_offer_terms
                                    )
                                }else{
                                    No_of_coupon_input.error = "This value is required."
                                }

                            }

                        }else{
                            str_date_checking = "0"
                            Toast.makeText(
                                applicationContext,
                                "Expire date must be equal or higher than start date",
                                Toast.LENGTH_LONG
                            ).show()
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    //uploadImage(str_offer_title,str_Offer_desc,str_Offer_For,str_Start_ON,str_Expire_ON,str_No_of_coupon,str_Coupon_Code,str_offer_terms)
               /* }else{
                    Toast.makeText(
                        applicationContext,
                        "Upload offer image",
                        Toast.LENGTH_LONG
                    ).show()
                }*/

            }


        }

        if(!arrayListBranches.isEmpty()){
            arrayListBranches.clear()
        }
        if(!array_list_bool.isEmpty()){
            array_list_bool.clear()
        }
        branch_loaded(str_mem)
    }

    fun add_to_branches_checking(pos: Int, int_v: Int) {

        Log.i("Tested============", "" + pos);

        if(pos==0){

            if(int_v==1){
                for (i in 0 until array_list_bool.size){
                    array_list_bool.set(i, 0)
                }
                branchAdapter.notifyDataSetChanged()
            }else{
                for (i in 0 until array_list_bool.size){
                    array_list_bool.set(i, 1)
                }
                branchAdapter.notifyDataSetChanged()
            }

        }else{

            val int_first_pos = array_list_bool.get(0)

            if(int_first_pos==1){
                if(int_v==1){
                    for (i in 0 until array_list_bool.size){
                        array_list_bool.set(i, 0)
                    }
                    branchAdapter.notifyDataSetChanged()
                }else{
                    array_list_bool.set(pos, 1);
                    branchAdapter.notifyDataSetChanged()
                }

            }else{
                if(int_v==1){
                    array_list_bool.set(pos, 0);
                    branchAdapter.notifyDataSetChanged()
                }else{
                    array_list_bool.set(pos, 1);
                    branchAdapter.notifyDataSetChanged()
                }
            }



        }






    } // -------------------------------------------- add_to_branches ----------------------------


    fun add_to_branches(name: String, id: String) {

        Offer_For_text.setText(name)
        offer_branch_str = id

        alertDialog!!.dismiss()

    } // -------------------------------------------- add_to_branches ----------------------------

    fun logout_home() {
        val alertDialog = AlertDialog.Builder(this@AddOffer)
        alertDialog.setTitle("Logout !")
        alertDialog.setMessage("Are sure you want to logout your account.")
        alertDialog.setPositiveButton("Yes") { dialog, id ->

            val db = DatabaseHandler(this@AddOffer)
            db.resetTables()

            dialog.dismiss()
            val intent = Intent(this@AddOffer, LoginAuth::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()

        }
        alertDialog.setNegativeButton("Not Now") { dialog, id -> dialog.dismiss() }
        alertDialog.show()
    }

    private fun showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        val viewGroup = findViewById<ViewGroup>(R.id.content)

        //then we will inflate the custom alert dialog xml that we created
        val dialogView: View =
            LayoutInflater.from(this).inflate(R.layout.my_dialog, viewGroup, false)

        val txt_camera: TextView = dialogView.findViewById(R.id.txt_camera)
        val txt_gallery: TextView = dialogView.findViewById(R.id.txt_gallery)

        val btn_close: TextView = dialogView.findViewById(R.id.btn_close)

        //Now we need an AlertDialog.Builder object
        val builder = AlertDialog.Builder(this)

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it
        val alertDialog = builder.create()
        alertDialog.show()

        txt_camera.setOnClickListener { v->
            PERMISSION_CODE = 1000;
            camera_fn()
            alertDialog.dismiss()
        }
        txt_gallery.setOnClickListener { v->
            PERMISSION_CODE = 1001;
            gallery_fn()
            alertDialog.dismiss()
        }


        btn_close.setOnClickListener { v->
            alertDialog.dismiss()
        }
    }
    private fun camera_fn() {
        //if system os is Marshmallow or Above, we need to request runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED ||
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED){
                //permission was not enabled
                val permission = arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                //show popup to request permission
                requestPermissions(permission, PERMISSION_CODE)
            }
            else{
                //permission already granted
                openCamera()
            }
        }
        else{
            //system os is < marshmallow
            openCamera()
        }
    }
    private fun gallery_fn() {
        //if system os is Marshmallow or Above, we need to request runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (checkSelfPermission(Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED ||
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED){
                //permission was not enabled
                val permission = arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                //show popup to request permission
                requestPermissions(permission, PERMISSION_CODE)
            }
            else{
                //permission already granted
                pickImageFromGallery();
            }
        }
        else{
            //system os is < marshmallow
             pickImageFromGallery();
        }
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }
    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    private fun showBranchDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        val viewGroup = findViewById<ViewGroup>(R.id.content)

        //then we will inflate the custom alert dialog xml that we created
        val dialogView: View =
            LayoutInflater.from(this).inflate(R.layout.alert_branch, viewGroup, false)

        recycler_branch_list = dialogView.findViewById(R.id.recycler_branch_list)
        mLayoutManager = LinearLayoutManager(this@AddOffer)
        recycler_branch_list.layoutManager = LinearLayoutManager(
            this@AddOffer,
            LinearLayoutManager.VERTICAL,
            false
        )
        recycler_branch_list.addItemDecoration(
            DividerItemDecoration(
                this@AddOffer,
                LinearLayoutManager.VERTICAL
            )
        )
        recycler_branch_list.layoutManager = mLayoutManager

        recycler_branch_list.adapter = branchAdapter

        val btn_close: TextView = dialogView.findViewById(R.id.btn_close)
        val btn_submit: TextView = dialogView.findViewById(R.id.btn_submit)

        //Now we need an AlertDialog.Builder object
        val builder = AlertDialog.Builder(this)

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it
        alertDialog = builder.create()
        alertDialog!!.show()

        btn_close.setOnClickListener { v->
            alertDialog!!.dismiss()
        }

        btn_submit.setOnClickListener { v->
            var str_select = "0"

            if(!offer_branch_list_id.isEmpty()){
                offer_branch_list_id.clear()
            }
            if(!offer_branch_list_string.isEmpty()){
                offer_branch_list_string.clear()
            }



            for (i in 0 until array_list_bool.size){
                val str_pos = array_list_bool.get(0)
                val str_v = array_list_bool.get(i)

                if(str_pos==1){
                    offer_branch_str = ""
                    str_select = "1"
                    break
                }else{
                    if(str_v==1){
                        val str_v_br_id = arrayListBranches.get(i).get("id").toString()
                        offer_branch_list_id.add(str_v_br_id)
                        offer_branch_list_string.add(arrayListBranches.get(i).get("name").toString())
                    }
                } // str_pos

            }

            if(!offer_branch_list_id.isEmpty()){
                offer_branch_str =
                    Arrays.toString(offer_branch_list_id.toArray()).replace("[", "").replace("]", "")

                val str_br_name =  Arrays.toString(offer_branch_list_string.toArray()).replace("[", "").replace("]", "")
                Offer_For_text.setText(str_br_name)

                Log.i("offer_branch_str","----------------------"+offer_branch_str)
                alertDialog!!.dismiss()
            }else{
                if(str_select=="0"){
                    Toast.makeText(
                        applicationContext,
                        "Select the offer for",
                        Toast.LENGTH_LONG
                    ).show()
                }else{
                    Offer_For_text.setText("All Branches")
                    alertDialog!!.dismiss()
                }

            }

        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        //called when user presses ALLOW or DENY from Permission Request Popup
        when(requestCode){


            PERMISSION_CODE -> {

                Log.i("requestCodeSSSSSS", "" + requestCode)

                if (grantResults.size > 0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    //permission from popup was granted
                    if (PERMISSION_CODE == 1000) {
                        openCamera()
                    } else {
                        camera_fn()
                    } // if

                } else {
                    //permission from popup was denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        //called when image was captured from camera intent
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_CAPTURE_CODE){
            //set image captured to image view



            mediaPath = FilePath.getPath(this, image_uri)

            val file = File(mediaPath)
            val fileInBytes = file.length().toFloat()
            val fileSizeInKB = fileInBytes / 1024

            Log.i("fileSizeInKB", "666666666666666666666" + fileSizeInKB)

            //if (fileSizeInKB <= 1024 * 2) {
                //mediaPath = image_uri.toString()
                image_v.setImageURI(image_uri)
           /* }else{
                Toast.makeText(
                    applicationContext,
                    "Select Images less than 2 MB ",
                    Toast.LENGTH_LONG
                ).show()
            }*/
           // image_v.setImageURI(selectedImage as Uri?)


        }else  if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){

         //   image_v.setImageURI(data?.data)

            // Get the Image from data

            // Get the Image from data
            val selectedImage = data!!.data
            val filePathColumn =
                arrayOf(MediaStore.Images.Media.DATA)

            val cursor: Cursor = contentResolver.query(
                selectedImage!!,
                filePathColumn,
                null,
                null,
                null
            )!!
            cursor.moveToFirst()

            val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
            mediaPath = cursor.getString(columnIndex)
            //str1.setText(mediaPath)
            // Set the Image in ImageView for Previewing the Media
            // Set the Image in ImageView for Previewing the Media

            val file = File(mediaPath)
            val fileInBytes = file.length().toFloat()
            val fileSizeInKB = fileInBytes / 1024

           // if (fileSizeInKB <= 1024 * 2) {
                image_v.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
            /*}else{
                Toast.makeText(
                    applicationContext,
                    "Select Images less than 2 MB ",
                    Toast.LENGTH_LONG
                ).show()
            }*/

            //image_v.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
            cursor.close()

           /* try {
                is_image = contentResolver.openInputStream(data!!.data!!)!!
                imageBytes  = getBytes(is_image!!)
            } catch (e: IOException) {
                e.printStackTrace()
            }*/

        }
    } // onActivityResult

    @Throws(IOException::class)
    fun getBytes(`is`: InputStream): ByteArray? {
        val byteBuff = ByteArrayOutputStream()
        val buffSize = 1024
        val buff = ByteArray(buffSize)
        var len = 0
        while (`is`.read(buff).also({ len = it }) != -1) {
            byteBuff.write(buff, 0, len)
        }
        return byteBuff.toByteArray()
    }

    fun submitted_success() {
        val alertDialog = AlertDialog.Builder(this@AddOffer)
        alertDialog.setTitle("Add offer")
        alertDialog.setCancelable(false)
        alertDialog.setMessage("Your add offer submitted successfully.")
        alertDialog.setPositiveButton("Yes") { dialog, id ->
            offer_branch_str = ""
            onBackPressed()
        }
     /*   alertDialog.setNegativeButton("Not Now") { dialog, id -> dialog.dismiss() }*/
        alertDialog.show()
    }

    private fun uploadImage(
        str_offer_title: String,
        str_Offer_desc: String,
        str_Offer_For: String,
        str_Start_ON: String,
        str_Expire_ON: String,
        str_No_of_coupon: String,
        str_Coupon_Code: String,
        str_offer_terms: String
    ) {
// image_uri:Uri

       // is_image

        Log.i("str_offer_title", "-------------" + str_offer_title);
        Log.i("str_offer_title", "-------------" + str_type);


        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()



        val gson: Gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        val service = retrofit.create(APIServices::class.java)

       // val file: File = FileUtils.get(this, image_uri)

        // create RequestBody instance from file
       /* val requestFile: RequestBody = RequestBody.create(
            MediaType.parse(contentResolver.getType(imageBytes)),
            "file"
        )*/


        Log.i("str_radio", "1111111111111111111111111" + str_radio)

        val merchant_id: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_mem
        )
        val title: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_offer_title
        )
        val offer_type: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_radio
        )
        /*val offer_branch: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_Offer_For
        ) */// str_Offer_For

         // offer_branch_list
       /* val offer_branch: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_Offer_For
        )*/

        Log.i("offer_branch_list=====", "test--------------------")
        Log.i("arrayListBranches", "" + offer_branch_list)

        val offer_branch: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            offer_branch_str.toString()
        )

        Log.i("arrayListBranches=====", "test")
        Log.i("offer_branch_list_id", "" + offer_branch_list_id)

        val description: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_Offer_desc
        )

        val offer_terms: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_offer_terms
        )
        val start_on: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_Start_ON
        )
        val end_on: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_Expire_ON
        )
        val limit_type: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_type.toString()
        )

        val coupon_count: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_No_of_coupon
        )
        val code: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_Coupon_Code
        )



        Log.i("body_body", "000000000000000000000000" + merchant_id)
        Log.i("body_body", "000000000000000000000000" + title)
        Log.i("body_body", "000000000000000000000000" + offer_type)
        //Log.i("offer_branch", "000000000000000000000000" + offer_branch)

        val file = File(mediaPath)

        // Parsing any Media type file
        Log.i("file_file", "000000000000000000000000" + file)
        Log.i("file_file", "000000000000000000000000" + file.getName())
        // Parsing any Media type file

            val requestBody: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
            var image_upload = MultipartBody.Part.createFormData(
                "profimg",
                file.getName(),
                requestBody
            )
            Log.i("body_body", "000000000000000000000000" + requestBody)
            Log.i("image_upload", "000000000000000000000000" + image_upload.headers())
            Log.i("image_upload", "000000000000000000000000" + image_upload.body())
            Log.i("image_upload", "000000000000000000000000" + image_upload)


        //val filename: RequestBody = RequestBody.create(MediaType.parse("text/plain"), file.getName())
        //val requestFile1: RequestBody = RequestBody.create(MediaType.parse("image/jpeg"), imageBytes)

        //val body = MultipartBody.Part.createFormData("image", "image.jpg", requestFile1)



        //val body = MultipartBody.Part.createFormData("profimg", file.path, requestFile)

       /* val requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageBytes)
        val body =
            MultipartBody.Part.createFormData("image", "image.jpg", requestFile)*/


       // mProgressBar.setVisibility(View.VISIBLE)

      /*  val call: Call<UploadResponse> = service.upload(str_mem,str_offer_title,str_radio,"1",str_Offer_desc,"offer_terms",str_Start_ON,str_Expire_ON,
            str_No_of_coupon,str_Coupon_Code);*/
        Log.i("image_upload", "-ssdssssss-------------------" + image_upload);


        if(!mediaPath.isEmpty()){

            val call: Call<UploadResponse> = service.getAddOffer(
                merchant_id,
                title,
                offer_type,
                offer_branch,
                description,
                offer_terms,
                start_on,
                end_on,
                limit_type,
                coupon_count,
                code,
                image_upload
            ); //body
            call.enqueue(object : Callback<UploadResponse> {
                @SuppressLint("ResourceType")
                override fun onResponse(
                    call: Call<UploadResponse>,
                    response: Response<UploadResponse>
                ) {

                    val statusCode = response.code()
                    val user = response.body()

                    progressDialog.dismiss()

                    Log.i("ordercount_response", "response------------------" + response)
                    Log.i("statusCode", " 123_456_789 " + statusCode)
                    Log.i("user", " 123_456_789se " + user)

                    //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                    val str_success = response.body()!!.success
                    val str_error = response.body()!!.error

                    if (str_success == 1) {
                        submitted_success()
                    } else {

                        val str_error_code = response.body()!!.error_code
                        val str_error_msg = response.body()!!.error_msg.toString()
                        if (str_error_code == 2) {
                            Toast.makeText(applicationContext, str_error_msg, Toast.LENGTH_SHORT)
                                .show()
                        }

                    }

                    Log.i("str_success", " 123_456_789se " + str_success)


                }// ---------------------------------------- onResponse --------------------------------

                override fun onFailure(call: Call<UploadResponse>, t: Throwable) {
                    // Log error here since request failed
                    progressDialog.dismiss()

                    Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                    Log.i("zxczxc", "123_456_789se" + "---------------------")

                }
            })

        }else{


                val call: Call<UploadResponse> = service.getAddOfferWithoutImage(
                    merchant_id,
                    title,
                    offer_type,
                    offer_branch,
                    description,
                    offer_terms,
                    start_on,
                    end_on,
                    limit_type,
                    coupon_count,
                    code
                ); //body
                call.enqueue(object : Callback<UploadResponse> {
                    @SuppressLint("ResourceType")
                    override fun onResponse(
                        call: Call<UploadResponse>,
                        response: Response<UploadResponse>
                    ) {

                        val statusCode = response.code()
                        val user = response.body()

                        progressDialog.dismiss()

                        Log.i("ordercount_response", "response------------------" + response)
                        Log.i("statusCode", " 123_456_789 " + statusCode)
                        Log.i("user", " 123_456_789se " + user)

                        //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                        val str_success = response.body()!!.success
                        val str_error = response.body()!!.error

                        if (str_success == 1) {
                            offer_branch_str = ""
                            submitted_success()
                        } else {

                            val str_error_code = response.body()!!.error_code
                            val str_error_msg = response.body()!!.error_msg.toString()

                            if (str_error_code == 2) {
                                Toast.makeText(
                                    applicationContext,
                                    str_error_msg,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                        }

                        Log.i("str_success", " 123_456_789se " + str_success)


                    }// ---------------------------------------- onResponse --------------------------------

                    override fun onFailure(call: Call<UploadResponse>, t: Throwable) {
                        // Log error here since request failed
                        progressDialog.dismiss()

                        Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                        Log.i("zxczxc", "123_456_789se" + "---------------------")

                    }
                })


        }

    }


    fun branch_loaded(mem_id: String){
        // ======================================================== MEMBERACTION ==================================================================

        /* Log.i("manage_loaded"," 123_456_789se " + mem_id)
         Log.i("manage_loaded"," 123_456_789se " + "https://adb.brpaccess.com/api/manageoffers?merchant_id="+mem_id)
     */

        val progressDialog = ProgressDialog(this@AddOffer)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        Log.i(
            "manage_loaded",
            " 123_456_789se " + "https://adb.brpaccess.com/api/paymenthistory" + "?merchant_id=" +
                    str_mem
        )
        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getMerchantBranch(mem_id)

        call.enqueue(object : Callback<MerchantBranchResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<MerchantBranchResponse>,
                response: Response<MerchantBranchResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("wer_ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    //val data = response.body()!!.data
                    val results = response.body()!!.data.toTypedArray()

                    val map = java.util.HashMap<String, String>()
                    map["id"] = "0"
                    map["name"] = "All Branches"
                    map["merchant_id"] = str_mem

                    Offer_For_text.setText("All Branches")

                    arrayListBranches.add(map)
                    offer_branch_list.add("All Branches")
                    array_list_bool.add(0)

                    //offer_branch_list_id.add("0")
                    //offer_branch_list_id.add("19")
                    //offer_branch_list_id.add("20")

                    Offer_For_text.setText("All Branches")

                    Log.i("offer_branch_list_id", " 123_456_789se " + offer_branch_list_id)

                    Log.i("coupon_jsonObject", " 123_456_789se " + results.size)
                    for (i in 0 until results.size) {

                        val jsonObject = results.get(i)
                        val id = jsonObject.id;
                        val name = jsonObject.name;
                        val merchant_id = jsonObject.merchant_id;

                        val map = java.util.HashMap<String, String>()
                        map["id"] = id
                        map["name"] = name
                        map["merchant_id"] = merchant_id
                        map["checking"] = "0"

                        arrayListBranches.add(map)
                        array_list_bool.add(0)

                        Log.i("arrayListBranches", " 123_456_789se " + arrayListBranches)

                    }

                    branchAdapter =
                        BranchAdapter(this@AddOffer, arrayListBranches, array_list_bool);

                    Log.i("data_data", " 123_456_789se " + results)

                    progressDialog.dismiss()


                } else if (str_error == 1) {

                    progressDialog.dismiss()


                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<MerchantBranchResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------


}