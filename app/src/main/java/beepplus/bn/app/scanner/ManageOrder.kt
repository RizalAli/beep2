package beepplus.bn.app.scanner

import android.annotation.SuppressLint
import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import beepplus.bn.app.R
import beepplus.bn.app.adapter.ManageAdapter
import beepplus.bn.app.adapter.TransAdapter
import beepplus.bn.app.home.Home
import beepplus.bn.app.sql.DatabaseHandler
import com.naruvis.ecom.pojo.CountResponse
import com.naruvis.ecom.pojo.ManageOffResponse
import kotlinx.android.synthetic.main.activity_manage_order.*
import kotlinx.android.synthetic.main.content_manage_offer.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl

class ManageOrder : AppCompatActivity() {



    private var mLayoutManager: LinearLayoutManager? = null
    var str_mem = "";
    lateinit var manageAdapter: ManageAdapter
    var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_order)

        toolbar.setTitle("MANAGE OFFER")

        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_small_white_24)
        toolbar.setNavigationOnClickListener { v->
            onBackPressed()
        }

        val isMem_His = DatabaseHandler(applicationContext)
        val mem = isMem_His.isMem_His
        val sb_mem = StringBuilder()
        sb_mem.append("")
        sb_mem.append(mem)
        str_mem = sb_mem.toString()

        Log.i("str_mem_str_mem",str_mem)


        mLayoutManager = LinearLayoutManager(this)
        recycler_manage_offer!!.layoutManager = LinearLayoutManager(this@ManageOrder, LinearLayoutManager.VERTICAL, false)
        recycler_manage_offer!!.addItemDecoration(DividerItemDecoration(this@ManageOrder, LinearLayoutManager.VERTICAL))
        recycler_manage_offer!!.layoutManager = mLayoutManager

        manage_loaded(str_mem)

    }

    fun manage_loaded(mem_id:String){
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("manage_loaded"," 123_456_789se " + mem_id)
        Log.i("manage_loaded"," 123_456_789se " + "https://brpatrick.beepxs.solutions/api/manageoffers?merchant_id="+mem_id)

        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getManage(mem_id,"0")


        call.enqueue(object : Callback<ManageOffResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<ManageOffResponse>, response: Response<ManageOffResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    //val data = response.body()!!.data
                    val results = response.body()!!.data.results

                    Log.i("coupon_jsonObject"," 123_456_789se " + results)
                    for (i in 0 until results.size){
                        val jsonObject = results.get(i)

                        val id = jsonObject.id;
                        val title = jsonObject.title;
                        val merchant_name = jsonObject.merchant_name;
                        val start_on = jsonObject.start_on;
                        val end_on = jsonObject.start_on;
                        val offer_branch = jsonObject.offer_branch;
                        val branch_names = jsonObject.branch_names;
                        val coupon_count = jsonObject.coupon_count;
                        val code = jsonObject.code;
                        val coupon_grabbed = jsonObject.coupon_grabbed;
                        val used_coupon = jsonObject.used_coupon;
                        val status = jsonObject.status;
                        val img = jsonObject.img;

                        val description = jsonObject.description;
                        val offer_terms = jsonObject.offer_terms;

                        val map = java.util.HashMap<String, String>()
                        map["id"] = id
                        map["title"] = title
                        map["merchant_name"] = merchant_name
                        map["start_on"] = start_on
                        map["end_on"] = end_on
                        map["offer_branch"] = offer_branch
                        map["branch_names"] = branch_names
                        map["coupon_count"] = coupon_count
                        map["code"] = code
                        map["coupon_grabbed"] = coupon_grabbed
                        map["used_coupon"] = used_coupon
                        map["status"] = status
                        map["img"] = img

                        arrayListTrans.add(map)


                    }



                    Log.i("data_data"," 123_456_789se " + results)


                    progressDialog.dismiss()

                    manageAdapter = ManageAdapter(this@ManageOrder ,arrayListTrans)
                    recycler_manage_offer.adapter = manageAdapter


                }else if(str_error==1){

                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<ManageOffResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------

}