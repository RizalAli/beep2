package beepplus.bn.app.home.ui.slideshow

import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import beepplus.bn.app.R
import beepplus.bn.app.auth_screen.LoginAuth
import beepplus.bn.app.home.Home
import com.adroitandroid.chipcloud.ChipListener


class SlideshowFragment : Fragment() {

  lateinit var chip_cloud: com.adroitandroid.chipcloud.ChipCloud
  lateinit var progress:ProgressBar
  private lateinit var slideshowViewModel: SlideshowViewModel
    lateinit var webview_load:WebView
    var str_mobile_no = LoginAuth.mobile_no
    private val url = "https://pasarpbg.com/loginclick?usermob="+str_mobile_no


    /* var str_symbol = "$"
    lateinit var recycler_trans: RecyclerView
    lateinit var mLayoutManager: LinearLayoutManager
    lateinit var transAdapter: TransAdapter
    var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()
*/
    lateinit var webview: WebView


  override fun onCreateView(
          inflater: LayoutInflater,
          container: ViewGroup?,
          savedInstanceState: Bundle?
  ): View? {

    slideshowViewModel =
            ViewModelProviders.of(this).get(SlideshowViewModel::class.java)
    val root = inflater.inflate(R.layout.fragment_slideshow, container, false)
    //val textView: TextView = root.findViewById(R.id.text_slideshow)
    slideshowViewModel.text.observe(viewLifecycleOwner, Observer {
      //textView.text = it
        //val myWebView: WebView = root.findViewById(R.id.webview)



        chip_cloud = root.findViewById(R.id.chip_cloud)
        webview_load = root.findViewById(R.id.webview_load)
        progress = root.findViewById(R.id.progress)

        chip_cloud.addChip("Pasarpbg" + " ( " + Home.cart_count + " )");
        chip_cloud.addChip("Rimba Point" + " ( " + "0" + " )");

        chip_cloud.setSelectedChip(0)

      chip_cloud!!.setChipListener(object : ChipListener {
        override fun chipSelected(i: Int) {
          Log.i("chipCloud", "" + i)

          if(i==0){

          }else{

          }

        }

        override fun chipDeselected(i: Int) {
        }
      }) // ----



        /* val webSetting: WebSettings = myWebView.getSettings()
         webSetting.builtInZoomControls = true
         webSetting.javaScriptEnabled = true
         myWebView.setWebViewClient(WebViewClient())
         //myWebView.loadUrl("http://www.javapapers.com")
         myWebView.loadUrl("https://pasarpbg.com")
         myWebView.settings.javaScriptEnabled = true
         myWebView.setHorizontalScrollBarEnabled(true);
         myWebView.setVerticalScrollBarEnabled(true);*/

        // Get the web view settings instance
        val settings = webview_load.settings

        // Enable java script in web view
        settings.javaScriptEnabled = true

        // Enable and setup web view cache
        settings.setAppCacheEnabled(true)
        settings.cacheMode = WebSettings.LOAD_DEFAULT
        //settings.setAppCachePath(cacheDir.path)


        // Enable zooming in web view
        settings.setSupportZoom(true)
        settings.builtInZoomControls = true
        settings.displayZoomControls = true


        // Zoom web view text
        settings.textZoom = 125


        // Enable disable images in web view
        settings.blockNetworkImage = false
        // Whether the WebView should load image resources
        settings.loadsImagesAutomatically = true


        // More web view settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            settings.safeBrowsingEnabled = true  // api 26
        }
        //settings.pluginState = WebSettings.PluginState.ON
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.mediaPlaybackRequiresUserGesture = false


        // More optional settings, you can enable it by yourself
        settings.domStorageEnabled = true
        settings.setSupportMultipleWindows(true)
        settings.loadWithOverviewMode = true
        settings.allowContentAccess = true
        settings.setGeolocationEnabled(true)
        settings.allowUniversalAccessFromFileURLs = true
        settings.allowFileAccess = true

        // WebView settings
        webview_load.fitsSystemWindows = true


        /*
            if SDK version is greater of 19 then activate hardware acceleration
            otherwise activate software acceleration
        */
        webview_load.setLayerType(View.LAYER_TYPE_HARDWARE, null)


        // Set web view client
        webview_load.webViewClient = object: WebViewClient(){
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                // Page loading started
                // Do something
//                context?.toast("Page loading.")

             // progress. visibility = View.VISIBLE

                // Enable disable back forward button
                //button_back.isEnabled = web_view.canGoBack()
                //button_forward.isEnabled = web_view.canGoForward()
            }

            override fun onPageFinished(view: WebView, url: String) {
                // Page loading finished
                // Display the loaded page title in a toast message
//              view.destroy()
//                context!!.toast("Page loaded: ${view.title}")

            //  progress. visibility = View.GONE

                // Enable disable back forward button
                //button_back.isEnabled = web_view.canGoBack()
                //button_forward.isEnabled = web_view.canGoForward()
            }
        }

        Log.i("Tested_mobile_network","oooooooo" + url)

        webview_load.loadUrl(url)

      webview_load.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
        if (event.action === KeyEvent.ACTION_DOWN) {
          if (keyCode == KeyEvent.KEYCODE_BACK && webview_load.canGoBack()) {  //表示按返回键
            webview_load.goBack() //后退
            //webview.goForward();//前进
            return@OnKeyListener true //已处理
          }
        }
        false
      })



    })
    return root
  }


    fun Context.toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

}