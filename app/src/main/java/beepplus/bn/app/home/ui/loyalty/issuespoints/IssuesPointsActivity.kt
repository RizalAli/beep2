package beepplus.bn.app.home.ui.loyalty.issuespoints

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import beepplus.bn.app.R
import beepplus.bn.app.home.ui.marketplace.OrderDetailsAct
import beepplus.bn.app.sql.DatabaseHandler
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.integration.android.IntentIntegrator
import com.naruvis.ecom.pojo.CommonResponse
import com.naruvis.ecom.pojo.MemberDetailsResponse
import com.naruvis.ecom.pojo.TPointsResponse
import com.naruvis.ecom.pojo.XsPointsResponse
import kotlinx.android.synthetic.main.activity_issues_points.*
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl


class IssuesPointsActivity : AppCompatActivity() {
    private var qrScan: IntentIntegrator? = null
    var str_name = "";
    var str_mem = "";
    var str_amt = "0.00"
    var points = "";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_issues_points)

       /* val isMem_His = DatabaseHandler(applicationContext)
        val mem = isMem_His.isMem_His
        val sb_mem = StringBuilder()
        sb_mem.append("")
        sb_mem.append(mem)
        str_mem = sb_mem.toString()*/


        val isMem_brid = DatabaseHandler(applicationContext)
        val mem_brid = isMem_brid.isMem_brid
        val sb_mem_brid = StringBuilder()
        sb_mem_brid.append("")
        sb_mem_brid.append(mem_brid)
        str_mem = sb_mem_brid.toString()

        val isMem_name = DatabaseHandler(applicationContext)
        val mem_name = isMem_name.isMem_name
        val sb_mem_n = StringBuilder()
        sb_mem_n.append("")
        sb_mem_n.append(mem_name)
        str_name = sb_mem_n.toString()

        txt_user_id.text = str_name;

        btn_issues_points.setOnClickListener { v->

            str_amt = edt_amount.text.toString()

            if(!str_amt.isEmpty()){

                val str_av_point = txt_avpoints.text.toString()

                amount_checking(str_amt)
               /* if(str_av_point.toInt()>=str_amt.toInt()){

                }else{
                   *//* Snackbar.make(v, "Your available points is too low", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()*//*
                }*/

                /*val intent = Intent(context, QRcode::class.java)
                intent.putExtra("amount",str_amt)
                intent.putExtra("notes",str_notes)
                startActivity(intent)*/

            }else{
                Snackbar.make(v, "Enter the amount", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }

        }

        img_back.setOnClickListener { v->
            onBackPressed()
        }

        showSoftKeyboard(edt_amount)
        member_details()
        common_loaded();

    }


    fun member_details(){
        // ======================================================== MEMBERACTION ==================================================================

      /*  val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()*/

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getMemberDetails(str_mem)


        call.enqueue(object : Callback<MemberDetailsResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<MemberDetailsResponse>,
                response: Response<MemberDetailsResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    val mem_data = response.body()!!.data.results
                    val xs_points = mem_data.xs_points

                    if(xs_points!=null){
                        txt_avpoints.setText(xs_points)
                    }else{
                        txt_avpoints.setText("0")
                    }

                    Log.i("str_success", "123_456_789se" + str_success)
                    //progressDialog.dismiss()


                } else if (str_error.equals("1")) {

                    txt_avpoints.setText("0")
                    //progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<MemberDetailsResponse>, t: Throwable) {
                // Log error here since request failed
                //progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------


    fun common_loaded(){
        // ======================================================== MEMBERACTION ==================================================================

        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getCommondata()


        call.enqueue(object : Callback<CommonResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<CommonResponse>, response: Response<CommonResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    //txt_program
                    //txt_xspoints


                    val program = response.body()!!.data.program
                    val show_xspoint = response.body()!!.data.show_xspoint
                    val show_amount = response.body()!!.data.show_amount
                    val show_currency = response.body()!!.data.show_currency

                    txt_program.text = program;
                    xspoints.text = show_xspoint + " " +  "XS Point"
                    txt_xspoints.text = show_currency + ""+show_amount

                    Log.i("str_success","123_456_789se" + str_success)

                    progressDialog.dismiss()


                }else if(str_error==1){

                    //Toast.makeText(this@IssuesPointsActivity, "Wrong Email or Password", 1000).show()
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<CommonResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------

    fun amount_checking(amount:String){
        // ======================================================== MEMBERACTION ==================================================================

        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getXsPoints(amount)


        call.enqueue(object : Callback<XsPointsResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<XsPointsResponse>, response: Response<XsPointsResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    //txt_program
                    //txt_xspoints

                    points = response.body()!!.point.toString()

                    Log.i("str_success","123_456_789se" + str_success)

                    Log.i("point","123_456_789se" + points)


                    qrScan = IntentIntegrator(this@IssuesPointsActivity)
                    qrScan!!.initiateScan()

                    progressDialog.dismiss()

                }else if(str_error==1){

                    val errmsg = response.body()!!.errmsg.toString()
                    Toast.makeText(this@IssuesPointsActivity, errmsg, 1000).show()
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<XsPointsResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            //imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    fun hideSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            //imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }


    fun xs_points_transfer(
        str_customer_id: String,
        str_amt: String,
        points: String,
        str_mem: String,
        s: String
    ) {
        // ======================================================== MEMBERACTION ==================================================================

        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getTPoints(str_amt,str_customer_id,str_mem,points,s)


        call.enqueue(object : Callback<TPointsResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<TPointsResponse>, response: Response<TPointsResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("xspoints_ordercount","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    val amount = response.body()!!.amount
                    val show_date = response.body()!!.show_date
                    val transaction_id = response.body()!!.transaction_id
                    val xs_point = response.body()!!.xs_point
                    val user_name = response.body()!!.user_name
                    val user_id = response.body()!!.user_id
                    var errmsg = response.body()!!.errmsg

                    Log.i("Testing_purpose",""+amount);
                    Log.i("Testing_purpose",""+show_date);
                    Log.i("Testing_purpose",""+transaction_id);
                    Log.i("Testing_purpose",""+xs_point);
                    Log.i("Testing_purpose",""+user_name);
                    Log.i("Testing_purpose",""+user_id);

                    progressDialog.dismiss()


                    val intent = Intent(this@IssuesPointsActivity, IssuesPointsSuccess::class.java)
                    intent.putExtra("amount",str_amt)
                    intent.putExtra("show_date",show_date)
                    intent.putExtra("transaction_id",transaction_id)
                    intent.putExtra("xs_point",xs_point)
                    intent.putExtra("user_name",user_name)
                    intent.putExtra("user_id",user_id)
                    intent.putExtra("status","1")
                    intent.putExtra("errmsg",errmsg)
                    startActivity(intent)
                    finish()

                }else if(str_error==1){

                    /*val intent = Intent(this@IssuesPointsActivity, IssuesPointsSuccess::class.java)
                    intent.putExtra("amount","")
                    intent.putExtra("show_date","")
                    intent.putExtra("transaction_id","")
                    intent.putExtra("xs_point","")
                    intent.putExtra("user_name","")
                    intent.putExtra("user_id","")
                    intent.putExtra("status","0")
                    intent.putExtra("errmsg","")
                    startActivity(intent)
                    finish()*/
                    var errmsg = response.body()!!.errmsg

                    Toast.makeText(this@IssuesPointsActivity, errmsg, 1000).show()
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<TPointsResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------



    fun alert_XSpoints(str_customer_id: String, str_amt: String, points: String, str_mem: String) {
        val alertDialog = AlertDialog.Builder(this@IssuesPointsActivity)
        alertDialog.setTitle("Confirm to issue points?")
        alertDialog.setMessage("You are issuing "+ this.points +"XS points to user")
        alertDialog.setPositiveButton("Yes") { dialog, id ->
            xs_points_transfer(str_customer_id,str_amt,points,str_mem,"1")
            dialog.dismiss()
        }
        alertDialog.setNegativeButton("Cancel") { dialog, id ->
            // xs_points_transfer(str_customer_id,str_amt,points,str_mem,"8")

         /*   val intent = Intent(this@IssuesPointsActivity, IssuesPointsSuccess::class.java)
            intent.putExtra("amount","")
            intent.putExtra("show_date","")
            intent.putExtra("transaction_id","")
            intent.putExtra("xs_point","")
            intent.putExtra("user_name","")
            intent.putExtra("user_id","")
            intent.putExtra("status","0")
            intent.putExtra("errmsg","")
            startActivity(intent)
            finish()*/
            //Toast.makeText(this@IssuesPointsActivity, "Wrong Email or Password", 1000).show()
            //progressDialog.dismiss()
            dialog.dismiss()

        }
        alertDialog.show()
    }

    //Getting the scan results
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        val result =
            IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            //if qrcode has nothing in it
            if (result.contents == null) {
                //    user_t.visibility = View.GONE
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show()
               // onBackPressed()
            } else {
                //if qr contains data
                try {
                    //converting the data to json
                    //    val obj = JSONObject(result.contents)
                    //setting values to textviews
                    //textViewName.setText(obj.getString("name"));
                    //textViewAddress.setText(obj.getString("address"));



                    val str_result = result.contents
                    val currentString = str_result
                    val separated: Array<String> = currentString.split("-").toTypedArray()
                    /* val separated =
                         currentString.split("-".toRegex()).toTypedArray()*/

                    if(separated[0].toString().trim()=="ACCESS"){
                        var str_customer_id = separated[1].toString().trim()
                        Log.i("str_customer_id", str_customer_id);
                        //        user_t.visibility = View.GONE
                       // str_coupon_code = ""
                       // access_scanned(str_customer_id)
                        alert_XSpoints(str_customer_id,str_amt,points,str_mem)
                    }else{
                        //str_coupon_code = str_result
                        //coupon_scanned(str_result)
                    }


                    Log.i("str_result_str_result",str_result);
                    //Toast.makeText(this, result.contents, Toast.LENGTH_LONG).show()

                } catch (e: JSONException) {
                    e.printStackTrace()
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
                    //       user_t.visibility = View.GONE
                    Toast.makeText(this, result.contents, Toast.LENGTH_LONG).show()
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

}