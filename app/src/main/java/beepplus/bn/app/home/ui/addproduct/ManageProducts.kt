package beepplus.bn.app.home.ui.addproduct

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.adapter.HistoryTrans
import beepplus.bn.app.adapter.ManageAdapter
import beepplus.bn.app.adapter.ManageProAdapter
import beepplus.bn.app.auth_screen.LoginAuth
import beepplus.bn.app.common.FilePath
import beepplus.bn.app.home.ui.gallery.GalleryFragment
import beepplus.bn.app.home.ui.home.HomeFragment
import beepplus.bn.app.home.ui.loyalty.LoyaltyFragment
import beepplus.bn.app.home.ui.loyalty.LoyaltyViewModel
import beepplus.bn.app.home.ui.loyalty.ManageOfferFragment
import beepplus.bn.app.home.ui.marketplace.MarketPlaceList
import beepplus.bn.app.sql.DatabaseHandler
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.naruvis.ecom.pojo.ManageOffResponse
import com.naruvis.ecom.pojo.ManageProResponse
import com.naruvis.ecom.pojo.UploadProResponse
import kotlinx.android.synthetic.main.fragment_manage_pro.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl
import java.io.File

class ManageProducts : Fragment() {

    lateinit var chip_cloud: com.adroitandroid.chipcloud.ChipCloud
    lateinit var progress: ProgressBar
    private lateinit var loyaltyViewModel: LoyaltyViewModel

    var str_mobile_no = LoginAuth.mobile_no
    var str_mem = ""
    private val url = "https://brpatrick.beepxs.solutions/merchant/offer?merchant_id=2"
    lateinit var historyTrans: HistoryTrans
    var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()
    var arrayListCheck = ArrayList<String>()

    var fragment: Fragment? = null
    var seller_id = "42"
    var cat_id = "3"
    var action = "AddProduct"
    lateinit var fragmentClass: Class<*>

    lateinit var edt_pro: EditText;
    lateinit var edt_price: EditText;

    lateinit var img_camera: ImageView;
    lateinit var btn_submit: Button;
    lateinit var add_pro: TextView;

    var mediaPath = ""
    var PERMISSION_CODE = 1000;
    var IMAGE_CAPTURE_CODE = 1001 // id_userprof_dateOfBirth_txtVw!!.text
    var IMAGE_PICK_CODE = 1000;
    var image_uri: Uri? = null

    private var mLayoutManager: LinearLayoutManager? = null
    lateinit var manageProAdapter: ManageProAdapter
    lateinit var recycler_manage: RecyclerView

    lateinit var l1: LinearLayout
    lateinit var l3: LinearLayout

    //lateinit var txt_no_records: TextView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        loyaltyViewModel =
            ViewModelProviders.of(this).get(LoyaltyViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_manage_pro, container, false)
        //val textView: TextView = root.findViewById(R.id.text_slideshow)
        loyaltyViewModel.text.observe(viewLifecycleOwner, Observer {

            val isMem_His = context?.let { it1 -> DatabaseHandler(it1) }
            val mem = isMem_His?.isMem_His
            val sb_mem = StringBuilder()
            sb_mem.append("")
            sb_mem.append(mem)
            str_mem = sb_mem.toString()

            edt_pro = root.findViewById(R.id.edt_pro)
            edt_price = root.findViewById(R.id.edt_price)
            img_camera = root.findViewById(R.id.img_camera)
            btn_submit = root.findViewById(R.id.btn_submit)
            l1 = root.findViewById(R.id.l1)
            l3 = root.findViewById(R.id.l3)

            img_camera.setOnClickListener { v->
                showCustomDialog();
            }

            l1.setOnClickListener { v->
                fragmentClass_Menu(1)
            }
            l3.setOnClickListener { v->
                fragmentClass_Menu(3)
            }

            btn_submit.setOnClickListener { v->

                var product = edt_pro.text.toString()
                var price = edt_price.text.toString()

                if(product!="" && product!=null){

                }else{
                    edt_pro.setError("Enter the product")
                }

                if(price!="" && price!=null){

                }else{
                    edt_price.setError("Enter the price")
                }

                if(product!="" && product!=null && price!="" && price!=null ){

                    if(mediaPath.isEmpty() && mediaPath==null){
                        Toast.makeText(
                            context,
                            "Upload offer image",
                            Toast.LENGTH_LONG
                        ).show()
                    }else{
                        uploadImage(seller_id,product,cat_id,price,action)
                    }

                }else{

                }
                //www.onebrunei.de/api/addproduct?seller_id=42&idProfile=&product_name=test pro&main_category=3&price=2&action=AddProduct
            }

            // --------------------------------------------------- MANAGE LIST
            recycler_manage = root!!.findViewById(R.id.recycler_manage)
            mLayoutManager = LinearLayoutManager(context)
            recycler_manage.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
         /*   recycler_manage.addItemDecoration(
                DividerItemDecoration(
                    context,
                    LinearLayoutManager.VERTICAL
                )
            )*/
            recycler_manage.layoutManager = mLayoutManager
            add_pro = root.findViewById(R.id.add_pro)
            manage_pro_loaded(seller_id)

            add_pro.setOnClickListener { v->
                Log.i("tested=========================","add_pro")
                fragmentClass_Menu(4)
            }

        })
        return root
    }


    fun manage_pro_loaded(s_id: String) {
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("manage_loaded", " 123_456_789se " + s_id)
        Log.i(
            "manage_loaded1",
            " 123_456_789se " + "https://onebrunei.ideablitztech.com/api/manageproduct?seller_id="+s_id
        )

        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_ONE_BRUNEI_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getManagePro(s_id)


        call.enqueue(object : Callback<ManageProResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<ManageProResponse>,
                response: Response<ManageProResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    //val data = response.body()!!.data
                    val results = response.body()!!.data

                    Log.i("coupon_jsonObject", " 123_456_789se " + results)
                    for (i in 0 until results.size) {
                        val jsonObject = results.get(i)

                        val id = jsonObject.id;
                        val name = jsonObject.name;
                        val image = jsonObject.image;
                        val opt_price = jsonObject.opt_price;
                        val category_id = jsonObject.category_id;


                        val map = java.util.HashMap<String, String>()
                        map["id"] = id
                        map["name"] = name
                        map["image"] = image
                        map["category_id"] = category_id
                        map["opt_price"] = opt_price

                        arrayListCheck.add(category_id)
                        arrayListTrans.add(map)

                    }


                    Log.i("data_data", " 123_456_789se " + results)


                    progressDialog.dismiss()

                    manageProAdapter = ManageProAdapter(context, arrayListTrans)
                    recycler_manage.adapter = manageProAdapter

                    //txt_no_records.visibility = View.GONE
                } else if (str_error == 1) {
                    //txt_no_records.visibility = View.VISIBLE
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<ManageProResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------

    // UPDAET ============================================ PRODUCTS =================================================================
    // ------------------------------------------------------------------------------------------------------------------------------
    private fun showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        val viewGroup = requireActivity().findViewById<ViewGroup>(R.id.content)

        //then we will inflate the custom alert dialog xml that we created
        val dialogView: View =
            LayoutInflater.from(context).inflate(R.layout.my_dialog, viewGroup, false)

        val txt_camera: TextView = dialogView.findViewById(R.id.txt_camera)
        val txt_gallery: TextView = dialogView.findViewById(R.id.txt_gallery)

        val btn_close: TextView = dialogView.findViewById(R.id.btn_close)

        //Now we need an AlertDialog.Builder object
        val builder = AlertDialog.Builder(requireActivity())

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it
        val alertDialog = builder.create()
        alertDialog.show()

        txt_camera.setOnClickListener { v->
            PERMISSION_CODE = 1000;
            camera_fn()
            alertDialog.dismiss()
        }
        txt_gallery.setOnClickListener { v->
            PERMISSION_CODE = 1001;
            gallery_fn()
            alertDialog.dismiss()
        }


        btn_close.setOnClickListener { v->
            alertDialog.dismiss()
        }
    }
    private fun camera_fn() {
        //if system os is Marshmallow or Above, we need to request runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (requireActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED){
                //permission was not enabled
                val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                //show popup to request permission
                requestPermissions(permission, PERMISSION_CODE)
            }
            else{
                //permission already granted
                openCamera()
            }
        }
        else{
            //system os is < marshmallow
            openCamera()
        }
    }
    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = requireActivity().contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }
    private fun gallery_fn() {
        //if system os is Marshmallow or Above, we need to request runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (requireActivity().checkSelfPermission(Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED ||
                requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED){
                //permission was not enabled
                val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                //show popup to request permission
                requestPermissions(permission, PERMISSION_CODE)
            }
            else{
                //permission already granted
                pickImageFromGallery();
            }
        }
        else{
            //system os is < marshmallow
            pickImageFromGallery();
        }
    }
    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //called when user presses ALLOW or DENY from Permission Request Popup
        when(requestCode){


            PERMISSION_CODE -> {

                Log.i("requestCodeSSSSSS","" + requestCode)

                if (grantResults.size > 0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup was granted
                    if(PERMISSION_CODE==1000){
                        openCamera()
                    }else{
                        camera_fn()
                    } // if

                }
                else{
                    //permission from popup was denied
                    Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show()
                }

            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        //called when image was captured from camera intent
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_CAPTURE_CODE){
            //set image captured to image view



            mediaPath = FilePath.getPath(context, image_uri)

            val file = File(mediaPath)
            val fileInBytes = file.length().toFloat()
            val fileSizeInKB = fileInBytes / 1024

            Log.i("fileSizeInKB","666666666666666666666" + fileSizeInKB)

            if (fileSizeInKB <= 1024 * 2) {
                //mediaPath = image_uri.toString()
                img_camera.setImageURI(image_uri)
            }else{
                Toast.makeText(
                    context,
                    "Select Images less than 2 MB ",
                    Toast.LENGTH_LONG
                ).show()
            }
            // image_v.setImageURI(selectedImage as Uri?)


        }else  if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){

            //   image_v.setImageURI(data?.data)

            // Get the Image from data

            // Get the Image from data
            val selectedImage = data!!.data
            val filePathColumn =
                arrayOf(MediaStore.Images.Media.DATA)

            val cursor: Cursor = requireActivity().contentResolver.query(selectedImage!!, filePathColumn, null, null, null)!!
            cursor.moveToFirst()

            val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
            mediaPath = cursor.getString(columnIndex)
            //str1.setText(mediaPath)
            // Set the Image in ImageView for Previewing the Media
            // Set the Image in ImageView for Previewing the Media

            val file = File(mediaPath)
            val fileInBytes = file.length().toFloat()
            val fileSizeInKB = fileInBytes / 1024

            if (fileSizeInKB <= 1024 * 2) {
                img_camera.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
            }else{
                Toast.makeText(
                    context,
                    "Select Images less than 2 MB ",
                    Toast.LENGTH_LONG
                ).show()
            }

            //image_v.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
            cursor.close()

            /* try {
                 is_image = contentResolver.openInputStream(data!!.data!!)!!
                 imageBytes  = getBytes(is_image!!)
             } catch (e: IOException) {
                 e.printStackTrace()
             }*/

        }
    } // onActivityResult

    private fun uploadImage(
        seller_id: String,
        product:String,
        cat_id:String,
        price:String,
        action:String
    ) {

        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()



        val gson: Gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        val service = retrofit.create(APIServices::class.java)

        // val file: File = FileUtils.get(this, image_uri)
        // create RequestBody instance from file
        /* val requestFile: RequestBody = RequestBody.create(
             MediaType.parse(contentResolver.getType(imageBytes)),
             "file"
         )*/
        // www.onebrunei.de/api/addproduct?seller_id=42&idProfile=&product_name=test pro&main_category=3&price=2&action=AddProduct

        val seller_id: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), seller_id)
        val product_name: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), product)
        val main_category: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), cat_id) // str_Offer_For
        val price: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), price)
        val action: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), action)

        val file = File(mediaPath)

        // Parsing any Media type file
        Log.i("file_file","000000000000000000000000"+file)
        Log.i("file_file","000000000000000000000000"+file.getName())
        // Parsing any Media type file
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val image_upload = MultipartBody.Part.createFormData("idProfile", file.getName(), requestBody)

        Log.i("body_body","000000000000000000000000"+requestBody)
        Log.i("image_upload","000000000000000000000000"+image_upload.headers())
        Log.i("image_upload","000000000000000000000000"+image_upload.body())

        //val filename: RequestBody = RequestBody.create(MediaType.parse("text/plain"), file.getName())
        //val requestFile1: RequestBody = RequestBody.create(MediaType.parse("image/jpeg"), imageBytes)

        //val body = MultipartBody.Part.createFormData("image", "image.jpg", requestFile1)



        //val body = MultipartBody.Part.createFormData("profimg", file.path, requestFile)

        /* val requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageBytes)
         val body =
             MultipartBody.Part.createFormData("image", "image.jpg", requestFile)*/

        val call: Call<UploadProResponse> = service.getAddPro(seller_id,image_upload,product_name,main_category,price,action); //body
        // mProgressBar.setVisibility(View.VISIBLE)

        /*  val call: Call<UploadResponse> = service.upload(str_mem,str_offer_title,str_radio,"1",str_Offer_desc,"offer_terms",str_Start_ON,str_Expire_ON,
              str_No_of_coupon,str_Coupon_Code);*/

        call.enqueue(object : Callback<UploadProResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<UploadProResponse>, response: Response<UploadProResponse>) {

                val statusCode = response.code()
                val user = response.body()

                progressDialog.dismiss()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){
                    submitted_success()
                }

                Log.i("str_success"," 123_456_789se " + str_success)


            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<UploadProResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }

    fun submitted_success() {
        val alertDialog = context?.let { AlertDialog.Builder(it) }
        alertDialog!!.setTitle("Add offer")
        alertDialog.setMessage("Your product added  successfully.")
        alertDialog.setPositiveButton("Yes") { dialog, id ->
            edt_price.setText("")
            edt_pro.setText("")
            mediaPath = "";
            img_camera.setImageDrawable(resources.getDrawable(R.drawable.ic_menu_camera))
            dialog.dismiss()
        }
        /*   alertDialog.setNegativeButton("Not Now") { dialog, id -> dialog.dismiss() }*/
        alertDialog.show()
    }

    fun fragmentClass_Menu(frag_check: Int){

        if(frag_check==1){
            fragmentClass = MarketPlaceList::class.java
        }else if(frag_check==2){
            fragmentClass = GalleryFragment::class.java
        }else if(frag_check==3){
            fragmentClass = LoyaltyFragment::class.java
        }else if(frag_check==4){
            fragmentClass = Addprofragment::class.java
        }


        try {
            fragment = fragmentClass.newInstance() as Fragment
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // Insert the fragment by replacing any existing fragment
        val fragmentManager: FragmentManager = requireActivity().supportFragmentManager
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment!!).commit()
        }
    }

}