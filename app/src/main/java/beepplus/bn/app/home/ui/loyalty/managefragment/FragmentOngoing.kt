package beepplus.bn.app.home.ui.loyalty.managefragment


import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.adapter.ManageAdapter
import beepplus.bn.app.details.ManageDetails
import beepplus.bn.app.home.ui.loyalty.ManageOfferDetails
import beepplus.bn.app.sql.DatabaseHandler
import com.naruvis.ecom.pojo.ManageOffResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit.Builder
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl

class FragmentOngoing : Fragment(){ //ManageAdapter.ClickListener

    // 1 - 0 -8 // status  =  ongoing - pending - expired
    private var mLayoutManager: LinearLayoutManager? = null
    var str_mem = "";
    lateinit var manageAdapter: ManageAdapter
    var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()
    lateinit var recycler_list: RecyclerView
    lateinit var txt_no_records: TextView
    var fragment: Fragment? = null
    lateinit var fragmentClass: Class<*>
    private var rootView: View? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_list, container, false)
        /*val isMem_His = context?.let { DatabaseHandler(it) }
        val mem = isMem_His!!.isMem_His
        val sb_mem = StringBuilder()
        sb_mem.append("")
        sb_mem.append(mem)
        str_mem = sb_mem.toString()*/

        val isMem_brid = context?.let { it1 -> DatabaseHandler(it1) }
        val mem_brid = isMem_brid?.isMem_brid
        val sb_mem_brid = StringBuilder()
        sb_mem_brid.append("")
        sb_mem_brid.append(mem_brid)
        str_mem = sb_mem_brid.toString()

        Log.i("str_mem_str_mem", str_mem)
        recycler_list = rootView!!.findViewById(R.id.recycler_list)
        mLayoutManager = LinearLayoutManager(context)
        recycler_list.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recycler_list.addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayoutManager.VERTICAL
            )
        )
        recycler_list.layoutManager = mLayoutManager
        // txt_record_found = root.findViewById(R.id.txt_record_found)
        txt_no_records = rootView!!.findViewById(R.id.txt_no_records)

        if (!arrayListTrans.isEmpty()) {
            arrayListTrans.clear();
            manage_loaded(str_mem)
        } else {
            manage_loaded(str_mem)
        }

        return rootView
    }

    fun manage_loaded(mem_id: String) {
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("manage_loaded", " 123_456_789se " + mem_id)
        Log.i(
            "manage_loaded1",
            " 123_456_789se " + "https://brpatrick.beepxs.solutions/api/manageoffers?merchant_id=" + mem_id + "&status=1"
        )

        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Builder()
            .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getManage(mem_id, "1")


        call.enqueue(object : Callback<ManageOffResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<ManageOffResponse>,
                response: Response<ManageOffResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    //val data = response.body()!!.data
                    val results = response.body()!!.data.results

                    Log.i("coupon_jsonObject", " 123_456_789se " + results)
                    for (i in 0 until results.size) {
                        val jsonObject = results.get(i)

                        val id = jsonObject.id;
                        val title = jsonObject.title;
                        val merchant_name = jsonObject.merchant_name;
                        val start_on = jsonObject.start_on;
                        val end_on = jsonObject.end_on;
                        val offer_branch = jsonObject.offer_branch;
                        val branch_names = jsonObject.branch_names;
                        val coupon_count = jsonObject.coupon_count;
                        val code = jsonObject.code;
                        val coupon_grabbed = jsonObject.coupon_grabbed;
                        val used_coupon = jsonObject.used_coupon;
                        val status = jsonObject.status;
                        val img = jsonObject.img;

                        val description = jsonObject.description;
                        val offer_terms = jsonObject.offer_terms;

                        val map = java.util.HashMap<String, String>()
                        map["id"] = id
                        map["title"] = title
                        map["merchant_name"] = merchant_name
                        map["start_on"] = start_on
                        map["end_on"] = end_on
                        map["offer_branch"] = offer_branch
                        map["branch_names"] = branch_names
                        map["coupon_count"] = coupon_count
                        map["code"] = code
                        map["coupon_grabbed"] = coupon_grabbed
                        map["used_coupon"] = used_coupon
                        map["status"] = status
                        map["img"] = img

                        arrayListTrans.add(map)

                    }


                    Log.i("data_data", " 123_456_789se " + results)


                    progressDialog.dismiss()

                    manageAdapter = ManageAdapter(context, arrayListTrans)
                    recycler_list.adapter = manageAdapter
                    //manageAdapter.setOnItemClickListener(this@FragmentOngoing)

                   // recyclerView.adapter = Adapter(this, fetchList(), this)

                    txt_no_records.visibility = View.GONE
                } else if (str_error == 1) {
                    txt_no_records.visibility = View.VISIBLE
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<ManageOffResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------

/*    override fun onClick(pos: Int, aView: View) {



        val id = arrayListTrans.get(pos)["id"].toString()
        val title =  arrayListTrans.get(pos)["title"].toString()
        val merchant_name =  arrayListTrans.get(pos)["merchant_name"].toString()
        val start_on =  arrayListTrans.get(pos)["start_on"].toString()
        val end_on =  arrayListTrans.get(pos)["end_on"].toString()
        val offer_branch =  arrayListTrans.get(pos)["offer_branch"].toString()
        val branch_names =  arrayListTrans.get(pos)["branch_names"].toString()
        val coupon_count =  arrayListTrans.get(pos)["coupon_count"].toString()
        val code =  arrayListTrans.get(pos)["code"].toString()
        val coupon_grabbed =  arrayListTrans.get(pos)["coupon_grabbed"].toString()
        val used_coupon =  arrayListTrans.get(pos)["used_coupon"].toString()
        val status =  arrayListTrans.get(pos)["status"].toString()
        val img =  arrayListTrans.get(pos)["img"].toString()

        Log.i("id","qqqqqqqqqqqqq" + id)
        Log.i("title","qqqqqqqqqqqqq" + title)
        Log.i("merchant_name","qqqqqqqqqqqqq" + merchant_name)

        val intent = Intent(context, ManageDetails::class.java)
        intent.putExtra("id",id)
        intent.putExtra("title",title)
        intent.putExtra("merchant_name",merchant_name)
        intent.putExtra("start_on",start_on)
        intent.putExtra("end_on",end_on)
        intent.putExtra("offer_branch",offer_branch)
        intent.putExtra("branch_names",branch_names)
        intent.putExtra("coupon_count",coupon_count)
        intent.putExtra("code",code)
        intent.putExtra("coupon_grabbed",coupon_grabbed)
        intent.putExtra("used_coupon",used_coupon)
        intent.putExtra("status",status)
        intent.putExtra("img",img)
        startActivity(intent)

*//*        val ldf = ManageOfferDetails()
        val args = Bundle()
        args.putString("id", id)
        args.putString("title",title)
        args.putString("merchant_name",merchant_name)
        args.putString("start_on",start_on)
        args.putString("end_on",end_on)
        args.putString("offer_branch",offer_branch)
        args.putString("branch_names",branch_names)
        args.putString("coupon_count",coupon_count)
        args.putString("code",code)
        args.putString("coupon_grabbed",coupon_grabbed)
        args.putString("used_coupon",used_coupon)
        args.putString("status",status)
        args.putString("img",img)
        ldf.setArguments(args)*//*

 *//*       val fragment_m = ManageOfferDetails()
        val bundle = Bundle()
        bundle.putString("id", id)
        bundle.putString("title",title)
        bundle.putString("merchant_name",merchant_name)
        bundle.putString("start_on",start_on)
        bundle.putString("end_on",end_on)
        bundle.putString("offer_branch",offer_branch)
        bundle.putString("branch_names",branch_names)
        bundle.putString("coupon_count",coupon_count)
        bundle.putString("code",code)
        bundle.putString("coupon_grabbed",coupon_grabbed)
        bundle.putString("used_coupon",used_coupon)
        bundle.putString("status",status)
        bundle.putString("img",img)
        fragment_m.arguments = bundle

        fragmentClass_Menu(1)*//*

        Log.i("onclick_view","qqqqqqqqqqqqq")
    }*/

    fun fragmentClass_Menu(frag_check: Int){

        if(frag_check==1){
            fragmentClass = ManageOfferDetails::class.java
        }

        try {
            fragment = fragmentClass.newInstance() as Fragment
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // Insert the fragment by replacing any existing fragment
        val fragmentManager: FragmentManager = requireActivity().supportFragmentManager
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment!!).commit()
        }

    }
}