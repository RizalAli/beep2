package beepplus.bn.app.home.ui.marketplace

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import beepplus.bn.app.R
import beepplus.bn.app.auth_screen.LoginAuth
import beepplus.bn.app.home.ui.loyalty.ManageshowViewModel
import beepplus.bn.app.sql.DatabaseHandler
import com.google.android.material.tabs.TabLayout

class OrderDetails : Fragment() {

    var fragment: Fragment? = null
    lateinit var fragmentClass: Class<*>

    lateinit var chip_cloud: com.adroitandroid.chipcloud.ChipCloud
    lateinit var progress: ProgressBar
    private lateinit var manageshowViewModel: ManageshowViewModel
    lateinit var webview_load1: WebView
    lateinit var txt_user_name: TextView
    lateinit var btn1: Button;
    lateinit var btn2: Button;
    lateinit var btn3: Button;
    lateinit var btn4: Button;
    var str_mobile_no = LoginAuth.mobile_no
    var str_name = ""
    private val url = "https://brpatrick.beepxs.solutions/merchant/offer?merchant_id=2"
    //private val url = "https://www.graceonline.in/"
    lateinit var tabLayout: TabLayout
    lateinit var linear_add_offer: LinearLayout
    lateinit var viewPager: ViewPager
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        manageshowViewModel =
            ViewModelProviders.of(this).get(ManageshowViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_order_details, container, false)
        //val textView: TextView = root.findViewById(R.id.text_slideshow)
        manageshowViewModel.text.observe(viewLifecycleOwner, Observer {
            //textView.text = it

            //  webview_load1 = root.findViewById(R.id.webview_load1)

            Log.i("testing_purpose","----------------------------------")

            val isMem_Name = context?.let { it1 -> DatabaseHandler(it1) }
            val mem_name = isMem_Name?.isMem_name
            val sb_mem_name = StringBuilder()
            sb_mem_name.append("")
            sb_mem_name.append(mem_name)
            str_name = sb_mem_name.toString()

            tabLayout =  root.findViewById(R.id.tabLayout);
            viewPager =  root.findViewById(R.id.pager);


            tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#0064a7"));
            //tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
            tabLayout.setTabTextColors(Color.parseColor("#0064a7"), Color.parseColor("#0064a7"));

            setupViewPager(viewPager);

            tabLayout.setupWithViewPager(viewPager)

        })
        return root
    }


    fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(FragmentMpending(), "Pending") //Need to call first tab fragment
        adapter.addFragment(FragmentMinprogress(), "In progress") // Need second tab fragment and so on...
        adapter.addFragment(FragmentMicompleted(), "Completed") // Need second tab fragment and so on...
        viewPager.adapter = adapter
    }


    class ViewPagerAdapter(manager: FragmentManager?) : FragmentPagerAdapter(manager!!) {
        private val mFragmentList: MutableList<Fragment> =
            ArrayList()
        private val mFragmentTitleList: MutableList<String> = ArrayList()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    fun Context.toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

}