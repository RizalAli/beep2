package beepplus.bn.app.home.ui.ecomcatalogue

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.adapter.ManageAdapter
import beepplus.bn.app.adapter.OrderViewAdapter
import beepplus.bn.app.home.Home
import beepplus.bn.app.home.Home.Companion.manage_fragment_restart2
import beepplus.bn.app.sql.DatabaseHandler
import com.naruvis.ecom.pojo.ManageOffResponse
import com.naruvis.ecom.pojo.OrderviewResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.Retrofit.*
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl

class FragmentECOMinprogress : Fragment() {

    // 1 - 0 -8 // status  =  ongoing - pending - expired
    private var mLayoutManager: LinearLayoutManager? = null
    var str_mem = "";
    var seller_id = "43"
    var seller_obid = "43"
    lateinit var orderViewAdapter: OrderViewAdapter
    var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()
    lateinit var recycler_list: RecyclerView
    lateinit var txt_no_records: TextView

    private var rootView: View? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_list, container, false)
        val isMem_His = context?.let { DatabaseHandler(it) }
        val mem = isMem_His!!.isMem_His
        val sb_mem = StringBuilder()
        sb_mem.append("")
        sb_mem.append(mem)
        str_mem = sb_mem.toString()

        val isMem_obid = context?.let { it1 -> DatabaseHandler(it1) }
        val memoid = isMem_obid?.isMem_obid
        val sb_memobid = StringBuilder()
        sb_memobid.append("")
        sb_memobid.append(memoid)
        seller_obid = sb_memobid.toString()

        val isMem_kid = context?.let { it1 -> DatabaseHandler(it1) }
        val memkid = isMem_kid?.isMem_kid
        val sb_memkid = StringBuilder()
        sb_memkid.append("")
        sb_memkid.append(memkid)
        seller_id = sb_memkid.toString()

        Log.i("str_mem_str_mem", str_mem)
        recycler_list = rootView!!.findViewById(R.id.recycler_list)
        mLayoutManager = LinearLayoutManager(context)
        recycler_list.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recycler_list.addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayoutManager.VERTICAL
            )
        )
        recycler_list.layoutManager = mLayoutManager
        // txt_record_found = root.findViewById(R.id.txt_record_found)
        txt_no_records = rootView!!.findViewById(R.id.txt_no_records)

        if (!arrayListTrans.isEmpty()) {
            arrayListTrans.clear();
            manage_loaded(seller_id)
        } else {
            manage_loaded(seller_id)
        }

        return rootView
    }

    fun manage_loaded(s_id: String) {
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("manage_loaded", " 123_456_789se " + s_id)
        Log.i(
            "manage_loaded1",
            " 123_456_789se " + "https://onebrunei.ideablitztech.com/api/orderview?seller_id=" + s_id + "&status=1"
        )

        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        var URL = ""
        if(Home.manage_fragment_orders==1){
            URL = APIUrl.BASE_KONTENA_URL
            seller_id = seller_id;
        }else{
            URL = APIUrl.BASE_ONE_BRUNEI_URL
            seller_id = seller_obid;
        }

        val retrofit = Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getMarketpalce(seller_id , "1")


        call.enqueue(object : Callback<OrderviewResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<OrderviewResponse>,
                response: Response<OrderviewResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    if(!arrayListTrans.isEmpty()){
                        arrayListTrans.clear()
                    }
                    //val data = response.body()!!.data
                    val results = response.body()!!.data

                    Log.i("coupon_jsonObject", " 123_456_789se " + results)
                    for (i in 0 until results.size) {
                        val jsonObject = results.get(i)

                        val order_id = jsonObject.order_id;
                        val seller_id = jsonObject.seller_id;
                        val cus_name = jsonObject.cus_name;
                        val product_name = jsonObject.product_name;
                        val total_qty = jsonObject.total_qty;
                        val total_price = jsonObject.total_pricesel;
                        val created_at = jsonObject.created_at;
                        val status = jsonObject.status;
                        val image = jsonObject.image;
                        val tbl_no = jsonObject.tbl_no;


                        val map = java.util.HashMap<String, String>()
                        map["order_id"] = order_id
                        map["seller_id"] = seller_id
                        map["cus_name"] = cus_name
                        map["product_name"] = product_name
                        map["total_qty"] = total_qty
                        map["total_price"] = total_price
                        map["created_at"] = created_at
                        map["status"] = status
                        map["image"] = image
                        map["tbl_no"] = tbl_no


                        arrayListTrans.add(map)

                    }


                    Log.i("data_data", " 123_456_789se " + results)


                    progressDialog.dismiss()

                    orderViewAdapter = OrderViewAdapter(context, arrayListTrans)
                    recycler_list.adapter = orderViewAdapter

                    txt_no_records.visibility = View.GONE
                } else if (str_error == 1) {
                    txt_no_records.visibility = View.VISIBLE
                    orderViewAdapter = OrderViewAdapter(context, arrayListTrans)
                    orderViewAdapter.notifyDataSetChanged()
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<OrderviewResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------
    override fun onResume() {
        super.onResume()

        Log.i("FragmentECOMinprogress", "=================================" + "FragmentECOMinprogress")
        if(manage_fragment_restart2 ==2){

            if (!arrayListTrans.isEmpty()) {
                arrayListTrans.clear();
                manage_loaded(seller_id)
            } else {
                manage_loaded(seller_id)
            }

            manage_fragment_restart2 ==0
        }
        /* if (alert.isShowing()) {
            alert.dismiss();
        }*/Log.i("on_the_restart", "=================================" + "FragmentECOMinprogress")
    }
}