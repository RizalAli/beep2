package beepplus.bn.app.home.ui.loyalty.issuespoints

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import beepplus.bn.app.R
import kotlinx.android.synthetic.main.activity_issues_points_success.*

class IssuesPointsSuccess : AppCompatActivity() {
    var bundle: Bundle? = null
    var amount = "";
    var show_date = "";

    var transaction_id = "";
    var xs_point = "";

    var user_name = "";
    var user_id = "";

    var status = "";
    var errmsg = "";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_issues_points_success)

        bundle = getIntent().getExtras();

        if (bundle != null) {
            amount = bundle!!.getString("amount").toString();
            show_date = bundle!!.getString("show_date").toString();
            transaction_id = bundle!!.getString("transaction_id").toString();
            xs_point = bundle!!.getString("xs_point").toString();
            user_name = bundle!!.getString("user_name").toString();
            user_id = bundle!!.getString("user_id").toString();
            status = bundle!!.getString("status").toString();
            Log.i("FasTesting_str_from1","str_from-------------------------" + transaction_id)
        }else {
            Log.i("Testing_str_from2","str_from-------------------------" + transaction_id)

        }


        if(status=="1"){

            txt_xs_points.text = xs_point+"XS"+" " +"Points"
            txt_xs_message.text = "ISSUED SUCCESSFUL!"
            img_success_error.setImageResource(R.drawable.ic_baseline_check_circle_outline_green_24)

            txt_amt.text = "$"+amount
            txt_date.text = show_date
            txt_trans_id.text = transaction_id
            txt_Loyalty_points.text = xs_point +" " +"XS"
            txt_user_name.text = user_name
            txt_user_id.text = user_id

        }else if(status=="8"){

        }else{

            img_success_error.setImageResource(R.drawable.ic_baseline_highlight_off_red_24)
            txt_xs_points.text = ""
            txt_xs_message.text = "ISSUED FAILED!"

        }

        btn_issues_points.setOnClickListener { v->
            onBackPressed()

        }

        image_back_issue.setOnClickListener { v->
            onBackPressed()
        }

    }
}