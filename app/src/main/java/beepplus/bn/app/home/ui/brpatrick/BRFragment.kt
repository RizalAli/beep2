package beepplus.bn.app.home.ui.brpatrick

import android.app.Activity.RESULT_CANCELED
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import beepplus.bn.app.R
import beepplus.bn.app.auth_screen.LoginAuth
import beepplus.bn.app.home.ui.gallery.GalleryFragment
import beepplus.bn.app.home.ui.home.HomeFragment
import beepplus.bn.app.scanner.*
import beepplus.bn.app.settings.ChangePassword
import beepplus.bn.app.sql.DatabaseHandler


class BRFragment : Fragment() {

  lateinit var chip_cloud: com.adroitandroid.chipcloud.ChipCloud
  lateinit var progress:ProgressBar
  private lateinit var slideshowViewModel: BRshowViewModel
  lateinit var webview_load1:WebView
  lateinit var txt_user_name:TextView
  lateinit var btn1:Button;
  lateinit var btn2:Button;
  lateinit var btn3:Button;
  lateinit var btn4:Button;
  var str_mobile_no = LoginAuth.mobile_no
  var str_name = ""
  private val url = "https://brpatrick.beepxs.solutions/merchant/offer?merchant_id=2"
    //private val url = "https://www.graceonline.in/"

  var fragment: Fragment? = null
  lateinit var fragmentClass: Class<*>

    /* var str_symbol = "$"
    lateinit var recycler_trans: RecyclerView
    lateinit var mLayoutManager: LinearLayoutManager
    lateinit var transAdapter: TransAdapter
    var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()
*/
    //lateinit var webview_load1: WebView


  override fun onCreateView(
          inflater: LayoutInflater,
          container: ViewGroup?,
          savedInstanceState: Bundle?
  ): View? {

    slideshowViewModel =
            ViewModelProviders.of(this).get(BRshowViewModel::class.java)
    val root = inflater.inflate(R.layout.fragment_brpatrick, container, false)
    //val textView: TextView = root.findViewById(R.id.text_slideshow)
    slideshowViewModel.text.observe(viewLifecycleOwner, Observer {
      //textView.text = it
        //val myWebView: WebView = root.findViewById(R.id.webview)

     //   chip_cloud = root.findViewById(R.id.chip_cloud)
      webview_load1 = root.findViewById(R.id.webview_load1)
        progress = root.findViewById(R.id.progress)

      val isMem_Name = context?.let { it1 -> DatabaseHandler(it1) }
      val mem_name = isMem_Name?.isMem_name
      val sb_mem_name = StringBuilder()
      sb_mem_name.append("")
      sb_mem_name.append(mem_name)
      str_name = sb_mem_name.toString()

      txt_user_name = root.findViewById(R.id.txt_user_name)
      btn1 = root.findViewById(R.id.btn1)
      btn2 = root.findViewById(R.id.btn2)
      btn3 = root.findViewById(R.id.btn3)
      btn4 = root.findViewById(R.id.btn4)

      txt_user_name.setText(str_name)

      btn1.setOnClickListener { v->

       /* val intent = Intent(context, ScanActivity::class.java)
        intent.putExtra("from","1");
        startActivity(intent)*/

        val intent = Intent(context, AddOffer::class.java)
        //intent.putExtra("from","1");
        startActivity(intent)

      }

      btn2.setOnClickListener { v->

        val intent = Intent(context, ManageOrder::class.java)
        startActivity(intent)

          
       //   Log.i("THEEKUCHI----------------",""+"2")
       /* val intent = Intent(context, ScanActivity::class.java)
        intent.putExtra("from","2");
        startActivity(intent)*/
      }

      btn3.setOnClickListener { v->
        val intent = Intent(context, ScanActivity::class.java)
        intent.putExtra("from","3");
        startActivity(intent)
      }

        btn4.setOnClickListener { v->

            /*val intent = Intent(context, History::class.java)
            //intent.putExtra("from","4");
            startActivity(intent)*/

          fragmentClass_Menu()

        }



        /* val webSetting: WebSettings = myWebView.getSettings()
         webSetting.builtInZoomControls = true
         webSetting.javaScriptEnabled = true
         myWebView.setWebViewClient(WebViewClient())
         //myWebView.loadUrl("http://www.javapapers.com")
         myWebView.loadUrl("https://pasarpbg.com")
         myWebView.settings.javaScriptEnabled = true
         myWebView.setHorizontalScrollBarEnabled(true);
         myWebView.setVerticalScrollBarEnabled(true);*/

        // Get the web view settings instance
        val settings = webview_load1.settings

        // Enable java script in web view
        settings.javaScriptEnabled = true

        // Enable and setup web view cache
        settings.setAppCacheEnabled(true)
        settings.cacheMode = WebSettings.LOAD_DEFAULT
        //settings.setAppCachePath(cacheDir.path)


        // Enable zooming in web view
        settings.setSupportZoom(true)
        settings.builtInZoomControls = true
        settings.displayZoomControls = true


        // Zoom web view text
        settings.textZoom = 125


        // Enable disable images in web view
        settings.blockNetworkImage = false
        // Whether the WebView should load image resources
        settings.loadsImagesAutomatically = true


        // More web view settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            settings.safeBrowsingEnabled = true  // api 26
            settings.setPluginState(WebSettings.PluginState.ON);
        }
        //settings.pluginState = WebSettings.PluginState.ON
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.mediaPlaybackRequiresUserGesture = false
        settings.setPluginState(WebSettings.PluginState.ON);

        // More optional settings, you can enable it by yourself
        settings.domStorageEnabled = true
        settings.setSupportMultipleWindows(true)
        settings.loadWithOverviewMode = true
        settings.allowContentAccess = true
        settings.setGeolocationEnabled(true)
        settings.allowUniversalAccessFromFileURLs = true
        settings.allowFileAccess = true


        // WebView settings
      webview_load1.fitsSystemWindows = true


        /*
            if SDK version is greater of 19 then activate hardware acceleration
            otherwise activate software acceleration
        */
      webview_load1.setLayerType(View.LAYER_TYPE_HARDWARE, null)


        // Set web view client
      webview_load1.webViewClient = object: WebViewClient(){
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                // Page loading started
                // Do something
//                context?.toast("Page loading.")

             // progress. visibility = View.VISIBLE

                // Enable disable back forward button
                //button_back.isEnabled = web_view.canGoBack()
                //button_forward.isEnabled = web_view.canGoForward()
            }

            override fun onPageFinished(view: WebView, url: String) {
                // Page loading finished
                // Display the loaded page title in a toast message
//              view.destroy()
//                context!!.toast("Page loaded: ${view.title}")

            //  progress. visibility = View.GONE

                // Enable disable back forward button
                //button_back.isEnabled = web_view.canGoBack()
                //button_forward.isEnabled = web_view.canGoForward()
            }
        }

        Log.i("Tested_mobile_network","oooooooo" + url)

      webview_load1.loadUrl(url)

      webview_load1.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
        if (event.action === KeyEvent.ACTION_DOWN) {
          if (keyCode == KeyEvent.KEYCODE_BACK && webview_load1.canGoBack()) {  //表示按返回键
            webview_load1.goBack() //后退
            //webview.goForward();//前进
            return@OnKeyListener true //已处理
          }
        }
        false
      })

    })
    return root
  }

  fun fragmentClass_Menu(){

      fragmentClass = GalleryFragment::class.java

    try {
      fragment = fragmentClass.newInstance() as Fragment
    } catch (e: Exception) {
      e.printStackTrace()
    }
    // Insert the fragment by replacing any existing fragment
    val fragmentManager: FragmentManager = requireActivity().supportFragmentManager
    if (fragment != null) {
      fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment!!).commit()
    }


  }

    fun Context.toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }


}