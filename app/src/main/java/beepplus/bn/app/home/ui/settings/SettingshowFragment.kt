package beepplus.bn.app.home.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.adapter.SettingAdapter


class SettingshowFragment : Fragment() {

    private lateinit var slideshowViewModel: SettingsshowViewModel
    val name_arrays = arrayOf(
        "Payment Methods",
        "Bank Accounts",
        "Change Password"
    )
    var image_arrays = arrayOf<Int>(
        R.drawable.ic_baseline_payment_24,
        R.drawable.ic_baseline_account_balance_24,
        R.drawable.ic_baseline_autorenew_24
    )

    lateinit var recycler_settings: RecyclerView
    lateinit var mLayoutManager: LinearLayoutManager
    lateinit var settingAdapter: SettingAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        slideshowViewModel =
            ViewModelProviders.of(this).get(SettingsshowViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_settings, container, false)
        //val textView: TextView = root.findViewById(R.id.text_slideshow)
        slideshowViewModel.text.observe(viewLifecycleOwner, Observer {
            //textView.text = it
            recycler_settings = root.findViewById(R.id.recycler_settings)
            mLayoutManager = LinearLayoutManager(context)
            recycler_settings.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            recycler_settings.addItemDecoration(
                DividerItemDecoration(
                    recycler_settings.getContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            recycler_settings.layoutManager = mLayoutManager

            val adapter = SettingAdapter(context,image_arrays,name_arrays);
            recycler_settings.adapter = adapter
        })
        return root
    }
}