package beepplus.bn.app.home.profile

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import beepplus.bn.app.R
import beepplus.bn.app.common.BackgroundNotificationService
import com.bumptech.glide.Glide
import com.naruvis.ecom.pojo.ProfileResponse
import kotlinx.android.synthetic.main.activity_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl
import java.io.File


class ProfileActivity : AppCompatActivity() {

    var seller_id = "42"
    var share_link = ""
    var qrlink = ""

    companion object {
        var PROGRESS_UPDATE = "progress_update"
        var PERMISSION_REQUEST_CODE = 1
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)


        image_back.setOnClickListener {
            onBackPressed()
        }

        share1.setOnClickListener {
            share()
        }
        share2.setOnClickListener {
            share()
        }
        share3.setOnClickListener {
            share()
        }
        share4.setOnClickListener {
            share()
        }

        profile_loaded(seller_id)

        img_qr.setOnClickListener {
            showDialog()
        }

        btn_download.setOnClickListener {
            showDialog()
            //showDialog()
           /* if (checkPermission()) {
                startImageDownload();
            } else {
                requestPermission();
            }*/

        }
        //registerReceiver()
    }

    fun share(){
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, share_link)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }

    fun profile_loaded(s_id:String){
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("manage_loaded"," 123_456_789se " + s_id)
        Log.i("manage_loaded"," 123_456_789se " + "https://onebrunei.ideablitztech.com/api/seller?seller_id="+s_id)


        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_ONE_BRUNEI_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getProfile(s_id)


        call.enqueue(object : Callback<ProfileResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<ProfileResponse>, response: Response<ProfileResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    //val data = response.body()!!.data
                    val results = response.body()!!.data

                    Log.i("coupon_jsonObject"," 123_456_789se " + results)
            //        for (i in 0 until results.size){
            //            val jsonObject = results.get(i)

                        val seller_name = results.seller_name;
                        val description = results.description;
                        val logo = results.logo;

                        val str_map = results.map;
                        val facebook = results.facebook;
                        val instagram = results.instagram;

                        qrlink = results.qrlink;
                        val banner = results.banner;
                        val url = results.url;

                    share_link = url

                    Glide.with(this@ProfileActivity)
                        .load(banner)
                        .into(image_banner)

                    Glide.with(this@ProfileActivity)
                        .load("https://onebrunei.ideablitztech.com/uploads/"+logo)
                        .into(img_logo)

                    txt_name.setText(seller_name)
                    txt_name_des.setText(description)

                    txt_name1.setText(str_map)
                    txt_name2.setText(facebook)
                    txt_name3.setText(instagram)

                    txt_url.setText(url)

                    Glide.with(this@ProfileActivity)
                        .load(qrlink)
                        .into(img_qr)

                       /* val map = java.util.HashMap<String, String>()
                        map["seller_name"] = seller_name
                        map["description"] = description
                        map["logo"] = logo

                        map["str_map"] = str_map
                        map["facebook"] = facebook
                        map["instagram"] = instagram

                        map["qrlink"] = qrlink
                        map["banner"] = banner
                        map["url"] = url*/

                        //arrayListTrans.add(map)





                    Log.i("data_data"," 123_456_789se " + results)

                    progressDialog.dismiss()



                }else if(str_error==1){

                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------

    private fun showDialog() {
        val dialog = Dialog(this@ProfileActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_layout)

        val image_v= dialog.findViewById(R.id.image_v) as ImageView
        //body.text = title

        Glide.with(this@ProfileActivity)
            .load(qrlink)
            .into(image_v)

        val noBtn = dialog.findViewById(R.id.txt_no) as TextView
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        return result == PackageManager.PERMISSION_GRANTED
    } // checkPermission
    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            PERMISSION_REQUEST_CODE
        )
    } // requestPermission


    private fun registerReceiver() {
        val bManager = LocalBroadcastManager.getInstance(this)
        val intentFilter = IntentFilter()
        intentFilter.addAction(PROGRESS_UPDATE)
        bManager.registerReceiver(mBroadcastReceiver, intentFilter)
    }

    private val mBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            if (intent.action == PROGRESS_UPDATE) {
                val downloadComplete =
                    intent.getBooleanExtra("downloadComplete", false)
                //Log.d("API123", download.getProgress() + " current progress");
                if (downloadComplete) {
                    Toast.makeText(
                        applicationContext,
                        "File download completed",
                        Toast.LENGTH_SHORT
                    ).show()
                    val file = File(
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                            .getPath() + File.separator.toString() +
                                "journaldev-image-downloaded.jpg"
                    )
                    // Picasso.get().load(file).into(imageView)
                }
            }
        }
    }// mBroadcastReceiver

    private fun startImageDownload() {
        val intent = Intent(this, BackgroundNotificationService::class.java)
        startService(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //called when user presses ALLOW or DENY from Permission Request Popup
        when(requestCode){

            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startImageDownload()
            } else {
                Toast.makeText(applicationContext, "Permission Denied", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }



}