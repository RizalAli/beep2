package beepplus.bn.app.home.ui.partners

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import beepplus.bn.app.R
import beepplus.bn.app.auth_screen.OTPAuth
import beepplus.bn.app.partners.WebAct
import beepplus.bn.app.partners.WebActivity


class PartnersFragment : Fragment() {

  private lateinit var slideshowViewModel: PartnersViewModel
    lateinit var webview: WebView
  override fun onCreateView(
          inflater: LayoutInflater,
          container: ViewGroup?,
          savedInstanceState: Bundle?
  ): View? {
    slideshowViewModel =
            ViewModelProviders.of(this).get(PartnersViewModel::class.java)
    val root = inflater.inflate(R.layout.fragment_partners, container, false)
    //val textView: TextView = root.findViewById(R.id.text_slideshow)
    slideshowViewModel.text.observe(viewLifecycleOwner, Observer {
      //textView.text = it

        val rel1: RelativeLayout = root.findViewById(R.id.rel1)
        val rel2: RelativeLayout = root.findViewById(R.id.rel2)
        val rel3: RelativeLayout = root.findViewById(R.id.rel3)

        rel1.setOnClickListener { v->
            val intent = Intent(context, WebAct::class.java)
            startActivity(intent)
        }


      /*  val webSetting: WebSettings = myWebView.getSettings()
        webSetting.builtInZoomControls = true
        webSetting.javaScriptEnabled = true
        myWebView.setWebViewClient(WebViewClient())
        //myWebView.loadUrl("http://www.javapapers.com")
        myWebView.loadUrl("https://pasarpbg.com")
        myWebView.settings.javaScriptEnabled = true
        myWebView.setHorizontalScrollBarEnabled(true);
        myWebView.setVerticalScrollBarEnabled(true);*/

    })
    return root
  }
}