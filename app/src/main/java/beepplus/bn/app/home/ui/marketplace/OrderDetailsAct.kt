package beepplus.bn.app.home.ui.marketplace

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.adapter.OrderViewAdapter
import beepplus.bn.app.adapter.ProductDetAdapter
import beepplus.bn.app.sql.DatabaseHandler
import com.bumptech.glide.Glide
import com.naruvis.ecom.pojo.OrderResponse
import com.naruvis.ecom.pojo.OrderviewResponse
import kotlinx.android.synthetic.main.activity_order_details.*
import kotlinx.android.synthetic.main.fragment_order_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl
import java.text.SimpleDateFormat
import java.util.*

class OrderDetailsAct : AppCompatActivity() {

    var str_name = ""
    var str_order_id = ""
    var seller_id = "42"
    lateinit var productDetAdapter: ProductDetAdapter
    var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()
    var bundle: Bundle? = null
    var str_dollar = "$"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_details)

        val isMem_name = DatabaseHandler(applicationContext)
        val mem_name = isMem_name.isMem_name
        val sb_mem_n = StringBuilder()
        sb_mem_n.append("")
        sb_mem_n.append(mem_name)
        str_name = sb_mem_n.toString()

        Log.i("str_name----------",str_name)

        var mLayoutManager: LinearLayoutManager? = null

        /*val toolbar: Toolbar = findViewById(R.id.toolbar_ord)
        setSupportActionBar(toolbar)*/

        toolbar_ord.title = str_name

        toolbar_ord.setNavigationIcon(R.drawable.ic_baseline_arrow_small_white_24)
        toolbar_ord.setNavigationOnClickListener { v->
            onBackPressed()
        }
        //recycler_list

        bundle = getIntent().getExtras();

        if (bundle != null) {
            str_order_id = bundle!!.getString("order_id").toString();
            Log.i("Testing_str_from1","str_from-------------------------" + str_order_id)
        }else {
            Log.i("Testing_str_from2","str_from-------------------------" + str_order_id)

        }


        mLayoutManager = LinearLayoutManager(this@OrderDetailsAct)
        recycler_list.layoutManager =
            LinearLayoutManager(this@OrderDetailsAct, LinearLayoutManager.VERTICAL, false)
        recycler_list.addItemDecoration(
            DividerItemDecoration(
                this@OrderDetailsAct,
                LinearLayoutManager.VERTICAL
            )
        )
        recycler_list.layoutManager = mLayoutManager

        change_status.setOnClickListener { v->

        }

        details_loaded(seller_id,str_order_id)
    }


    private fun showDialog() {
        val dialog = Dialog(this@OrderDetailsAct)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_layout)

        val image_v= dialog.findViewById(R.id.image_v) as ImageView
        image_v.visibility = View.GONE

        val txt_no= dialog.findViewById(R.id.txt_no) as TextView
        txt_no.visibility = View.GONE

        txt_no
        //body.text = title

       /* Glide.with(this@OrderDetailsAct)
            .load(qrlink)
            .into(image_v)*/

        val noBtn = dialog.findViewById(R.id.txt_no) as TextView
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

    fun details_loaded(s_id: String,o_id:String) {
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("manage_loaded", " 123_456_789se " + s_id)
        Log.i(
            "manage_loaded1",
            " 123_456_789se " + "https://onebrunei.ideablitztech.com/api/orderview?seller_id=" + s_id + "&status=0"
        )

        val progressDialog = ProgressDialog(this@OrderDetailsAct)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_ONE_BRUNEI_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getOrderDetails(seller_id , o_id)


        call.enqueue(object : Callback<OrderResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<OrderResponse>,
                response: Response<OrderResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    //val data = response.body()!!.data
                    val results = response.body()!!.data


                    val s_order_id = results.orderdetails.order_id;
                    val created_at = results.orderdetails.created_at;
                    val total_qty = results.orderdetails.total_qty;
                    val total_price = results.orderdetails.total_price;
                    val total_product = results.orderdetails.total_product;
                    val cus_name = results.orderdetails.cus_name;
                    val cus_mob = results.orderdetails.cus_mob;
                    val s_status = results.orderdetails.status;

                    val orderitems = results.orderitems;

                    order_id.setText("Order id : " + "#" + s_order_id)

                    if(s_status=="0"){
                        status.setText("Status :" + " " +"Pending")
                    }else if(s_status=="7"){
                        status.setText("Status :" + " " +"Completed")
                    }else{
                        status.setText("Status :" + " " +"In Progress")
                    }



                    var format = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                    val newDate: Date = format.parse(created_at)

                    format = SimpleDateFormat("MMM dd,yyyy hh:mm a")
                    val date = format.format(newDate)

                    txt_date.setText(date)

                    name.setText("Name : " + " " + cus_name)
                    mobile_no.setText("Mobile Number : " + " " + "+673"+ " " + cus_mob)


                    Log.i("coupon_jsonObject", " 123_456_789se " + results)
                    for (i in 0 until orderitems.size) {
                        val jsonObject = orderitems.get(i)

                        val product_name = jsonObject.product_name;
                        val unit_name = jsonObject.unit_name;
                        val qty = jsonObject.qty;
                        val total_price = jsonObject.total_price;
                        val image = jsonObject.image;
                        val category_id = jsonObject.category_id;

                        val map = java.util.HashMap<String, String>()
                        map["product_name"] = product_name
                        map["unit_name"] = unit_name
                        map["qty"] = qty
                        map["total_price"] = total_price
                        map["image"] = image
                        map["category_id"] = category_id

                        arrayListTrans.add(map)

                    }

                    Log.i("data_data", " 123_456_789se " + results)

                    progressDialog.dismiss()

                    productDetAdapter = ProductDetAdapter(this@OrderDetailsAct, arrayListTrans)
                    recycler_list.adapter = productDetAdapter

                    total_amount.setText("Total Amount : " +str_dollar + "" +  total_price)

                 //   txt_no_records.visibility = View.GONE
                } else if (str_error == 1) {
                 //  txt_no_records.visibility = View.VISIBLE
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------
}