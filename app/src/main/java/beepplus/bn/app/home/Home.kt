package beepplus.bn.app.home

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.BitmapFactory
import android.graphics.drawable.LayerDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import beepplus.bn.app.R
import beepplus.bn.app.auth_screen.LoginAuth
import beepplus.bn.app.common.BadgeDrawable
import beepplus.bn.app.common.FilePath
import beepplus.bn.app.common.IOnBackPressed
import beepplus.bn.app.home.pick_merchant.PickMerchant
import beepplus.bn.app.home.profile.ProfileActivity
import beepplus.bn.app.home.ui.addproduct.Addprofragment
import beepplus.bn.app.home.ui.addproduct.ManageProducts
import beepplus.bn.app.home.ui.ecomcatalogue.Catalogue
import beepplus.bn.app.home.ui.ecomcatalogue.OrderManageEcom
import beepplus.bn.app.home.ui.gallery.GalleryFragment
import beepplus.bn.app.home.ui.home.HomeFragment
import beepplus.bn.app.home.ui.loyalty.LoyaltyFragment
import beepplus.bn.app.home.ui.marketplace.MarketPlaceList
import beepplus.bn.app.sql.DatabaseHandler
import com.bumptech.glide.Glide
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.naruvis.ecom.pojo.*
import kotlinx.android.synthetic.main.activity_settings.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl
import java.io.File


class Home : AppCompatActivity() { //Test upgrade

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navView: NavigationView
    private lateinit var text_h1:TextView
    private lateinit var txt_log_out1:TextView;
    var str_email = ""
    var str_mem = ""
    var str_name = ""
    var str_m_name = ""

    var str_mem_obid = ""
    var str_mem_kid = ""
    var str_mem_brid = ""

    private lateinit var drawerLayout:DrawerLayout;
    var fragment: Fragment? = null
    lateinit var fragmentClass: Class<*>
    var PERMISSION_CODE = 1000;
    var IMAGE_CAPTURE_CODE = 1001
    var image_uri: Uri? = null
    var IMAGE_PICK_CODE = 1000;
    var mediaPath = ""
    lateinit var imageView1: ImageView
    lateinit var imageView: de.hdodenhof.circleimageview.CircleImageView

    companion object {
        var cart_count = "0"
        var count_restart_rating_reviews = 0
        var doubleBackToExitPressedOnce = false
        var manage_fragment_restart1 = 0
        var manage_fragment_restart2 = 0
        var manage_fragment_restart3 = 0
        var manage_fragment_orders = 0

    }

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

       /* val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }*/

        val isMem_His = DatabaseHandler(applicationContext)
        val mem = isMem_His.isMem_His
        val sb_member = StringBuilder()
        sb_member.append("")
        sb_member.append(mem)
        str_mem = sb_member.toString()

        val isMem_Email = DatabaseHandler(applicationContext)
        val mem_email = isMem_Email.isMem_email
        val sb_mem = StringBuilder()
        sb_mem.append("")
        sb_mem.append(mem_email)
        str_email = sb_mem.toString()

        val isMem_name = DatabaseHandler(applicationContext)
        val mem_name = isMem_name.isMem_name
        val sb_mem_n = StringBuilder()
        sb_mem_n.append("")
        sb_mem_n.append(mem_name)
        str_name = sb_mem_n.toString()

        val isMem_obid = DatabaseHandler(applicationContext)
        val mem_obid = isMem_obid.isMem_obid
        val sb_mem_obid = StringBuilder()
        sb_mem_obid.append("")
        sb_mem_obid.append(mem_obid)
        str_mem_obid = sb_mem_obid.toString()

        val isMem_kid = DatabaseHandler(applicationContext)
        val mem_kid = isMem_kid.isMem_kid
        val sb_mem_kid = StringBuilder()
        sb_mem_kid.append("")
        sb_mem_kid.append(mem_kid)
        str_mem_kid = sb_mem_kid.toString()

        val isMem_brid = DatabaseHandler(applicationContext)
        val mem_brid = isMem_brid.isMem_brid
        val sb_mem_brid = StringBuilder()
        sb_mem_brid.append("")
        sb_mem_brid.append(mem_brid)
        str_mem_brid = sb_mem_brid.toString()

        val isMem_m_name = DatabaseHandler(applicationContext)
        val mem_m_name= isMem_m_name.isMem_merchant_name
        val sb_mem_m_name = StringBuilder()
        sb_mem_m_name.append("")
        sb_mem_m_name.append(mem_m_name)
        str_m_name = sb_mem_m_name.toString()

        Log.i("str_mem_brid===","------------------------------")
        Log.i("str_mem_obid",str_mem_obid)
        Log.i("str_mem_kid",str_mem_kid)
        Log.i("str_mem_brid",str_mem_brid)


        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        imageView = findViewById(R.id.imageView)
        val text_m_name:TextView = findViewById(R.id.text_m_name)
        val text_h1: TextView = findViewById(R.id.text_h1)
        val textView: TextView = findViewById(R.id.textView)

        text_m_name.setText(str_m_name)
        text_h1.setText(str_name)
        textView.setText("+673" + " " + str_email)

        val home_txt: TextView = findViewById(R.id.home_txt)
 //       val transaction_txt: TextView = findViewById(R.id.transaction_txt)
        val ecommerce_txt: TextView = findViewById(R.id.ecommerce_txt)

        val linear_ecom: LinearLayout = findViewById(R.id.linear_ecom)
        val linear_partners: LinearLayout = findViewById(R.id.linear_partners)

        val partner_txt: TextView = findViewById(R.id.partner_txt)

        val loyalty_txt: TextView = findViewById(R.id.loyalty_txt)
        val one_brunei_txt: TextView = findViewById(R.id.one_brunei_txt)
        val linear_onebrunei: LinearLayout = findViewById(R.id.linear_onebrunei)


        val pick_txt: TextView = findViewById(R.id.pick_txt)
        val logout: TextView = findViewById(R.id.logout)

        val add_pro_txt: TextView = findViewById(R.id.add_pro_txt)
        val marketplace_txt: TextView = findViewById(R.id.marketplace_txt)
        val manage_pro_txt: TextView = findViewById(R.id.manage_pro_txt)
        val profile_txt: TextView = findViewById(R.id.profile_txt)
        val order_manage_txt: TextView = findViewById(R.id.order_manage_txt)
        val cat_manage_txt:TextView = findViewById(R.id.cat_manage_txt)

        val setting_txt:TextView = findViewById(R.id.setting_txt)

        toolbar.title = str_name
        home_txt.setOnClickListener { v->

            toolbar.title = str_name
            fragmentClass_Menu(1);

        }

        loyalty_txt.setOnClickListener { v->

            fragmentClass_Menu(3);
            toolbar.title = "Loyalty"

            //alert_loyalty()

            /*  val intent = Intent(this@Home, LoyaltyDrawer::class.java)
              startActivity(intent)*/

        }

        one_brunei_txt.setOnClickListener { v->

            toolbar.title = str_name

            if(linear_onebrunei.visibility == View.VISIBLE){ //
                linear_onebrunei.visibility = View.GONE
                one_brunei_txt.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_baseline_keyboard_arrow_down_white_24,
                    0
                );
            }else{
                linear_onebrunei.visibility = View.VISIBLE
                one_brunei_txt.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_baseline_keyboard_arrow_up_white_24,
                    0
                );
            }

        }

        ecommerce_txt.setOnClickListener { v->

            toolbar.title = str_name

            if(linear_ecom.visibility == View.VISIBLE){ //
                linear_ecom.visibility = View.GONE
                ecommerce_txt.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_baseline_keyboard_arrow_down_white_24,
                    0
                );
            }else{
                linear_ecom.visibility = View.VISIBLE
                ecommerce_txt.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_baseline_keyboard_arrow_up_white_24,
                    0
                );
            }
            //fragmentClass_Menu(2);
        }

        order_manage_txt.setOnClickListener { v->
            toolbar.title = str_name
            /*fragmentClass_Menu(9);
            toolbar.title = str_name*/

            /*  val intent = Intent(this@Home, LoyaltyDrawer::class.java)
              startActivity(intent)*/

            alert_loyalty()

        }
        cat_manage_txt.setOnClickListener { v->
            toolbar.title = str_name
           /* fragmentClass_Menu(8);
            toolbar.title = str_name*/

            /*  val intent = Intent(this@Home, LoyaltyDrawer::class.java)
              startActivity(intent)*/

            alert_loyalty()

        }
       /* partner_txt.setOnClickListener { v->

            if(linear_partners.visibility == View.VISIBLE){
                linear_partners.visibility = View.GONE
                partner_txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_keyboard_arrow_down_white_24, 0);
            }else{
                linear_partners.visibility = View.VISIBLE
                partner_txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_keyboard_arrow_up_white_24, 0);
            }
            //fragmentClass_Menu(2);
        }*/
       /* transaction_txt.setOnClickListener { v->
            //fragmentClass_Menu(2);
        }*/

        add_pro_txt.setOnClickListener { v->
            toolbar.title = str_name
            fragmentClass_Menu(4);
            toolbar.title = str_name
            /*  val intent = Intent(this@Home, LoyaltyDrawer::class.java)
              startActivity(intent)*/

        }

        marketplace_txt.setOnClickListener { v->
            toolbar.title = str_name
            fragmentClass_Menu(5);
            toolbar.title = str_name
        }

        manage_pro_txt.setOnClickListener { v->
            toolbar.title = str_name
            fragmentClass_Menu(6);
            toolbar.title = str_name
        }

        profile_txt.setOnClickListener { v->
            toolbar.title = str_name
            fragmentClass_Menu(7);
            toolbar.title = str_name
        }


        pick_txt.setOnClickListener { v->
            val intent = Intent(this@Home, PickMerchant::class.java)
            startActivity(intent)
        }
        logout.setOnClickListener { v->
            toolbar.title = str_name
            logout_home()
        }

        setting_txt.setOnClickListener { v->
            val intent = Intent(this@Home, beepplus.bn.app.settings.SettingsCommon::class.java)
            startActivity(intent)
        }

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.home_txt,R.id.transaction_txt,R.id.loyalty_txt
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        imageView.setImageResource(R.drawable.ic_baseline_account_circle_24)
        imageView.setOnClickListener { v->

            showCustomDialog()

        }

        //navView.setCheckedItem(1)
     /*   val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.setDrawerListener(toggle)
        toggle.syncState()
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView*/
       //navigationView.setNavigationItemSelectedListener(this@Home)


        if(str_mem_obid=="0"){
            one_brunei_txt.visibility = View.GONE
            linear_onebrunei.visibility = View.GONE

        }else{
            one_brunei_txt.visibility = View.VISIBLE
            linear_onebrunei.visibility = View.VISIBLE
        }

        if(str_mem_kid=="0"){
            loyalty_txt.visibility = View.GONE
        }else{
            loyalty_txt.visibility = View.VISIBLE
        }

        if(str_mem_brid=="0"){
            loyalty_txt.visibility = View.GONE
        }else{
            loyalty_txt.visibility = View.VISIBLE
        }

        member_details()


    }

    fun fragmentClass_Menu(frag_check: Int){

        if(frag_check==1){
            fragmentClass = HomeFragment::class.java
        }else if(frag_check==2){
            fragmentClass = GalleryFragment::class.java
        }else if(frag_check==3){
            fragmentClass = LoyaltyFragment::class.java
        }else if(frag_check==4){
            fragmentClass = Addprofragment::class.java
        }else if(frag_check==5){
            fragmentClass = MarketPlaceList::class.java
        }else if(frag_check==6){
            fragmentClass = ManageProducts::class.java
        }else if(frag_check==7){
            //fragmentClass = ManageProducts::class.java

            val intent = Intent(this@Home, ProfileActivity::class.java)
            startActivity(intent)

        }else if(frag_check==8){
            fragmentClass = Catalogue::class.java
        }else if(frag_check==9){
            fragmentClass = OrderManageEcom::class.java
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }

        try {
            fragment = fragmentClass.newInstance() as Fragment
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // Insert the fragment by replacing any existing fragment
        val fragmentManager: FragmentManager = supportFragmentManager
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment!!).commit()
        }


    }
    fun logout_home() {
        val alertDialog = AlertDialog.Builder(this@Home)
        alertDialog.setTitle("Logout !")
        alertDialog.setMessage("Are sure you want to logout your account.")
        alertDialog.setPositiveButton("Yes") { dialog, id ->

            val db = DatabaseHandler(this@Home)
            db.resetTables()

            dialog.dismiss()
            val intent = Intent(this@Home, LoginAuth::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()

        }
        alertDialog.setNegativeButton("Not Now") { dialog, id -> dialog.dismiss() }
        alertDialog.show()
    }
    fun onGet_cart_count() {
        Log.i("cart_count", "" + cart_count)
        invalidateOptionsMenu()
    }
    fun setBadgeCount(context: Context?, icon: LayerDrawable, count: String?) {
        val badge: BadgeDrawable
        // Reuse drawable if possible
        val reuse = icon.findDrawableByLayerId(R.id.ic_badge)
        badge = if (reuse != null && reuse is BadgeDrawable) {
            reuse
        } else {
            BadgeDrawable(context)
        }
        badge.setCount(count)
        icon.mutate()
        icon.setDrawableByLayerId(R.id.ic_badge, badge)
    }

    fun alert_loyalty() {
        val alertDialog = AlertDialog.Builder(this@Home)
        alertDialog.setTitle("Ecommerce")
        alertDialog.setMessage("Sorry, you are not associated with Ecommerce.")
        alertDialog.setPositiveButton("CLOSE") { dialog, id ->

           /* val db = DatabaseHandler(this@Home)
            db.resetTables()*/

            dialog.dismiss()

            /*val intent = Intent(this@Home, LoginAuth::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()*/

        }
       // alertDialog.setNegativeButton("Not Now") { dialog, id -> dialog.dismiss() }
        alertDialog.show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)

        val itemCart = menu.findItem(R.id.action_cart)
        //BitmapDrawable icon = (BitmapDrawable) itemCart.getIcon();
        val icon = itemCart.icon as LayerDrawable
        //LayerDrawable iconLayer = new LayerDrawable(new Drawable [] { icon });
        setBadgeCount(this@Home, icon, cart_count.toString())
        itemCart.setOnMenuItemClickListener {

            //navView.setCheckedItem(2)
            navView.menu.getItem(1).isChecked = true
            //navView.isSelected.=true
            navView.getMenu().performIdentifierAction(R.id.nav_slideshow, 0);


            /* val intent = Intent(this@Home, ViewCart::class.java)
             startActivity(intent)*/
            /*   //kupe/////////////////////////////////////////////////////////////////////////////
                Intent intent = new Intent(Home.this, ViewCart.class);
                intent.putExtra("welcome","ViewCart");
                startActivity(intent);
                //kupe//////// */
            false
        } // ================================================= onclick view cart
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            //imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
            imm.toggleSoftInput(
                InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY
            )
        }
    }

    fun count_loaded(mobile_no: String){
        // ======================================================== MEMBERACTION ==================================================================

            val progressDialog = ProgressDialog(this)
            progressDialog.setMessage("Loading...")
            progressDialog.setCanceledOnTouchOutside(false)
            progressDialog.show()

            val retrofit = Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            val service = retrofit.create(APIServices::class.java)
            val call = service.getCount(mobile_no)


            call.enqueue(object : Callback<CountResponse> {
                @SuppressLint("ResourceType")
                override fun onResponse(
                    call: Call<CountResponse>,
                    response: Response<CountResponse>
                ) {
                    val statusCode = response.code()
                    val user = response.body()

                    Log.i("ordercount_response", "response------------------" + response)
                    Log.i("statusCode", " 123_456_789 " + statusCode)
                    Log.i("user", " 123_456_789se " + user)

                    //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                    val str_success = response.body()!!.success
                    val str_error = response.body()!!.error

                    if (str_success == 1) {

                        val str_count = response.body()!!.count

                        cart_count = str_count.toString()

                        // cart_count = "0"

                        Log.i("str_success", "123_456_789se" + str_success)
                        Log.i("cart_count", "123_456_789se" + cart_count)

                        //    Toast.makeText(applicationContext,cart_count,Toast.LENGTH_SHORT).show()

                        onGet_cart_count()

                        progressDialog.dismiss()


                    } else if (str_error.equals("1")) {

                        progressDialog.dismiss()

                    }// =================================================== str_success

                }// ---------------------------------------- onResponse --------------------------------

                override fun onFailure(call: Call<CountResponse>, t: Throwable) {
                    // Log error here since request failed
                    progressDialog.dismiss()

                    Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                    Log.i("zxczxc", "123_456_789se" + "---------------------")

                }
            })

        }// -------------------------------------- otp request ------------------------------------------

    private fun showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        val viewGroup = findViewById<ViewGroup>(R.id.content)

        //then we will inflate the custom alert dialog xml that we created
        val dialogView: View =
            LayoutInflater.from(this).inflate(R.layout.my_dialog, viewGroup, false)

        val txt_camera: TextView = dialogView.findViewById(R.id.txt_camera)
        val txt_gallery: TextView = dialogView.findViewById(R.id.txt_gallery)

        val btn_close: TextView = dialogView.findViewById(R.id.btn_close)

        //Now we need an AlertDialog.Builder object
        val builder = AlertDialog.Builder(this)

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it
        val alertDialog = builder.create()
        alertDialog.show()

        txt_camera.setOnClickListener { v->
            PERMISSION_CODE = 1000;
            camera_fn()
            alertDialog.dismiss()
        }
        txt_gallery.setOnClickListener { v->
            PERMISSION_CODE = 1001;
            gallery_fn()
            alertDialog.dismiss()
        }


        btn_close.setOnClickListener { v->
            alertDialog.dismiss()
        }
    }
    private fun camera_fn() {
        //if system os is Marshmallow or Above, we need to request runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (this.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED){
                //permission was not enabled
                val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                //show popup to request permission
                requestPermissions(permission, PERMISSION_CODE)
            }
            else{
                //permission already granted
                openCamera()
            }
        }
        else{
            //system os is < marshmallow
            openCamera()
        }
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }
    private fun gallery_fn() {
        //if system os is Marshmallow or Above, we need to request runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (this.checkSelfPermission(Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED ||
                this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED){
                //permission was not enabled
                val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                //show popup to request permission
                requestPermissions(permission, PERMISSION_CODE)
            }
            else{
                //permission already granted
                pickImageFromGallery();
            }
        }
        else{
            //system os is < marshmallow
            pickImageFromGallery();
        }
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //called when user presses ALLOW or DENY from Permission Request Popup
        when(requestCode){


            PERMISSION_CODE -> {

                Log.i("requestCodeSSSSSS","" + requestCode)

                if (grantResults.size > 0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup was granted
                    if(PERMISSION_CODE==1000){
                        openCamera()
                    }else{
                        camera_fn()
                    } // if

                }
                else{
                    //permission from popup was denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        //called when image was captured from camera intent
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_CAPTURE_CODE){
            //set image captured to image view



            mediaPath = FilePath.getPath(this, image_uri)
            imageView.setImageURI(image_uri)

            uploadImage(str_mem)

            /*   val file = File(mediaPath)
               val fileInBytes = file.length().toFloat()
               val fileSizeInKB = fileInBytes / 1024

               Log.i("fileSizeInKB","666666666666666666666" + fileSizeInKB)

               if (fileSizeInKB <= 1024 * 2) {
                   //mediaPath = image_uri.toString()
                   img_camera.setImageURI(image_uri)
               }else{
                   Toast.makeText(
                       context,
                       "Select Images less than 2 MB ",
                       Toast.LENGTH_LONG
                   ).show()
               }*/
            // image_v.setImageURI(selectedImage as Uri?)


        }else  if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){

            //   image_v.setImageURI(data?.data)

            // Get the Image from data

            // Get the Image from data
            val selectedImage = data!!.data
            val filePathColumn =
                arrayOf(MediaStore.Images.Media.DATA)

            val cursor: Cursor = this.contentResolver.query(selectedImage!!, filePathColumn, null, null, null)!!
            cursor.moveToFirst()

            val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
            mediaPath = cursor.getString(columnIndex)
            //str1.setText(mediaPath)
            // Set the Image in ImageView for Previewing the Media
            // Set the Image in ImageView for Previewing the Media

            imageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath))

            uploadImage(str_mem)


            /*   val file = File(mediaPath)
               val fileInBytes = file.length().toFloat()
               val fileSizeInKB = fileInBytes / 1024

               if (fileSizeInKB <= 1024 * 2) {
                   img_camera.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
               }else{
                   Toast.makeText(
                       context,
                       "Select Images less than 2 MB ",
                       Toast.LENGTH_LONG
                   ).show()
               }*/

            //image_v.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
            cursor.close()

            /* try {
                 is_image = contentResolver.openInputStream(data!!.data!!)!!
                 imageBytes  = getBytes(is_image!!)
             } catch (e: IOException) {
                 e.printStackTrace()
             }*/

        }
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        val fragment = this.supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        (fragment as? IOnBackPressed)?.onBackPressed()?.not()?.let {
            Log.i("-----------------------","");
            // Log.i("points-----------------------",""+)
            // super.onBackPressed()
        }
        doubleBackToExitPressedOnce = true

        //Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()
       // Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)

    }

    private fun uploadImage(
        str_mem: String
    ) {

        val progressDialog = ProgressDialog(this@Home)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val gson: Gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        val service = retrofit.create(APIServices::class.java)

        Log.i("kontena_id","000000000000000000000000"+str_mem)

        val mem_id: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), str_mem)

        val file = File(mediaPath)

        // Parsing any Media type file
        Log.i("file_file","000000000000000000000000"+file)
        Log.i("file_file","000000000000000000000000"+file.getName())
        // Parsing any Media type file
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val image_upload = MultipartBody.Part.createFormData("profimg", file.getName(), requestBody)

        Log.i("body_body","000000000000000000000000"+requestBody)
        Log.i("image_upload","000000000000000000000000"+image_upload.headers())
        Log.i("image_upload","000000000000000000000000"+image_upload.body())


        val call: Call<UploadImageResponse> = service.getUploadProfileUser(mem_id,image_upload); //body
        // mProgressBar.setVisibility(View.VISIBLE)

        /*  val call: Call<UploadResponse> = service.upload(str_mem,str_offer_title,str_radio,"1",str_Offer_desc,"offer_terms",str_Start_ON,str_Expire_ON,
              str_No_of_coupon,str_Coupon_Code);*/

        call.enqueue(object : Callback<UploadImageResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<UploadImageResponse>, response: Response<UploadImageResponse>) {

                val statusCode = response.code()
                val user = response.body()

                progressDialog.dismiss()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){
                    Toast.makeText(this@Home,"Upload Image Successful.",Toast.LENGTH_SHORT).show()

                }

                Log.i("str_success"," 123_456_789se " + str_success)


            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<UploadImageResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }


    fun member_details(){
        // ======================================================== MEMBERACTION ==================================================================

        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getUserDetails(str_mem)


        call.enqueue(object : Callback<UserDetailsResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<UserDetailsResponse>,
                response: Response<UserDetailsResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    val result = response.body()!!.data
                    val profile_img = result.profile_img
                    val avatar = response.body()!!.avatar

                    if(profile_img!=null){
                        if(!profile_img.isEmpty()){
                            Glide.with(this@Home)
                                .load("http://www.beepxs.solutions/uploads/"+profile_img)
                                .into(imageView)
                        }else{
                            Glide.with(this@Home)
                                .load(avatar)
                                .into(imageView)
                        }
                    }else{
                        Glide.with(this@Home)
                            .load(avatar)
                            .into(imageView)
                    }

                    Log.i("str_success", "123_456_789se" + str_success)
                    progressDialog.dismiss()


                } else if (str_error.equals("1")) {

                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<UserDetailsResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------


    /* override fun onBackPressed() {
         val fragment = this.supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
         (fragment as? IOnBackPressed)?.onBackPressed()?.not()?.let {
             Log.i("-----------------------","");
            // Log.i("points-----------------------",""+)
                // super.onBackPressed()
         }*/
    //}




}