package beepplus.bn.app.home.pick_merchant

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import beepplus.bn.app.R
import beepplus.bn.app.adapter.HistoryTrans
import beepplus.bn.app.adapter.pick_merchant.PickMerchantAdapter
import beepplus.bn.app.home.Home
import beepplus.bn.app.sql.DatabaseHandler
import com.naruvis.ecom.pojo.HistoryResponse
import com.naruvis.ecom.pojo.MerchantDetailsResponse
import com.naruvis.ecom.pojo.MerchantListResponse
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.activity_pick_merchant.*
import kotlinx.android.synthetic.main.activity_pick_merchant.toolbar
import kotlinx.android.synthetic.main.content_history.*
import kotlinx.android.synthetic.main.content_pick_merchant.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl

class PickMerchant : AppCompatActivity() {

    private var mLayoutManager: LinearLayoutManager? = null
    var str_mem = "";
    var str_email = ""
    var str_name = ""
    lateinit var pickMerchantAdapter: PickMerchantAdapter
    var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pick_merchant)

        toolbar.setTitle("Switch Merchant")

        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_small_white_24)
        toolbar.setNavigationOnClickListener { v->
            onBackPressed()
        }

        val isMem_His = DatabaseHandler(applicationContext)
        val mem = isMem_His.isMem_His
        val sb_mem = StringBuilder()
        sb_mem.append("")
        sb_mem.append(mem)
        str_mem = sb_mem.toString()

        val isMem_Email = DatabaseHandler(applicationContext)
        val mem_email = isMem_Email.isMem_email
        val sb_mem_no = StringBuilder()
        sb_mem_no.append("")
        sb_mem_no.append(mem_email)
        str_email = sb_mem_no.toString()

        val isMem_name = DatabaseHandler(applicationContext)
        val mem_name = isMem_name.isMem_name
        val sb_mem_n = StringBuilder()
        sb_mem_n.append("")
        sb_mem_n.append(mem_name)
        str_name = sb_mem_n.toString()

        Log.i("str_mem_str_mem",str_mem)

        mLayoutManager = LinearLayoutManager(this)
        recycler_pick_merchant!!.layoutManager = LinearLayoutManager(this@PickMerchant, LinearLayoutManager.VERTICAL, false)
        recycler_pick_merchant!!.addItemDecoration(DividerItemDecoration(this@PickMerchant, LinearLayoutManager.VERTICAL))
        recycler_pick_merchant!!.layoutManager = mLayoutManager

        merchant_loaded(str_mem)

    }

    fun merchant_loaded(mem_id:String){
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("merchant_loaded"," 123_456_789se " + mem_id)


        //http://www.beepxs.solutions/api/merchant/lists?user_id=2

        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getMerchantList(mem_id)


        call.enqueue(object : Callback<MerchantListResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<MerchantListResponse>, response: Response<MerchantListResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    //val data = response.body()!!.data
                    val results = response.body()!!.data

                    Log.i("coupon_jsonObject"," 123_456_789se " + results)
                    for (i in 0 until results.size){
                        val jsonObject = results.get(i)
                        val created_at = jsonObject.created_at;
                        val name = jsonObject.name;
                        val email_id = jsonObject.email_id;
                        val mobile_no = jsonObject.mobile_no;
                        val id = jsonObject.id;

                        val map = java.util.HashMap<String, String>()
                        map["created_at"] = created_at
                        map["name"] = name
                        map["email_id"] = email_id
                        map["mobile_no"] = mobile_no
                        map["id"] = id

                        arrayListTrans.add(map)

                        //arrayListTrans.add()

                        Log.i("coupon_jsonObject"," 123_456_789se " + id)

                    }


                    Log.i("data_data"," 123_456_789se " + results)

                    progressDialog.dismiss()

                    pickMerchantAdapter = PickMerchantAdapter(this@PickMerchant ,arrayListTrans)
                    recycler_pick_merchant.adapter = pickMerchantAdapter


                }else if(str_error==1){

                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<MerchantListResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------

    fun update_merchant(merchant_id:String){

        merchant_details(merchant_id)
    } // merchant_id


    fun merchant_details(merchant_id:String){
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("merchant_details"," 123_456_789se " + merchant_id)


        //http://www.beepxs.solutions/api/merchant/lists?user_id=2

        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getMerchantDetails(merchant_id)


        call.enqueue(object : Callback<MerchantDetailsResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<MerchantDetailsResponse>, response: Response<MerchantDetailsResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("pick_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    //val data = response.body()!!.data
                    val results = response.body()!!.data



                    var onebrunei_id = response.body()!!.onebrunei_id
                    var kontena_id = response.body()!!.kontena_id
                    var brpatrick_id = response.body()!!.brpatrick_id

                    if(onebrunei_id.isEmpty()){
                        onebrunei_id = "0";
                    }
                    if(kontena_id.isEmpty()){
                        kontena_id = "0";
                    }
                    if(brpatrick_id.isEmpty()){
                        brpatrick_id = "0";
                    }

                    val merchant_name = results!!.name
                    val merchant_id = results.id


                    val db = DatabaseHandler(this@PickMerchant)
                    db.resetTables()

                   // brpatrick_id = "20"

                    db.Member_history(str_mem,str_email,str_name,onebrunei_id,kontena_id,brpatrick_id,merchant_id,merchant_name)
                    //db.Member_history("1",email,"1111",onebrunei_id,kontena_id,brpatrick_id)


                    val intent = Intent(this@PickMerchant, Home::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent)
                    finish()

                    Log.i("data_data"," 123_456_789se " + results)

                    progressDialog.dismiss()




                }else if(str_error==1){

                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<MerchantDetailsResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------


}