package beepplus.bn.app.home.ui.ecomcatalogue

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import beepplus.bn.app.R
import beepplus.bn.app.auth_screen.LoginAuth
import beepplus.bn.app.home.Home.Companion.manage_fragment_orders
import beepplus.bn.app.home.ui.addproduct.Addprofragment
import beepplus.bn.app.home.ui.gallery.GalleryFragment
import beepplus.bn.app.home.ui.loyalty.LoyaltyFragment
import beepplus.bn.app.home.ui.loyalty.ManageshowViewModel
import beepplus.bn.app.home.ui.marketplace.OMSListProduct
import beepplus.bn.app.sql.DatabaseHandler
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.naruvis.ecom.pojo.OrdersCountResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl

class OrderManageEcom : Fragment() {

    var fragment: Fragment? = null
    lateinit var fragmentClass: Class<*>

    lateinit var chip_cloud: com.adroitandroid.chipcloud.ChipCloud
    lateinit var progress: ProgressBar
    private lateinit var manageshowViewModel: ManageshowViewModel
    lateinit var webview_load1: WebView
    lateinit var txt_user_name: TextView
    lateinit var kontena_btn: Button;
    lateinit var one_brunei_btn: Button;
    lateinit var btn3: Button;
    lateinit var btn4: Button;
    var str_mobile_no = LoginAuth.mobile_no
    var str_name = ""
    var seller_id = "43"
    var seller_obid = "43"
    lateinit var txt_new_orders_n: TextView
    lateinit var txt_pending_orders_n: TextView

    lateinit var txt_new_orders1_n: TextView
    lateinit var txt_pending_orders1_n: TextView

    private val url = "https://brpatrick.beepxs.solutions/merchant/offer?merchant_id=2"
    //private val url = "https://www.graceonline.in/"
    lateinit var tabLayout: TabLayout
    lateinit var linear_add_offer: LinearLayout
    lateinit var viewPager: ViewPager
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        manageshowViewModel =
            ViewModelProviders.of(this).get(ManageshowViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_oms, container, false)
        //val textView: TextView = root.findViewById(R.id.text_slideshow)
        manageshowViewModel.text.observe(viewLifecycleOwner, Observer {
            //textView.text = it

            //  webview_load1 = root.findViewById(R.id.webview_load1)

            Log.i("testing_purpose", "----------------------------------")

            val isMem_Name = context?.let { it1 -> DatabaseHandler(it1) }
            val mem_name = isMem_Name?.isMem_name
            val sb_mem_name = StringBuilder()
            sb_mem_name.append("")
            sb_mem_name.append(mem_name)
            str_name = sb_mem_name.toString()

            val isMem_kid = context?.let { it1 -> DatabaseHandler(it1) }
            val memkid = isMem_kid?.isMem_kid
            val sb_memkid = StringBuilder()
            sb_memkid.append("")
            sb_memkid.append(memkid)
            seller_id = sb_memkid.toString()

            val isMem_obid = context?.let { it1 -> DatabaseHandler(it1) }
            val memoid = isMem_obid?.isMem_obid
            val sb_memobid = StringBuilder()
            sb_memobid.append("")
            sb_memobid.append(memoid)
            seller_obid = sb_memobid.toString()

            txt_new_orders_n = root.findViewById(R.id.txt_new_orders_n);
            txt_pending_orders_n = root.findViewById(R.id.txt_pending_orders_n);
            txt_pending_orders1_n = root.findViewById(R.id.txt_pending_orders1_n);
            txt_new_orders1_n = root.findViewById(R.id.txt_new_orders1_n);

            txt_new_orders_n.setText("0")
            txt_pending_orders_n.setText("0")
            txt_pending_orders1_n.setText("0")
            txt_new_orders1_n.setText("0")

            kontena_btn = root.findViewById(R.id.kontena_btn);
            one_brunei_btn = root.findViewById(R.id.one_brunei_btn);

            kontena_btn.setOnClickListener { v ->
                manage_fragment_orders = 1;
                fragmentClass_Menu(1)
            }
            one_brunei_btn.setOnClickListener { v ->
                manage_fragment_orders = 2;
                fragmentClass_Menu(1)
            }

            OrderCountOneBrunei(seller_id)
            OrderCount(seller_id)

        })
        return root
    }

    fun fragmentClass_Menu(frag_check: Int){

        if(frag_check==1){
            fragmentClass = OMSListProduct::class.java
        }else if(frag_check==2){
            fragmentClass = GalleryFragment::class.java
        }else if(frag_check==3){
            fragmentClass = LoyaltyFragment::class.java
        }else if(frag_check==4){
            fragmentClass = Addprofragment::class.java
        }


        try {
            fragment = fragmentClass.newInstance() as Fragment
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // Insert the fragment by replacing any existing fragment
        val fragmentManager: FragmentManager = requireActivity().supportFragmentManager
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment!!).commit()
        }
    }


    fun Context.toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun OrderCount(s_id: String) {
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("manage_loaded", " 123_456_789se " + s_id)
        Log.i(
            "manage_loaded1",
            " 123_456_789se " + "https://onebrunei.ideablitztech.com/api/orderview?seller_id=" + s_id + "&status=7"
        )

        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_KONTENA_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getSellerOrdersCount(seller_id)

        call.enqueue(object : Callback<OrdersCountResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<OrdersCountResponse>,
                response: Response<OrdersCountResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()
                //val jsonElement = response.body()
                //Log.i("jsonElement", " 123_456_789se " + jsonElement)
                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                /*val s: String = str_success.replace("\"", "")
                val e: String = str_error.replace("\"", "")*/

                if (str_success == 1) {

                    //val str_all = jsonElement!!.asJsonObject.get("all")
                    val pending = response.body()!!.pending.toString()
                    val in_progress = response.body()!!.active.toString()

                  /*  val x: String = pending.replace("\"", "")
                    val y: String = in_progress.replace("\"", "")
*/
                    Log.i("str_success", " 123_456_789se " + pending)
                    Log.i("str_error", " 123_456_789se " + in_progress)

                    /*Log.i("str_all", " 123_456_789se " + str_all)
                    Log.i("pending", " 123_456_789se " + pending)
*/
                    txt_new_orders_n.text = pending.toString()
                    txt_pending_orders_n.text = in_progress.toString()



                } else {
                    txt_new_orders_n.text = "0"
                    txt_pending_orders_n.text = "0"
                }


                progressDialog.dismiss()

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<OrdersCountResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------
    fun OrderCountOneBrunei(s_id: String) {
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("manage_loaded", " 123_456_789se " + s_id)
        Log.i(
            "manage_loaded1",
            " 123_456_789se " + "https://onebrunei.ideablitztech.com/api/orderview?seller_id=" + s_id + "&status=7"
        )

        /*val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()*/

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_ONE_BRUNEI_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getSellerOrdersCount(seller_obid)

        call.enqueue(object : Callback<OrdersCountResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<OrdersCountResponse>,
                response: Response<OrdersCountResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()
                val jsonElement = response.body()

                Log.i("jsonElement", " 123_456_789se " + jsonElement)
                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    //val str_all = jsonElement!!.asJsonObject.get("all")
                    val pending = response.body()!!.pending.toString()
                    val in_progress = response.body()!!.active.toString()


                   /* val x: String = pending.replace("\"", "")
                    val y: String = in_progress.replace("\"", "")*/

                    Log.i("str_success", " 123_456_789se " + str_success)
                    Log.i("str_error", " 123_456_789se " + str_error)

                    Log.i("str_all", " 123_456_789se " + pending)
                    Log.i("pending", " 123_456_789se " + in_progress)

                    txt_new_orders1_n.text = pending
                    txt_pending_orders1_n.text = in_progress


                } else {
                    txt_new_orders1_n.text = "0"
                    txt_pending_orders1_n.text = "0"
                }


             //   progressDialog.dismiss()

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<OrdersCountResponse>, t: Throwable) {
                // Log error here since request failed
             //   progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------


}