package beepplus.bn.app.home.ui.ecomcatalogue

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import beepplus.bn.app.R
import beepplus.bn.app.adapter.ProductDetAdapter
import beepplus.bn.app.home.Home
import beepplus.bn.app.home.Home.Companion.manage_fragment_restart1
import beepplus.bn.app.home.Home.Companion.manage_fragment_restart2
import beepplus.bn.app.home.Home.Companion.manage_fragment_restart3
import beepplus.bn.app.sql.DatabaseHandler
import com.naruvis.ecom.pojo.OrderResponse
import com.naruvis.ecom.pojo.UpdateorderResponse
import kotlinx.android.synthetic.main.activity_orders_details_ecom.*
import kotlinx.android.synthetic.main.fragment_order_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl
import java.text.SimpleDateFormat
import java.util.*

class OrdersDetailsEcom : AppCompatActivity() {

    var str_name = ""
    var str_order_id = ""
    var seller_id = "43"
    var seller_obid = "44"
    lateinit var productDetAdapter: ProductDetAdapter
    var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()
    var bundle: Bundle? = null
    var str_dollar = "$"
    var order_status = ""
    var select_status = ""
    var double1: Double = 0.00

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_orders_details_ecom)

        val isMem_name = DatabaseHandler(applicationContext)
        val mem_name = isMem_name.isMem_name
        val sb_mem_n = StringBuilder()
        sb_mem_n.append("")
        sb_mem_n.append(mem_name)
        str_name = sb_mem_n.toString()

        Log.i("str_name----------", str_name)

        val isMem_kid = DatabaseHandler(applicationContext)
        val memkid = isMem_kid?.isMem_kid
        val sb_memkid = StringBuilder()
        sb_memkid.append("")
        sb_memkid.append(memkid)
        seller_id = sb_memkid.toString()

        val isMem_obid = DatabaseHandler(applicationContext)
        val memoid = isMem_obid?.isMem_obid
        val sb_memobid = StringBuilder()
        sb_memobid.append("")
        sb_memobid.append(memoid)
        seller_obid = sb_memobid.toString()


        var mLayoutManager: LinearLayoutManager? = null

        /*val toolbar: Toolbar = findViewById(R.id.toolbar_ord)
        setSupportActionBar(toolbar)*/

        toolbar_ord.title = str_name

        toolbar_ord.setNavigationIcon(R.drawable.ic_baseline_arrow_small_white_24)
        toolbar_ord.setNavigationOnClickListener { v->
            Log.i("toolbar_ord", "toolbar_ord-------------------------" + "toolbar_ord")
            onBackPressed()
        }
        //recycler_list

        bundle = getIntent().getExtras();

        if (bundle != null) {
            str_order_id = bundle!!.getString("order_id").toString();
            Log.i("Testing_str_from1", "str_from-------------------------" + str_order_id)
        }else {
            Log.i("Testing_str_from2", "str_from-------------------------" + str_order_id)

        }


        mLayoutManager = LinearLayoutManager(this@OrdersDetailsEcom)
        recycler_list.layoutManager =
            LinearLayoutManager(this@OrdersDetailsEcom, LinearLayoutManager.VERTICAL, false)
        recycler_list.addItemDecoration(
            DividerItemDecoration(
                this@OrdersDetailsEcom,
                LinearLayoutManager.VERTICAL
            )
        )
        recycler_list.layoutManager = mLayoutManager

        change_status.setOnClickListener { v->
            showCustomDialog();
            //showDialog();
            Log.i("TestPurpose", "00000000000000000000000000000000000");
            //showCustomDialog();
        }

        details_loaded(seller_id, str_order_id)
    }


    private fun showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        val viewGroup = findViewById<ViewGroup>(R.id.content)

        //then we will inflate the custom alert dialog xml that we created
        val dialogView: View =
            LayoutInflater.from(this).inflate(R.layout.custom_layout, viewGroup, false)

        val yes:TextView = dialogView.findViewById(R.id.yes)
        val no:TextView = dialogView.findViewById(R.id.no)

        val radioGroup:RadioGroup = dialogView.findViewById(R.id.radioGroup)

        val radioButton1:RadioButton = dialogView.findViewById(R.id.radioButton1)
        val radioButton2:RadioButton = dialogView.findViewById(R.id.radioButton2)
        val radioButton3:RadioButton = dialogView.findViewById(R.id.radioButton3)


        if(order_status=="0"){
            radioButton2.visibility =View.GONE
            radioButton3.visibility =View.GONE
        }else if(order_status=="1"){
            radioButton1.visibility =View.GONE
        }else if(order_status=="7"){

        }

        //Now we need an AlertDialog.Builder object
        val builder = AlertDialog.Builder(this)

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it
        val alertDialog = builder.create()
        alertDialog.show()

        // Get radio group selected item using on checked change listener

        radioGroup.setOnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
            //val radioButton1:RadioButton = dialogView.findViewById(R.id.radioButton1)
            val rb = dialogView.findViewById<View>(checkedId) as RadioButton

            if(rb.isChecked){

                if (order_status == "0") {
                    select_status = "1"
                } else if (order_status == "1") {
                    select_status = "7"
                } else if (order_status == "7") {
                    //status.setText("Status :" + " " + "In Progress")
                }

            }else{
                //Toast.makeText(applicationContext, "Select the change status", Toast.LENGTH_SHORT).show()
            }

            Log.i("check_it===============",""+rb.text.toString())

            //Toast.makeText(applicationContext, rb.text, Toast.LENGTH_SHORT).show()
        }


        yes.setOnClickListener { v->

            if(!select_status.isEmpty()){

                if (order_status == "0") {
                    select_status = "1"
                    update_orders(select_status)
                    alertDialog.dismiss()
                } else if (order_status == "1") {
                    select_status = "7"
                    update_orders(select_status)
                    alertDialog.dismiss()
                } else if (order_status == "7") {
                    alertDialog.dismiss()
                    //status.setText("Status :" + " " + "In Progress")
                }

            }else{
                Toast.makeText(this@OrdersDetailsEcom, "Select the change status", Toast.LENGTH_SHORT).show()
            }
            //alertDialog.dismiss()
        }
        no.setOnClickListener { v->
            select_status = ""
            alertDialog.dismiss()
        }

    }
    private fun showDialog() {
        val dialog = Dialog(this@OrdersDetailsEcom)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_layout)

        /*val image_v= dialog.findViewById(R.id.image_v) as ImageView
        image_v.visibility = View.GONE
        val txt_no= dialog.findViewById(R.id.txt_no) as TextView
        txt_no.visibility = View.GONE*/

        val yes = dialog.findViewById(R.id.yes) as TextView
        val noBtn = dialog.findViewById(R.id.no) as TextView
        yes.setOnClickListener { dialog.dismiss() }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

    fun details_loaded(s_id: String, o_id: String) {
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("manage_loaded", " 123_456_789se " + s_id)
        Log.i(
            "manage_loaded1",
            " 123_456_789se " + "https://onebrunei.ideablitztech.com/api/orderview?seller_id=" + s_id + "&status=0"
        )

        val progressDialog = ProgressDialog(this@OrdersDetailsEcom)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        var URL = ""
        if(Home.manage_fragment_orders==1){
            URL = APIUrl.BASE_KONTENA_URL
            seller_id = seller_id;
        }else{
            URL = APIUrl.BASE_ONE_BRUNEI_URL
            seller_id = seller_obid;
        }

        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getOrderDetails(seller_id, o_id)


        call.enqueue(object : Callback<OrderResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<OrderResponse>,
                response: Response<OrderResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    //val data = response.body()!!.data
                    val results = response.body()!!.data

                    val s_order_id = results.orderdetails.order_id;
                    val created_at = results.orderdetails.created_at;
                    val total_qty = results.orderdetails.total_qty;
                    val total_price = results.orderdetails.total_price;
                    val total_product = results.orderdetails.total_product;
                    val cus_name = results.orderdetails.cus_name;
                    val cus_mob = results.orderdetails.cus_mob;
                    val tbl_no = results.orderdetails.tbl_no;
                    val s_status = results.orderdetails.status;

                    val orderitems = results.orderitems;


                    order_id.setText("Order id : " + "#" + s_order_id)
                    order_status = s_status;

                    if (s_status == "0") {
                        status.setText("Status :" + " " + "Pending")
                    } else if (s_status == "7") {
                        change_status.visibility = View.GONE
                        status.setText("Status :" + " " + "Completed")
                    } else {
                        status.setText("Status :" + " " + "In Progress")
                    }


                    var format = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                    val newDate: Date = format.parse(created_at)

                    format = SimpleDateFormat("MMM dd,yyyy hh:mm a")
                    val date = format.format(newDate)

                    txt_date.setText(date)

                    name.setText("Name : " + " " + cus_name)
                    mobile_no.setText("Mobile Number : " + " " + "+673" + " " + cus_mob)
                    table_no.setText("Table Number :" + " " + tbl_no)

                    Log.i("coupon_jsonObject", " 123_456_789se " + results)
                    for (i in 0 until orderitems.size) {
                        val jsonObject = orderitems.get(i)

                        val product_name = jsonObject.product_name;
                        val unit_name = jsonObject.unit_name;
                        val qty = jsonObject.qty;
                        val total_price = jsonObject.total_price;
                        val image = jsonObject.image;
                        val category_id = jsonObject.category_id;

                        val map = java.util.HashMap<String, String>()
                        map["product_name"] = product_name
                        map["unit_name"] = unit_name
                        map["qty"] = qty
                        map["total_price"] = total_price
                        map["image"] = image
                        map["category_id"] = category_id

                         double1 = double1 + total_price.toString().toDouble()
                        Log.i("pppppppppppppppppppppp",""+double1)

                        arrayListTrans.add(map)

                    }



                    Log.i("data_data", " 123_456_789se " + results)

                    progressDialog.dismiss()

                    productDetAdapter = ProductDetAdapter(this@OrdersDetailsEcom, arrayListTrans)
                    recycler_list.adapter = productDetAdapter

                    var str_amount = String.format("%.2f", double1)
                    total_amount.setText("Total Amount : " + str_dollar + "" + str_amount)

                    //   txt_no_records.visibility = View.GONE
                } else if (str_error == 1) {
                    //  txt_no_records.visibility = View.VISIBLE
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------

    fun update_orders(s_status: String) {
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("manage_loaded", " 123_456_789se " + s_status)
        Log.i(
            "manage_loaded1",
            " 123_456_789se " + "https://onebrunei.ideablitztech.com/api/orderview?seller_id=" + s_status + "&status=0"
        )

        val progressDialog = ProgressDialog(this@OrdersDetailsEcom)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        var URL = ""
        if(Home.manage_fragment_orders==1){
            URL = APIUrl.BASE_KONTENA_URL
            seller_id = seller_id;
        }else{
            URL = APIUrl.BASE_ONE_BRUNEI_URL
            seller_id = seller_obid;
        }

        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getupdateorder(seller_id,str_order_id,s_status)


        call.enqueue(object : Callback<UpdateorderResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<UpdateorderResponse>,
                response: Response<UpdateorderResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    //val data = response.body()!!.data
                    val results = response.body()!!.succ_msg.toString()

                    Log.i("data_data", " 123_456_789se " + results)

                    Toast.makeText(this@OrdersDetailsEcom, results, Toast.LENGTH_SHORT).show()


                    manage_fragment_restart1 = 1
                    manage_fragment_restart2 = 2
                    manage_fragment_restart3 = 3
                    if (order_status == "0") {
                        onBackPressed()
                    } else if (order_status == "1") {
                        onBackPressed()
                    } else if (order_status == "7") {
                        onBackPressed()
                        //status.setText("Status :" + " " + "In Progress")
                    }


                    progressDialog.dismiss()

                    //   txt_no_records.visibility = View.GONE
                } else if (str_error == 1) {
                    //  txt_no_records.visibility = View.VISIBLE
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<UpdateorderResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// ----

}