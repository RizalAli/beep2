package beepplus.bn.app.home.ui.gallery

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.adapter.TransAdapter
import com.google.android.material.textfield.TextInputEditText

class GalleryFragment : Fragment() {

    private lateinit var galleryViewModel: GalleryViewModel
    var str_symbol = "$"
    lateinit var recycler_trans: RecyclerView
    lateinit var mLayoutManager: LinearLayoutManager
    lateinit var transAdapter: TransAdapter
    var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        galleryViewModel =
            ViewModelProviders.of(this).get(GalleryViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)
        //val textView: TextView = root.findViewById(R.id.text_gallery)
        galleryViewModel.text.observe(viewLifecycleOwner, Observer {
            //textView.text = it

            recycler_trans = root.findViewById(R.id.recycler_trans)
            mLayoutManager = LinearLayoutManager(context)
            recycler_trans.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            recycler_trans.layoutManager = mLayoutManager

            for (i in 0 until 5){

                if(i==0){
                    val map1 = java.util.HashMap<String, String>()

                    map1["payment_status"] = "1"
                    map1["trans_amount"] = "1500.00"
                    map1["trans_title"] = "QR CODE GENERATED"
                    map1["trans_note"] = "GW - AliPay"
                    map1["date"] = "Today"

                    arrayListTrans.add(map1)

                } //  if

                if(i==1){
                    val map1 = java.util.HashMap<String, String>()
                    map1["payment_status"] = "2"
                    map1["trans_amount"] = "2200.00"
                    map1["trans_title"] = "CANCELLED"
                    map1["trans_note"] = "GW - BruPay"
                    map1["date"] = "Friday, 24 July 2020"

                    arrayListTrans.add(map1)
                }

                if(i==2){

                    val map1 = java.util.HashMap<String, String>()
                    map1["payment_status"] = "2"
                    map1["trans_amount"] = "6500.00"
                    map1["trans_title"] = "CANCELLED"
                    map1["trans_note"] = "GW - AliPay"
                    map1["date"] = "Friday, 24 July 2020"

                    arrayListTrans.add(map1)
                }

                if(i==3){

                    val map1 = java.util.HashMap<String, String>()
                    map1["payment_status"] = "1"
                    map1["trans_amount"] = "6500.00"
                    map1["trans_title"] = "QR CODE GENERATED"
                    map1["trans_note"] = "GW - BruPay"
                    map1["date"] = "Wednesday, 22 July 2020"
                    arrayListTrans.add(map1)
                }

                if(i==4){

                    val map1 = java.util.HashMap<String, String>()
                    map1["payment_status"] = "2"
                    map1["trans_amount"] = "10.00"
                    map1["trans_title"] = "CANCELLED"
                    map1["trans_note"] = "GW - BruPay"
                    map1["date"] = "Tuesday, 21 July 2020"
                    arrayListTrans.add(map1)

                }

            } // loop

            Log.i("ArrayListShowing","==========" + arrayListTrans)
            transAdapter = TransAdapter(context ,arrayListTrans)
            recycler_trans.adapter = transAdapter


        })
        return root
    }
}