package beepplus.bn.app.home.ui.loyalty

import android.R.attr.defaultValue
import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import beepplus.bn.app.R
import beepplus.bn.app.home.ui.gallery.GalleryFragment
import beepplus.bn.app.home.ui.home.HomeFragment
import kotlinx.android.synthetic.main.activity_manage_details.*


class ManageOfferDetails : Fragment() {

    lateinit var progress: ProgressBar
    private lateinit var loyaltyViewModel: LoyaltyViewModel
    var fragment: Fragment? = null
    lateinit var fragmentClass: Class<*>

    var id:String = ""
    var title:String = ""
    var merchant_name:String = ""
    var start_on:String = ""
    var end_on:String = ""
    var offer_branch:String = ""
    var branch_names:String = ""
    var coupon_count:String = ""
    var code:String = ""
    var coupon_grabbed :String = ""
    var used_coupon :String = ""

    var str_status :String = ""
    var img :String = ""
    var bundle = this.arguments
    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        loyaltyViewModel =
            ViewModelProviders.of(this).get(LoyaltyViewModel::class.java)
        val root = inflater.inflate(R.layout.activity_manage_details, container, false)
        //val textView: TextView = root.findViewById(R.id.text_slideshow)
        loyaltyViewModel.text.observe(viewLifecycleOwner, Observer {


            Log.i("bundle_bundle","-------------" + bundle)
            if (bundle != null) {

                id = bundle!!.getString("id").toString();
                title = bundle!!.getString("title").toString();
                merchant_name = bundle!!.getString("merchant_name").toString();
                start_on = bundle!!.getString("start_on").toString();
                end_on = bundle!!.getString("end_on").toString();

                offer_branch = bundle!!.getString("offer_branch").toString();
                branch_names = bundle!!.getString("branch_names").toString();
                coupon_count = bundle!!.getString("coupon_count").toString();
                code = bundle!!.getString("code").toString();
                coupon_grabbed = bundle!!.getString("coupon_grabbed").toString();
                used_coupon = bundle!!.getString("used_coupon").toString();

                str_status = bundle!!.getString("status").toString();
                img = bundle!!.getString("img").toString();

            }

            if(str_status=="0"){
                status.setText("Pending")
                status.setTextColor(Color.parseColor("#FF9800"));
            }else  if(str_status=="8"){
                status.setText("Expired")
                status.setTextColor(Color.parseColor("#E91E63"));
            }else if(str_status=="9"){
                status.setText("Rejected")
                status.setTextColor(Color.parseColor("#ffcc0000"));
            }else{
                status.setText("Ongoing")
                status.setTextColor(Color.parseColor("#ff669900"));
            }
            //imge_back
            //status.setText(title)
            txt_offer_details.setText(title)
            txt_merchant.setText(merchant_name)
            txt_duration.setText(start_on+"\n"+end_on)

            if(offer_branch=="1"){
                txt_offervalid.setText("All")
            }else{
                txt_offervalid.setText(branch_names)
            }

            txt_coupondetails.setText(coupon_count + " " + "coupon" + " " + code)
            txt_coupongrap.setText(coupon_grabbed)
            txt_couponused.setText(used_coupon)

            imge_back.setOnClickListener { v->
               // onBackPressed()
            }

        })
        return root
    }


    fun fragmentClass_Menu(frag_check: Int){

        if(frag_check==1){
            fragmentClass = HomeFragment::class.java
        }else if(frag_check==2){
            fragmentClass = GalleryFragment::class.java
        }else if(frag_check==3){
            fragmentClass = ManageOfferFragment::class.java
        }else if(frag_check==4){
            fragmentClass = GalleryFragment::class.java
        }

        try {
            fragment = fragmentClass.newInstance() as Fragment
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // Insert the fragment by replacing any existing fragment
        val fragmentManager: FragmentManager = requireActivity().supportFragmentManager
        if (fragment != null) {
            fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment!!).commit()
        }

    }

}