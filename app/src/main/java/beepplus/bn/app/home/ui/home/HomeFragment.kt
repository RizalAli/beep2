package beepplus.bn.app.home.ui.home

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import beepplus.bn.app.R
import beepplus.bn.app.auth_screen.OTPAuth
import beepplus.bn.app.qrcode.QRcode
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    lateinit var note_edit_text: TextInputEditText
    lateinit var edt_amount: EditText
    lateinit var btn_gen_qr:Button
    var str_amt = "0.00"
    var str_notes = "0.00"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
//        val textView: TextView = root.findViewById(R.id.text_amount)
//        homeViewModel.text.observe(viewLifecycleOwner, Observer {
//            textView.text = it
//        })

        edt_amount = root.findViewById(R.id.edt_amount)
        note_edit_text = root.findViewById(R.id.note_edit_text)
        btn_gen_qr = root.findViewById(R.id.btn_gen_qr)
    //    showSoftKeyboard(edt_amount)


        btn_gen_qr.setOnClickListener { v->

            str_amt = edt_amount.text.toString()
            str_notes = note_edit_text.text.toString()

            if(str_notes.isEmpty()){
                str_notes = ""
            }

            if(!str_amt.isEmpty()){

                    val intent = Intent(context, QRcode::class.java)
                    intent.putExtra("amount",str_amt)
                    intent.putExtra("notes",str_notes)
                    startActivity(intent)

            }else{
                Snackbar.make(v, "Enter the amount", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }



        }

        return root
    }

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            //imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }
}