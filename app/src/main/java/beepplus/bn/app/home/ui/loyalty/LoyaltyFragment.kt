package beepplus.bn.app.home.ui.loyalty

import android.annotation.SuppressLint
import android.app.Activity.RESULT_CANCELED
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.ParseException
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import beepplus.bn.app.R
import beepplus.bn.app.adapter.HistoryTrans
import beepplus.bn.app.adapter.TransAdapter
import beepplus.bn.app.auth_screen.LoginAuth
import beepplus.bn.app.home.ui.gallery.GalleryFragment
import beepplus.bn.app.home.ui.home.HomeFragment
import beepplus.bn.app.home.ui.loyalty.issuespoints.IssuesPointsActivity
import beepplus.bn.app.scanner.*
import beepplus.bn.app.settings.ChangePassword
import beepplus.bn.app.settings.SettingsCommon
import beepplus.bn.app.sql.DatabaseHandler
import com.naruvis.ecom.pojo.HistoryResponse
import kotlinx.android.synthetic.main.content_add_offer.*
import kotlinx.android.synthetic.main.content_history.*
import kotlinx.android.synthetic.main.fragment_loyalty.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl
import java.text.SimpleDateFormat
import java.util.*


class LoyaltyFragment : Fragment() {

  lateinit var chip_cloud: com.adroitandroid.chipcloud.ChipCloud
  lateinit var progress:ProgressBar
  private lateinit var loyaltyViewModel: LoyaltyViewModel
  lateinit var webview_load1:WebView
  lateinit var txt_user_name:TextView
  lateinit var btn1:Button;
  lateinit var btn2:Button;
  lateinit var btn3:Button;
  lateinit var btn4:Button;
  var str_mobile_no = LoginAuth.mobile_no
  var str_mem = ""
  var str_from_date = ""
  var str_to_date = ""
  var str_status = "1"
  private val url = "https://adb.brpaccess.com/merchant/offer?merchant_id=2"
  lateinit var historyTrans: HistoryTrans
  var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()

  var fragment: Fragment? = null
  lateinit var fragmentClass: Class<*>
  var mYear = 0
  var mMonth = 0
  var mDay = 0
  lateinit var recycler_trans: RecyclerView
  lateinit var txt_record_found:TextView
  lateinit var mLayoutManager: LinearLayoutManager
  lateinit var transAdapter: TransAdapter
  lateinit var img1:ImageView;
  lateinit var img2:ImageView;
  lateinit var img3:ImageView;
  lateinit var img_user:ImageView;

  /* var str_symbol = "$"
  lateinit var recycler_trans: RecyclerView
  lateinit var mLayoutManager: LinearLayoutManager
  lateinit var transAdapter: TransAdapter
  var arrayListTrans = java.util.ArrayList<java.util.HashMap<String, String>>()
*/
  //lateinit var webview_load1: WebView


  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {

    loyaltyViewModel =
      ViewModelProviders.of(this).get(LoyaltyViewModel::class.java)
    val root = inflater.inflate(R.layout.fragment_loyalty, container, false)
    //val textView: TextView = root.findViewById(R.id.text_slideshow)
    loyaltyViewModel.text.observe(viewLifecycleOwner, Observer {

     /* val isMem_His = context?.let { it1 -> DatabaseHandler(it1) }
      val mem = isMem_His?.isMem_His
      val sb_mem = StringBuilder()
      sb_mem.append("")
      sb_mem.append(mem)
      str_mem = sb_mem.toString()*/

      val isMem_brid = context?.let { it1 -> DatabaseHandler(it1) }
      val mem_brid = isMem_brid?.isMem_brid
      val sb_mem_brid = StringBuilder()
      sb_mem_brid.append("")
      sb_mem_brid.append(mem_brid)
      str_mem = sb_mem_brid.toString()

      recycler_trans = root.findViewById(R.id.recycler_trans)
      mLayoutManager = LinearLayoutManager(context)
      recycler_trans.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
      recycler_trans.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
      recycler_trans.layoutManager = mLayoutManager
      txt_record_found = root.findViewById(R.id.txt_record_found)

      history_loaded(str_mem)

      img1 = root.findViewById(R.id.img1)
      img2 = root.findViewById(R.id.img2)
      img3 = root.findViewById(R.id.img3)

      img_user = root.findViewById(R.id.img_user)

      img2.setOnClickListener { v->
        val intent = Intent(context, ScanActivity::class.java)
        intent.putExtra("from","3");
        startActivity(intent)
      }
      img3.setOnClickListener { v->

        fragmentClass_Menu(3)
      /*  val intent = Intent(context, ManageOrder::class.java)
        startActivity(intent)*/

      }

      img1.setOnClickListener { v->
        val intent = Intent(context, IssuesPointsActivity::class.java)
        //intent.putExtra("from","3");
        startActivity(intent)
      }

      img_user.setOnClickListener { v->
        val intent = Intent(context, SettingsCommon::class.java)
        //intent.putExtra("from","3");
        startActivity(intent)
      }


      start_date.setOnClickListener { v->

        Log.i("Start_on_text", "=========================")

        val c = Calendar.getInstance()
        mYear = c[Calendar.YEAR]
        mMonth = c[Calendar.MONTH]
        mDay = c[Calendar.DAY_OF_MONTH]
        val datePickerDialog = DatePickerDialog(
          requireContext(),
          DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            start_date.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
            str_from_date = start_date.text.toString().trim()

          }, mYear, mMonth, mDay


        )

        datePickerDialog.show()

      }

      end_date.setOnClickListener { v->
        str_from_date = start_date.text.toString().trim()

        Log.i("str_from_date",""+str_from_date)

        if(!str_from_date.isEmpty() && !str_from_date.equals("Start Date")){

          val c = Calendar.getInstance()
          mYear = c[Calendar.YEAR]
          mMonth = c[Calendar.MONTH]
          mDay = c[Calendar.DAY_OF_MONTH]
          val datePickerDialog = DatePickerDialog(
            requireContext(),
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
              end_date.setText(dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year)
              str_to_date = dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year

              if(!str_to_date.isEmpty() && !str_to_date.equals("End Date")){

                try {
                  val sdf = SimpleDateFormat("dd-MM-yyyy") //edit here
                  val date1 = sdf.parse(str_from_date)
                  val date2 = sdf.parse(str_to_date)
                  Log.i("wwesssdsd", "00000000000000000000000000")
                  println(sdf.format(date1))
                  println(sdf.format(date2))
                  if (date1.compareTo(date2) > 0) {
                    Toast.makeText(context,"Invalid Date",Toast.LENGTH_SHORT).show()
                  } else {
                    history_loaded(str_mem)
                  }
                } catch (ex: ParseException) {
                  Log.i("addd_porduct", "00000000000000000000000000")
                  ex.printStackTrace()
                }

              }else{
                Toast.makeText(context,"Select end date",Toast.LENGTH_SHORT).show()
              }

            }, mYear, mMonth, mDay
          )

          str_to_date = end_date.text.toString().trim()

         /* if(!str_to_date.isEmpty() && !str_to_date.equals("End Date")){

            try {
              val sdf = SimpleDateFormat("dd-MM-yyyy") //edit here
              val date1 = sdf.parse(str_from_date)
              val date2 = sdf.parse(str_to_date)
              Log.i("wwesssdsd", "00000000000000000000000000")
              println(sdf.format(date1))
              println(sdf.format(date2))
              if (date1.compareTo(date2) < 0) {
                Toast.makeText(context,"Invalid Date",Toast.LENGTH_SHORT).show()
              } else {
                history_loaded(str_mem)
              }
            } catch (ex: ParseException) {
              Log.i("addd_porduct", "00000000000000000000000000")
              ex.printStackTrace()
            }

          }else{
            Toast.makeText(context,"Select end date",Toast.LENGTH_SHORT).show()
          }*/

          datePickerDialog.show()

        }else{
          Toast.makeText(context,"Select start date",Toast.LENGTH_SHORT).show()
        }

      }


    })
    return root
  }

  fun history_loaded(mem_id:String){
    // ======================================================== MEMBERACTION ==================================================================

   /* Log.i("manage_loaded"," 123_456_789se " + mem_id)
    Log.i("manage_loaded"," 123_456_789se " + "https://adb.brpaccess.com/api/manageoffers?merchant_id="+mem_id)
*/

    val progressDialog = ProgressDialog(context)
    progressDialog.setMessage("Loading...")
    progressDialog.setCanceledOnTouchOutside(false)
    progressDialog.show()

    Log.i("manage_loaded"," 123_456_789se " + "https://adb.brpaccess.com/api/paymenthistory"+"?merchant_id="+
    str_mem)
    val retrofit = Retrofit.Builder()
      .baseUrl(APIUrl.BASE_BR_ACCESS_URL)
      .addConverterFactory(GsonConverterFactory.create())
      .build()

    val service = retrofit.create(APIServices::class.java)
    val call = service.getHistory(mem_id,str_from_date,str_to_date,str_status)


    call.enqueue(object : Callback<HistoryResponse> {
      @SuppressLint("ResourceType")
      override fun onResponse(call: Call<HistoryResponse>, response: Response<HistoryResponse>) {
        val statusCode = response.code()
        val user = response.body()

        Log.i("qse_ordercount_response","response------------------" + response)
        Log.i("statusCode"," 123_456_789 " + statusCode)
        Log.i("user"," 123_456_789se " + user)

        //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


        val str_success = response.body()!!.success
        val str_error = response.body()!!.error

        if(str_success==1){

          //val data = response.body()!!.data
          val results = response.body()!!.data.results

          Log.i("coupon_jsonObject"," 123_456_789se " + results)
          for (i in 0 until results.size){
            val jsonObject = results.get(i)
            val id = jsonObject.id;
            val created_at = jsonObject.created_at;
            val customer_name = jsonObject.customer_name;
            val customer_mobile = jsonObject.customer_mobile;

            val coupon_code = jsonObject.coupon_code;
            val amount = jsonObject.amount;

            val points = jsonObject.points;


            val map = java.util.HashMap<String, String>()
            map["id"] = id
            map["created_at"] = created_at
            map["customer_name"] = customer_name
            map["customer_mobile"] = customer_mobile

            map["coupon_code"] = coupon_code
            map["amount"] = amount

            map["points"] = points

            arrayListTrans.add(map)

            //arrayListTrans.add()

            Log.i("coupon_jsonObject"," 123_456_789se " + coupon_code)

          }

          Log.i("data_data"," 123_456_789se " + results)

          progressDialog.dismiss()


          if(!results.isEmpty()){
            historyTrans = HistoryTrans(context ,arrayListTrans)
            recycler_trans.adapter = historyTrans
            txt_record_found.visibility = View.GONE
            recycler_trans.visibility = View.VISIBLE
          }else{
            recycler_trans.visibility = View.GONE
            txt_record_found.visibility = View.VISIBLE
          }

        }else if(str_error==1){

          progressDialog.dismiss()
          recycler_trans.visibility = View.GONE
          txt_record_found.visibility = View.VISIBLE

        }// =================================================== str_success

      }// ---------------------------------------- onResponse --------------------------------

      override fun onFailure(call: Call<HistoryResponse>, t: Throwable) {
        // Log error here since request failed
        progressDialog.dismiss()

        Log.i("zxczxc","123_456_789se" + "---------------------" + t)
        Log.i("zxczxc","123_456_789se" + "---------------------")

      }
    })

  }// -------------------------------------- otp request ------------------------------------------

  fun fragmentClass_Menu(frag_check: Int){

    if(frag_check==1){
      fragmentClass = HomeFragment::class.java
    }else if(frag_check==2){
      fragmentClass = GalleryFragment::class.java
    }else if(frag_check==3){
      fragmentClass = ManageOfferFragment::class.java
    }else if(frag_check==4){
      fragmentClass = GalleryFragment::class.java
    }

    try {
      fragment = fragmentClass.newInstance() as Fragment
    } catch (e: Exception) {
      e.printStackTrace()
    }

    // Insert the fragment by replacing any existing fragment
    val fragmentManager: FragmentManager = requireActivity().supportFragmentManager
    if (fragment != null) {
      fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment!!).commit()
    }

  }

}