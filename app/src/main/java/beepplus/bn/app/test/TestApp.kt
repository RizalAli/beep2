package beepplus.bn.app.test

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import beepplus.bn.app.R

class TestApp : AppCompatActivity() { // Test
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_app)
    }
}