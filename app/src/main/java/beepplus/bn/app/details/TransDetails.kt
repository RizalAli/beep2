package beepplus.bn.app.details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import beepplus.bn.app.R
import kotlinx.android.synthetic.main.activity_trans_details.*

class TransDetails : AppCompatActivity() {

    var str_symbol = "$"
    var bundle: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trans_details)

        bundle = getIntent().getExtras();

        if (bundle != null) {
            //str_amt = bundle!!.getString("amount").toString();
            //str_notes = bundle!!.getString("notes").toString();
        }else {

        }

        image_back.setOnClickListener { v->
            onBackPressed()
        }

        txt_trans_amt.setText(str_symbol + " " + "1500.00")
        txt_status.setText("QR CODE GENERATED")
    }
}