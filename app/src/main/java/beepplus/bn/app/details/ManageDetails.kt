package beepplus.bn.app.details

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import beepplus.bn.app.R
import kotlinx.android.synthetic.main.activity_manage_details.*

class ManageDetails : AppCompatActivity() {
    var bundle: Bundle? = null

    var id:String = ""
    var title:String = ""
    var merchant_name:String = ""
    var start_on:String = ""
    var end_on:String = ""
    var offer_branch:String = ""
    var branch_names:String = ""
    var coupon_count:String = ""
    var code:String = ""
    var coupon_grabbed :String = ""
    var used_coupon :String = ""

    var str_status :String = ""
    var img :String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_details)

        bundle = getIntent().getExtras();


        if (bundle != null) {
            id = bundle!!.getString("id").toString();
            title = bundle!!.getString("title").toString();
            merchant_name = bundle!!.getString("merchant_name").toString();
            start_on = bundle!!.getString("start_on").toString();
            end_on = bundle!!.getString("end_on").toString();

            offer_branch = bundle!!.getString("offer_branch").toString();
            branch_names = bundle!!.getString("branch_names").toString();
            coupon_count = bundle!!.getString("coupon_count").toString();
            code = bundle!!.getString("code").toString();
            coupon_grabbed = bundle!!.getString("coupon_grabbed").toString();
            used_coupon = bundle!!.getString("used_coupon").toString();

            str_status = bundle!!.getString("status").toString();
            img = bundle!!.getString("img").toString();

        }else {

        }


        if(str_status=="0"){
            status.setText("Pending")
            status.setTextColor(Color.parseColor("#FF9800"));
        }else  if(str_status=="8"){
            status.setText("Expired")
            status.setTextColor(Color.parseColor("#E91E63"));
        }else if(str_status=="9"){
            status.setText("Rejected")
            status.setTextColor(Color.parseColor("#ffcc0000"));
        }else{
            status.setText("Ongoing")
            status.setTextColor(Color.parseColor("#ff669900"));
        }
        //imge_back
        //status.setText(title)
        txt_offer_details.setText(title)
        txt_merchant.setText(merchant_name)
        txt_duration.setText(start_on+"\n"+end_on)

        if(offer_branch=="1"){
            txt_offervalid.setText("All")
        }else{
            txt_offervalid.setText(branch_names)
        }

        txt_coupondetails.setText(coupon_count + " " + "coupon" + " " + code)
        txt_coupongrap.setText(coupon_grabbed)
        txt_couponused.setText(used_coupon)

        imge_back.setOnClickListener { v->
            onBackPressed()
        }

    }
}