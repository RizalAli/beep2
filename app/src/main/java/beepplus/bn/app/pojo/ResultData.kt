package com.naruvis.ecom.pojo

val int1 = 0
var int2 = 1

data class UserDetailsResponse(val success : Int, val error : Int,val onebrunei_id:String,val avatar:String,
                               val data:data_user_details);
data class data_user_details(val profile_img : String);


data class MerchantDetailsResponse(val success : Int, val error : Int,val onebrunei_id:String,val avatar:String,
                                   val kontena_id:String,val brpatrick_id:String,val data:data_merchant_details);
data class data_merchant_details(val id : String,val name:String,val mobile_no:String,val email_id:String,
                                val country:String,val profile_img:String);

data class MerchantBranchResponse(val success : Int, val error : Int,val data:List<merchant_branch>);
data class merchant_branch(val id : String,val merchant_id:String,val name:String);

data class MemberDetailsResponse(val success : Int, val error : Int,val data:data_member);
data class data_member(val results : results_member);
data class results_member(val xs_points : String,val logo:String);

data class UploadImageResponse(val success : Int, val error : Int);
data class UpdateorderResponse(val success : Int, val error : Int,val err_msg:String,val succ_msg:String);
data class OrdersCountResponse(val success : Int, val error : Int,val count:String,val pending:String,val active:String);

data class TPointsResponse(val success : Int, val error : Int,val amount:String,
                            val show_date:String,val transaction_id:String,val xs_point:String,
                            val user_name:String,val user_id:String,val errmsg:String)

data class  XsPointsResponse(val success : Int, val error : Int,val point:String,val errmsg:String)

data class CommonResponse(val success : Int, val error : Int,val data:common_data)
data class common_data(val app_version:String,
                        val app_build:String,
                        val program:String,
                        val show_xspoint:String,
                        val show_amount:String,
                        val show_currency:String);

data class OrderResponse(val success : Int, val error : Int,val data:order_result)
data class order_result(val orderdetails:orderdetails,val orderitems :List<orderitems>);

data class orderdetails(val order_id:String,
                           val created_at:String,
                           val total_qty:String,
                           val total_price:String,
                           val total_product:String,
                           val cus_name:String,
                           val cus_mob:String,
                           val status:String,
                           val tbl_no:String);
data class orderitems(val product_name:String,
                        val unit_name:String,
                        val qty:String,
                        val total_price:String,
                        val image:String,
                        val category_id:String
                        );

data class ProfileResponse(val success : Int, val error : Int,val data:results_profile)
data class results_profile(val seller_name:String,
                         val description:String,
                         val logo:String,
                         val map:String,
                         val facebook:String,
                         val instagram:String,
                         val qrlink:String,
                         val banner:String,
                         val url:String);
data class OrderviewResponse(val success : Int, val error : Int,val data:List<results_order>)
data class results_order(val order_id:String,
                          val seller_id:String,
                          val total_qty:String,
                          val total_price:String,
                          val status:String,
                          val cus_name:String,
                          val product_name:String,
                          val created_at:String,
                          val image:String,
                          val tbl_no:String,
                          val total_pricesel:String);
data class ManageProResponse(val success : Int, val error : Int,val data:List<results_manage>)
data class results_manage(val id:String,
                       val name:String,
                       val image:String,
                       val opt_price:String,
                       val category_id:String);


data class MerchantListResponse(val success : Int, val error : Int,val data:List<data_merchant_lis>)
data class data_merchant_lis(val id:String,
                             val name:String,
                             val email_id:String,
                             val mobile_no:String,
                             val created_at:String)

data class HistoryResponse(val success : Int, val error : Int,val data:data_his)
data class data_his(val results:List<results_his>)
data class results_his(val id:String,
                   val credit_type:String,
                   val type:String,
                   val type_text:String,
                   val notes:String,
                   val agent_id:String,
                   val customer_id:String,
                   val points:String,
                   val cur_xspoint:String,
                   val coupon_code:String,
                   val coupon_id:String,
                   val coupon_status:String,
                   val merchant_id:String,
                   val amount:String,
                   val created_by:String,
                   val created_at:String,
                   val updated_at:String,
                   val updated_ip:String,
                   val status:String,
                   val customer_name:String,
                   val customer_mobile:String,
                   val customer_email:String,
                   val customer_xspoints:String);



data class AccessCusResponse(val data: access_details)
data class access_details(val address: String,
                          val address_2: String,
                          val agent_id: String,
                          val area_id: String,
                          val city: String,
                          val country: String,
                          val created_at: String,
                          val created_by: String,
                          val created_from: String,
                          val created_ip: String,
                          val description: Any,
                          val dob: String,
                          val email: String,
                          val enable_exp: Int,
                          val exp_date: String,
                          val exp_msg: String,
                          val expalert_msg: String,
                          val expire: Int,
                          val gender: String,
                          val ic_number: String,
                          val id: String,
                          val landmark: String,
                          val logo: String,
                          val loyality_amount: Int,
                          val loyality_point: Int,
                          val scan_msg:String,
                          val mobile_1: String,
                          val mobile_2: String,
                          val name: String,
                          val otp: Any,
                          val password: String,
                          val pincode: String,
                          val policy_name: String,
                          val policy_no: String,
                          val slug: String,
                          val status: String,
                          val updated_at: String,
                          val updated_by: Any,
                          val updated_ip: String,
                          val username: String,
                          val verified: String,
                          val verify_by: Any,
                          val xs_points: String,
                          val zone_id: Any)

data class ClaimResponse(val success : Int, val error : Int)

data class CountResponse(val success : Int, val error : Int, val count : Int)
data class ScanResponse(val success : Int, val error : Int, val err_code:Int,val err_msg : String,val success_msg:String,val data:List<customer_det>);
data class datam(val customer_det:List<customer_det>)
data class customer_det(val address: String,
                        val address_2: String,
                        val agent_id: String,
                        val area_id: String,
                        val city: String,
                        val country: String,
                        val created_at: String,
                        val created_by: String,
                        val created_from: String,
                        val created_ip: String,
                        val description: String,
                        val dob: String,
                        val email: String,
                        val exp_date: String,
                        val gender: String,
                        val ic_number: String,
                        val id: String,
                        val landmark: String,
                        val logo: String,
                        val loyality_amount: Int,
                        val loyality_point: Int,
                        val scan_msg:String,
                        val mobile_1: String,
                        val mobile_2: String,
                        val name: String,
                        val otp: String,
                        val password: String,
                        val pincode: String,
                        val policy_name: String,
                        val policy_no: String,
                        val slug: String,
                        val status: String,
                        val updated_at: String,
                        val updated_by: String,
                        val updated_ip: String,
                        val username: String,
                        val verified: String,
                        val verify_by: String,
                        val xs_points: String,
                        val zone_id: String)
data class LoginResponse(val success : Int, val error : Int,val mem_id:String,val mem_name:String);
data class LoginBeepPlusResponse(val success : Int, val error : Int,val data:data_log_results,val mem_id:String,val mem_name:String,val results:results_login,
                                    val onebrunei_id:String,val kontena_id:String,val brpatrick_id:String);
data class data_log_results(val user:user_log_results,val merchant:merchant_log_results)
data class user_log_results(val id:String,val first_name:String,val last_name:String)
data class merchant_log_results(val id:String,val name:String,val allow_brpatrick:String,
val allow_onebrunei:String,val allow_kontena:String,val kyc_status:String)

data class results_login(val id:String,
                   val name:String,
                   val username:String,
                   val userpwd:String,
                   val mobile_no:String,
                   val email_id:String,
                   val plan_id:String,
                   val country:String,
                   val currency_code:String,
                   val time_zone:String,
                   val language_id:String,
                   val category_code:String,
                   val kyc_status:String,
                   val created_ip:String,
                   val created_at:String,
                   val updated_ip:String,
                   val updated_at:String,
                   val status:String,
                   val allow_brpatrick:String,
                   val allow_onebrunei:String,
                   val allow_kontena:String)

data class CheckUserOTPResponse(val success : Int, val error : Int,val succ_msg:String,val err_msg:String);
data class PasswordUpdateResponse(val success : Int, val error : Int,val succ_msg:String,val err_msg:String);
data class ChangePasswordUpdateResponse(val success : Int, val error : Int,val succ_msg:String,val err_msg:String);

data class ForgetResponse(val success : Int, val error : Int,val data:data_forget,val err_msg:String,val succ_msg:String);
data class data_forget(val id:String,val otp:String)

data class UploadProBeepPlusResponse(val success : Int, val error : Int);
data class UploadProResponse(val success : Int, val error : Int);
data class UploadResponse(val success : Int, val error : Int,val error_code:Int,val error_msg:String);

data class ManageOffResponse(val success : Int, val error : Int ,val data:data);
data class data(val results:List<results>)
data class results(val id:String,
                   val merchant_id:String,
                   val title:String,
                   val description:String,
                   val offer_terms:String,
                   val img:String,
                   val offer_type:String,
                   val offer_value:String,
                   val offer_point:String,
                   val offer_branch:String,
                   val start_on:String,
                   val end_on:String,
                   val coupon_count:String,
                   val used_coupon:String,
                   val coupon_grabbed:String,
                   val code:String,
                   val branch_names:String,
                   val created_at:String,
                   val updated_at:String,
                   val updated_ip:String,
                   val status:String,
                   val merchant_name:String)




data class AuthResponse(val success : Int, val error : Int,val member : MembersDetails)

data class MembersDetails(val id : String, val name : String, val email : String,
                          val username : String, val profile_image : String, val member_type : String,
                          val mobile : String, val slug : String, val email_activation_code : String,
                          val email_activation_expiration : String, val email_activation_status : String, val email_activated_at : String,
                          val company_name : String,val company_slug:String,val about_company:String,val company_logo:String,
                          val business_types_id:String,val address:String,val country_id:String,val states_admin_code:String,
                          val cities_id:String,val pincode:String,val gstin:String,val gstin_file:String,val credit_period:String,
                          val credit_limit:String,val created_at:String,val updated_at:String,val created_ip:String,val updated_ip:String,
                          val last_login_datetime:String,val last_login_ip:String,val login_status:String,val status:String,val edited_status:String,
                          val referer:String,val device:String,val admin_comment:String,val admin_users_id:String)

data class LatLngResponse(val success : Int, val error : Int,val error_desc:String,
                          val geometry : geometry)
data class geometry(val lat : String, val lng : String)

data class shippingResponse(val success : Int, val error : Int)

data class defaultaddressResponse(val success : Int, val error : Int)
data class addressDeleteResponse(val success : Int, val error : Int)

data class getStoresTimeingResponse(val availSlots:List<availSlots>)
data class availSlots(val value:String,val time:String)

data class getDelSlotsResponse(val success : Int, val error : Int,val delivery:String)

data class couponsResponse(val success : Int, val error : Int,val coupons:List<coupons>)
data class coupons(val coupon_code:String,val coupon_desc:String,val discount_type:String,val coupon_vl_type:String,
                    val coupon_vl:String,val coupon_validity:String,val coupon_ovr_amnt:String,val coupon_threshold:String)

data class anythingResponse(val success : Int, val error : Int)