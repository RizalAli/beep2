package beepplus.bn.app.pojo

data class test(
    val all: All,
    val count: Int,
    val error: Int,
    val success: Int
)