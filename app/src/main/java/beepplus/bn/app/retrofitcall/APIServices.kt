package seller.naruvis.com.retrofitcall

import com.google.gson.JsonObject
import com.naruvis.ecom.pojo.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface APIServices {
    //https://pasarpbg.com/ordercount?usermob=7177163 UploadResponse

   // https://brpatrick.beepxs.solutions/api/merchantupdate?mem_id=1&profimg=

    @GET("user/details") // orderdetails seller_id=42&order_id=521
    fun getUserDetails(
        @Query("user_id") user_id: String
    ): Call<UserDetailsResponse>


    @GET("merchant/details") // orderdetails seller_id=42&order_id=521
    fun getMerchantDetails(
        @Query("merchant_id") merchant_id: String
    ): Call<MerchantDetailsResponse>

    @GET("merchant/branch") // orderdetails seller_id=42&order_id=521
    fun getMerchantBranch(
        @Query("merchant_id") merchant_id: String
    ): Call<MerchantBranchResponse>

    @GET("merchant") // orderdetails seller_id=42&order_id=521
    fun getMemberDetails(
        @Query("merchant_id") merchant_id: String
    ): Call<MemberDetailsResponse>

    @Multipart
    @POST("merchantupdate")
    fun getUploadProfile(

        @Part("mem_id") mem_id: RequestBody,
        @Part profimg: MultipartBody.Part

    ): Call<UploadImageResponse>

    @Multipart
    @POST("user/logoupdate")
    fun getUploadProfileUser(

        @Part("user_id") user_id: RequestBody,
        @Part profimg: MultipartBody.Part

    ): Call<UploadImageResponse>

    @Multipart
    @POST("merchant/logoupdate")
    fun getUploadProfileMerchant(

        @Part("merchant_id") merchant_id: RequestBody,
        @Part profimg: MultipartBody.Part

    ): Call<UploadImageResponse>



    @GET("updateorder") // orderdetails seller_id=42&order_id=521
    fun getupdateorder(
        @Query("seller_id") seller_id: String,
        @Query("order_id") order_id: String,
        @Query("status") status: String
    ): Call<UpdateorderResponse>

    @GET("orderbyseller") // orderdetails seller_id=42&order_id=521
    fun getSellerOrdersCount(
        @Query("seller_id") seller_id: String
    ): Call<OrdersCountResponse>

    @GET("orderbyseller") // orderdetails seller_id=42&order_id=521
    fun getOrdersCount(
        @Query("seller_id") seller_id: String
    ): Call<JsonObject>

    @Multipart
    @POST("addproduct")
    fun getAddProBeepPlus(
        // www.onebrunei.de/api/addproduct?seller_id=42&idProfile=&product_name=test pro&main_category=3&price=2&action=AddProduct

        @Part("seller_id") seller_id: RequestBody,
        @Part("kontena_id") kontena_id: RequestBody,
        @Part("onebrunei_id") onebrunei_id: RequestBody,
        @Part idProfile: MultipartBody.Part,
        @Part("product_name") product_name: RequestBody,
        @Part("main_category") main_category: RequestBody,
        @Part("price") price: RequestBody,
        @Part("action") action: RequestBody


        /*@Part merchant_id: MultipartBody.Part,
    @Part title: MultipartBody.Part,
    @Part offer_type: MultipartBody.Part,
    @Part offer_branch: MultipartBody.Part,
    @Part description: MultipartBody.Part,
    @Part offer_terms: MultipartBody.Part,
    @Part start_on: MultipartBody.Part,
    @Part end_on: MultipartBody.Part,
    @Part coupon_count: MultipartBody.Part,
    @Part code: MultipartBody.Part,
    @Part image: MultipartBody.Part*/

    ): Call<UploadProBeepPlusResponse>

    @GET("merchantissuepoint") // orderdetails seller_id=42&order_id=521
    fun getTPoints(
        @Query("amount") amount: String,
        @Query("mem_id") mem_id: String,
        @Query("merchant_id") merchant_id: String,
        @Query("xspoint") xspoint: String,
        @Query("status") status: String
    ): Call<TPointsResponse>

    @GET("getpoints") // orderdetails seller_id=42&order_id=521
    fun getXsPoints(
        @Query("amount") amount: String
    ): Call<XsPointsResponse>

    @GET("merchant/commondata") // orderdetails seller_id=42&order_id=521
    fun getCommondata(
    ): Call<CommonResponse>

    @GET
    @Streaming
    fun downloadImage(
        @Url fileUrl: String?
    ): Call<ResponseBody?>?

    @GET("orderdetails") // orderdetails seller_id=42&order_id=521
    fun getOrderDetails(
        @Query("seller_id") seller_id: String,
        @Query("order_id") order_id: String
    ): Call<OrderResponse>

    @GET("seller")
    fun getProfile(
        @Query("seller_id") seller_id: String
    ): Call<ProfileResponse>

    @GET("orderview")
    fun getMarketpalce(
        @Query("seller_id") seller_id: String,
        @Query("status") status: String
    ): Call<OrderviewResponse>

    @GET("manageproduct")
    fun getManagePro(
        @Query("seller_id") seller_id: String
    ): Call<ManageProResponse>

    @Multipart
    @POST("addproduct")
    fun getAddPro(
        // www.onebrunei.de/api/addproduct?seller_id=42&idProfile=&product_name=test pro&main_category=3&price=2&action=AddProduct

        @Part("seller_id") seller_id: RequestBody,
        @Part idProfile: MultipartBody.Part,
        @Part("product_name") product_name: RequestBody,
        @Part("main_category") main_category: RequestBody,
        @Part("price") price: RequestBody,
        @Part("action") action: RequestBody


        /*@Part merchant_id: MultipartBody.Part,
        @Part title: MultipartBody.Part,
        @Part offer_type: MultipartBody.Part,
        @Part offer_branch: MultipartBody.Part,
        @Part description: MultipartBody.Part,
        @Part offer_terms: MultipartBody.Part,
        @Part start_on: MultipartBody.Part,
        @Part end_on: MultipartBody.Part,
        @Part coupon_count: MultipartBody.Part,
        @Part code: MultipartBody.Part,
        @Part image: MultipartBody.Part*/

    ): Call<UploadProResponse>



    @GET("merchant/lists")
    fun getMerchantList(
        @Query("user_id") amount: String
    ): Call<MerchantListResponse>

    @GET("paymenthistory")
    fun getHistory(
        @Query("merchant_id") amount: String,
        @Query("fromdate") fromdate: String,
        @Query("todate") todate: String,
        @Query("status") status: String
    ): Call<HistoryResponse>

    @GET("customer")
    fun getAcessCustomer(
        @Query("mem_id") amount: String
    ): Call<AccessCusResponse>

    @GET("amountinsert")
    fun getClaimed(
        @Query("amount") amount: String,
        @Query("mem_id") mem_id: String,
        @Query("merchant_id") merchant_id: String,
        @Query("coupon_code") coupon_code: String
    ): Call<ClaimResponse>

    @GET("manageoffers")
    fun getManage(
        @Query("merchant_id") merchant_id: String,
        @Query("status") status: String
    ): Call<ManageOffResponse>

    @GET("offer-post")
    fun upload(

        @Query("merchant_id") merchant_id: String,
        @Query("offer_title") title: String,
        @Query("offer_type") offer_type: String,
        //@Part("offer_for[]") offer_branch:RequestBody,
        //@Part("offer_for[]") offer_for:ArrayList<String>,
        //@Part("offer_for[]") offer_for: List<RequestBody?>,
        @Query("offer_for") offer_branch: String,
        @Query("offer_val") description: String,
        @Query("offer_terms") offer_terms: String,
        @Query("start_on") start_on: String,
        @Query("end_on") end_on: String,
        @Query("coupon_count") coupon_count: String,
        @Query("coupon_code") code: String

    ): Call<UploadProResponse>



    @Multipart
    @POST("offer-post")
    fun getAddOffer(

        @Part("merchant_id") merchant_id: RequestBody,
        @Part("offer_title") title: RequestBody,
        @Part("offer_type") offer_type: RequestBody,
        //@Part("offer_for[]") offer_branch: RequestBody,
        //@Part("offer_for[]") offer_for:ArrayList<String>,
        //@Part("offer_for[]") offer_for: List<RequestBody?>,
        @Part("offer_for") offer_branch:RequestBody,
        @Part("offer_val") description: RequestBody,
        @Part("offer_terms") offer_terms: RequestBody,
        @Part("start_on") start_on: RequestBody,
        @Part("end_on") end_on: RequestBody,
        @Part("limit_type") limit_type: RequestBody,
        @Part("coupon_count") coupon_count: RequestBody,
        @Part("coupon_code") code: RequestBody,
        @Part profimg: MultipartBody.Part


    ): Call<UploadResponse>

   /* @Multipart
    @POST("v1/group/new")
    fun newGroup(
        @Header("token") token: String?,
        @Part photo: MultipartBody.Part?,
        @Part("title") subject: RequestBody?,
        @Part("members[]") members: List<RequestBody?>?
    ): Call<MyResponse?>?*/

    @Multipart
    @POST("offer-post")
    fun getAddOfferWithoutImage(
        @Part("merchant_id") merchant_id: RequestBody,
        @Part("offer_title") title: RequestBody,
        @Part("offer_type") offer_type: RequestBody,
        //@Part("offer_for[]") offer_for:ArrayList<String>,
        //@Part("offer_for[]") offer_for: List<RequestBody?>,
        //@Part("offer_for[]") offer_branch:RequestBody,
        @Part("offer_for") offer_branch:RequestBody,
        @Part("offer_val") description: RequestBody,
        @Part("offer_terms") offer_terms: RequestBody,
        @Part("start_on") start_on: RequestBody,
        @Part("end_on") end_on: RequestBody,
        @Part("limit_type") limit_type: RequestBody,
        @Part("coupon_count") coupon_count: RequestBody,
        @Part("coupon_code") code: RequestBody
    ): Call<UploadResponse>
    @Multipart
    @POST("offer-post")
    fun getAddOfferWithoutImage_offer(
        @Part("merchant_id") merchant_id: RequestBody,
        @Part("offer_title") title: RequestBody,
        @Part("offer_type") offer_type: RequestBody,
        //@Part("offer_for[]") offer_for:ArrayList<String>,
        //@Part("offer_for[]") offer_for: List<RequestBody?>,
        // @Part("offer_for[]") offer_branch:RequestBody,
        //@Part("offer_for") offer_branch:RequestBody,
        @Part("offer_val") description: RequestBody,
        @Part("offer_terms") offer_terms: RequestBody,
        @Part("start_on") start_on: RequestBody,
        @Part("end_on") end_on: RequestBody,
        @Part("limit_type") limit_type: RequestBody,
        @Part("coupon_count") coupon_count: RequestBody,
        @Part("coupon_code") code: RequestBody
    ): Call<UploadResponse>




    @Multipart
    @POST("offer-post")
    fun getAddOffer1(
        //@Part("description") description: RequestBody?,
        @Part file: MultipartBody.Part?
    ): Call<ResponseBody?>?

    @GET("ordercount")
    fun getCount(
        @Query("usermob") type: String
    ): Call<CountResponse>

    @GET("couponvalid")
    fun getCouponValid(
        @Query("merchant_id") merchant_id: String,
        @Query("coupon_code") coupon_code: String
    ): Call<ScanResponse>

    @GET("merchant/login")
    fun getLogin(
        @Query("username") username: String,
        @Query("password") password: String
    ): Call<LoginBeepPlusResponse>

    @GET("user/login")
    fun getLogin_user(
        @Query("username") username: String,
        @Query("password") password: String
    ): Call<LoginBeepPlusResponse>

    @GET("forgetuserpwd")
    fun getForget(
        @Query("email_id") email_id: String
    ): Call<ForgetResponse>

    @GET("checkuserotp")
    fun getOtpVerify(
        @Query("user_id") email_id: String,
        @Query("otp") otp: String
    ): Call<CheckUserOTPResponse>

    @GET("userpwdupdate")
    fun getPasswordUpdate(
        @Query("user_id") email_id: String,
        @Query("password") otp: String
    ): Call<PasswordUpdateResponse>

    @GET("userpwdchange")
    fun getChangePassword(
        @Query("user_id") user_id: String,
        @Query("password") password: String,
        @Query("curpassword") curpassword: String
    ): Call<ChangePasswordUpdateResponse>

    @GET("auth")
    fun authL(
        @Query("type") type: String,
        @Query("m") mobile: String,
        @Query("tocken_id") token_id: String,
        @Query("device_id") device_id: String
    ): Call<AuthResponse>

    @GET("auth")
    fun authR(
        @Query("type") type: String,
        @Query("m") mobile: String,
        @Query("n") name: String,
        @Query("device") device: String,
        @Query("tocken_id") token_id: String,
        @Query("device_id") device_id: String
    ): Call<AuthResponse>


    @GET("getLatLongByAddress")
    fun getLatLongByAddress(
        @Query("a") a: String
    ): Call<LatLngResponse>


    @GET("shipping-post")
    fun shipping_post(
        @Query("mem_id") mem_id: String,
        @Query("ship_Name") ship_Name: String,
        @Query("ship_Street") ship_Street: String,
        @Query("ship_Landmark") ship_Landmark: String,
        @Query("ship_Phoneno") ship_Phoneno: String,
        @Query("ship_pincode") ship_pincode: String,
        @Query("ship_areaid") ship_areaid: String,
        @Query("ship_area") ship_area: String,
        @Query("ship_apartment") ship_apartment: String,
        @Query("ship_City") ship_City: String,
        @Query("ship_State") ship_State: String,
        @Query("ship_Country") ship_Country: String,
        @Query("action") action: String,
        @Query("edit_id") edit_id: String,
        @Query("txt_lat") txt_lat: String,
        @Query("txt_lang") txt_lang: String
    ): Call<shippingResponse>

    @GET("defaultaddress")
    fun get_defaultaddress(
        @Query("id") id: String,
        @Query("type") type: String,
        @Query("mem_id") mem_id: String
    ): Call<defaultaddressResponse>

    @GET("address-delete")
    fun addressDelete(
        @Query("deleteid") id: String,
        @Query("mem_id") mem_id: String
    ): Call<addressDeleteResponse>


    @GET("getStoresTimeing")
    fun getStoresTimeing(
        @Query("mem_id") mem_id: String,
        @Query("d") d: String
    ): Call<getStoresTimeingResponse>

    @GET("getDelSlots")
    fun getDelSlots(
        @Query("mem_id") mem_id: String,
        @Query("si") si: String
    ): Call<getDelSlotsResponse>

    @GET("coupons")
    fun getcoupons(
        @Query("cus_id") mem_id: String
    ): Call<couponsResponse>


    @FormUrlEncoded
    @POST("buy-anything")
    fun buy_anything(

        @Field("mem_id") mem_id: String,
        @Field("whatsapp") whatsapp: String,
        @Field("orderType") orderType: String,
        @Field("product[]") product: ArrayList<String>,
        @Field("quant[]") quant: ArrayList<String>

    ): Call<anythingResponse>


}
// https://www.naruvis.com/api/gr/v1/commondatas
// // https://seller.naruvis.com/api/v3/getLatLongByAddress?a=419,5th%20cross,%20naidu%20layout,%20electronic%20city
// https://www.naruvis.com/api/gr/v1/sendOTP?type=login&m=9789328078&c=0

//Constants.s_setUsDefaultAddressDatas_URL+"id="+s_shippAddressIdResult + "&type="+s_trackOrderTypeAns + "&mem_id="+s_customerMemberId;
 //  Constants.s_deleteShippingAddress_URL+"deleteid="+s_shippAddressIdResult + "&mem_id="+s_customerMemberId

 // https://www.naruvis.com/api/gr/v1/buy-anything

/*
product => array of product names
quant => Array of Req. qty
unit => Array of Req. qty units

mem_id
whatsapp, orderType*/
