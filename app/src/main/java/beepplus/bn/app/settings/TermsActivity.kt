package beepplus.bn.app.settings

import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import beepplus.bn.app.R
import kotlinx.android.synthetic.main.activity_terms.*
import kotlinx.android.synthetic.main.activity_terms.toolbar
import kotlinx.android.synthetic.main.activity_web2.*
import kotlinx.android.synthetic.main.content_web.*

class TermsActivity : AppCompatActivity() {

    private val url = "https://www.beep.solutions/terms-and-conditions"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms)
        //val myWebView = findViewById(R.id.webview_load) as WebView
        //val awv_progressBar = findViewById(R.id.awv_progressBar) as ProgressBar;
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_small_white_24)
        toolbar.setNavigationOnClickListener { v->
            onBackPressed()
        }

        toolbar.setTitle("TERMS OF USE")
        // Get the web view settings instance
        val settings = webview_load.settings

        // Enable java script in web view
        settings.javaScriptEnabled = true

        // Enable and setup web view cache
        settings.setAppCacheEnabled(true)
        settings.cacheMode = WebSettings.LOAD_DEFAULT
        settings.setAppCachePath(cacheDir.path)


        // Enable zooming in web view
        settings.setSupportZoom(true)
        settings.builtInZoomControls = true
        settings.displayZoomControls = true


        // Zoom web view text
        settings.textZoom = 125


        // Enable disable images in web view
        settings.blockNetworkImage = false
        // Whether the WebView should load image resources
        settings.loadsImagesAutomatically = true


        // More web view settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            settings.safeBrowsingEnabled = true  // api 26
        }
        //settings.pluginState = WebSettings.PluginState.ON
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.mediaPlaybackRequiresUserGesture = false


        // More optional settings, you can enable it by yourself
        settings.domStorageEnabled = true
        settings.setSupportMultipleWindows(true)
        settings.loadWithOverviewMode = true
        settings.allowContentAccess = true
        settings.setGeolocationEnabled(true)
        settings.allowUniversalAccessFromFileURLs = true
        settings.allowFileAccess = true

        // WebView settings
        webview_load.fitsSystemWindows = true


        webview_load.setLayerType(View.LAYER_TYPE_HARDWARE, null)

        // Set web view client
        webview_load.webViewClient = object: WebViewClient(){
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                // Page loading started
                // Do something
                toast("Page loading.")

                // Enable disable back forward button
                //button_back.isEnabled = web_view.canGoBack()
                //button_forward.isEnabled = web_view.canGoForward()
            }

            override fun onPageFinished(view: WebView, url: String) {
                // Page loading finished
                // Display the loaded page title in a toast message
                toast("Page loaded: ${view.title}")

                // Enable disable back forward button
                //button_back.isEnabled = web_view.canGoBack()
                //button_forward.isEnabled = web_view.canGoForward()
            }
        }


        // Set web view chrome client
        webview_load.webChromeClient = object: WebChromeClient(){
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                awv_progressBar.progress = newProgress
            }
        }

        webview_load.stopLoading()
        webview_load.loadUrl(url)

    }

    // Handle back button press in web view
    override fun onBackPressed() {
        if (webview_load.canGoBack()) {
            // If web view have back history, then go to the web view back history
            webview_load.goBack()
            toast("Going to back history")
        } else {
            // Ask the user to exit the app or stay in here
            super.onBackPressed()
            //showAppExitDialog()
        }
    }
}


// Extension function to show toast message
fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}