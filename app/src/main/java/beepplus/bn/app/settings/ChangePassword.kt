package beepplus.bn.app.settings

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import beepplus.bn.app.R
import beepplus.bn.app.auth_screen.LoginAuth
import beepplus.bn.app.sql.DatabaseHandler
import com.naruvis.ecom.pojo.ChangePasswordUpdateResponse
import com.naruvis.ecom.pojo.LoginBeepPlusResponse
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_login_auth.*
import kotlinx.android.synthetic.main.content_ch_pass.*
import kotlinx.android.synthetic.main.content_ch_pass.btn_submit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl


class ChangePassword : AppCompatActivity() {

    var str_current_pass = ""
    var str_new_pass = ""
    var str_confirm_pass = ""
    var str_mem = ""



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_small_white_24)
        toolbar.setNavigationOnClickListener { v->
            onBackPressed()
        }

        val isMem_His = DatabaseHandler(applicationContext)
        val mem = isMem_His.isMem_His
        val sb_member = StringBuilder()
        sb_member.append("")
        sb_member.append(mem)
        str_mem = sb_member.toString()

        btn_submit.setOnClickListener { v->

            str_current_pass = ct_p_edit_text.text.toString().trim()
            str_new_pass = new_p_edit_text.text.toString().trim()
            str_confirm_pass = conf_p_edit_text.text.toString().trim()


            if(!str_current_pass.isEmpty()){
                ct_p_text_input.error = null

                if(!str_new_pass.isEmpty()){
                    new_p_text_input.error = null

                    if(!str_confirm_pass.isEmpty()){
                        conf_p_text_input.error = null

                        if(str_new_pass==str_confirm_pass){
                            conf_p_text_input.error = null

                            Log.i("Information_do_late","===================="+str_mem)
                            Log.i("Information_do_late","===================="+str_current_pass)
                            Log.i("Information_do_late","===================="+str_confirm_pass)

                            password_update(str_mem,str_current_pass,str_confirm_pass)

                        }else{
                            conf_p_text_input.error = "New password and confirm password mismatched."
                        }

                    }else{
                        conf_p_text_input.error = "Enter the confirm password"
                    }

                }else{
                    new_p_text_input.error = "Enter the new password"
                }

            }else{
                ct_p_text_input.error = "Enter the current password"
            }

        }

        showSoftKeyboard(ct_p_edit_text)

    }

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }


    fun password_update(member_id:String,current_pass:String,password: String){
        // ======================================================== MEMBERACTION ==================================================================

        var progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL) //BASE_BEEP_PLUS_URL --- all // BASE_BEEP_URL --- BR -PATRICK
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getChangePassword(member_id,password,current_pass)


        call.enqueue(object : Callback<ChangePasswordUpdateResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<ChangePasswordUpdateResponse>, response: Response<ChangePasswordUpdateResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    val succ_msg = response.body()!!.succ_msg
                    Toast.makeText(this@ChangePassword, succ_msg, 1000).show()
                    progressDialog.dismiss()
                    logout_password(succ_msg)
                    //onBackPressed()


                }else if(str_error==1){
                    val err_msg = response.body()!!.err_msg
                    Toast.makeText(this@ChangePassword, err_msg, 1000).show()
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<ChangePasswordUpdateResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------


    fun logout_password(succ_msg:String) {
        val alertDialog = AlertDialog.Builder(this@ChangePassword)
        alertDialog.setTitle("Change Password")
        alertDialog.setMessage(succ_msg)
        alertDialog.setPositiveButton("OK") { dialog, id ->
            onBackPressed()
        }
        //alertDialog.setNegativeButton("Not Now") { dialog, id -> dialog.dismiss() }
        alertDialog.show()
    }

}