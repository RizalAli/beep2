package beepplus.bn.app.settings

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import beepplus.bn.app.R
import beepplus.bn.app.common.FilePath
import beepplus.bn.app.sql.DatabaseHandler
import beepplus.bn.app.sql.DatabaseHandlerTouchId
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.naruvis.ecom.pojo.MerchantDetailsResponse
import com.naruvis.ecom.pojo.UploadImageResponse
import com.naruvis.ecom.pojo.UserDetailsResponse
import kotlinx.android.synthetic.main.activity_settings.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl
import java.io.File


class SettingsCommon : AppCompatActivity() {
    lateinit var progressDialog:ProgressDialog;
    var str_m_id = ""
    var str_email = ""
    var str_name = ""
    var str_mem = ""

    var str_touch_id= ""

    var PERMISSION_CODE = 1000;
    var IMAGE_CAPTURE_CODE = 1001
    var image_uri: Uri? = null
    var IMAGE_PICK_CODE = 1000;
    var mediaPath = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        imge_back.setOnClickListener { v->
            onBackPressed()
        }

        val isMem_Tid = DatabaseHandlerTouchId(applicationContext)
        val mem_t_id = isMem_Tid.isMem_touch_id
        val sb_touch_id = StringBuilder()
        sb_touch_id.append("")
        sb_touch_id.append(mem_t_id)
        str_touch_id = sb_touch_id.toString()

        val isMem_His = DatabaseHandler(applicationContext)
        val mem = isMem_His.isMem_His
        val sb_member = StringBuilder()
        sb_member.append("")
        sb_member.append(mem)
        str_mem = sb_member.toString()

        val isMem_m_id = DatabaseHandler(applicationContext)
        val mem_m_id= isMem_m_id.isMem_merchant_id
        val sb_mem_m_id = StringBuilder()
        sb_mem_m_id.append("")
        sb_mem_m_id.append(mem_m_id)
        str_m_id = sb_mem_m_id.toString()

        val isMem_Email = DatabaseHandler(applicationContext)
        val mem_email = isMem_Email.isMem_email
        val sb_mem = StringBuilder()
        sb_mem.append("")
        sb_mem.append(mem_email)
        str_email = sb_mem.toString()

        val isMem_name = DatabaseHandler(applicationContext)
        val mem_name = isMem_name.isMem_name
        val sb_mem_n = StringBuilder()
        sb_mem_n.append("")
        sb_mem_n.append(mem_name)
        str_name = sb_mem_n.toString()

        merchant_details(str_m_id)

        val pinfo = packageManager.getPackageInfo(packageName, 0)
        val versionNumber = pinfo.versionCode
        val versionName = pinfo.versionName
        var str_device_id = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)


        txt_rv.setText(versionName)
        txt_d_rid.setText(str_device_id)



        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)!!.state == NetworkInfo.State.CONNECTED) {
            txt_w_rid.setText("Mobile Data")
        }else if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)!!.state == NetworkInfo.State.CONNECTED) {
            txt_w_rid.setText("Wifi")
        }
        /*img_company_logo
        txt_name_c
        txt_sub_c
        txt_address_c
        btn_upload_profile_photo*/

        btn_app_terms.setOnClickListener { v->

           /* val uri: Uri =
                Uri.parse("https://www.beep.solutions/terms-and-conditions") // missing 'http://' will cause crashed
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)*/

            val intent = Intent(
                this@SettingsCommon,
                beepplus.bn.app.settings.TermsActivity::class.java
            )
            startActivity(intent)

        }

        btn_change_password.setOnClickListener { v->

            val intent = Intent(
                this@SettingsCommon,
                beepplus.bn.app.settings.ChangePassword::class.java
            )
            startActivity(intent)

        }

        btn_upload_profile_photo.setOnClickListener { v->

            showCustomDialog()

        }

        if(str_touch_id=="1"){
            toggle_switch.isChecked = true
        }else{
            toggle_switch.isChecked = false
        }

        toggle_switch.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked == true) {

                    val db = DatabaseHandlerTouchId(this@SettingsCommon)
                    db.Member_touch_id("1")

                    Log.i("aaa", "Testing_fully------------------toggle");
                } else {

                    val db = DatabaseHandlerTouchId(this@SettingsCommon)
                    db.resetTables()
                    Log.i("bbb", "Testing_fully------------------toggle");
                }

            }
        })


    }


    fun merchant_details(
        merchant_id: String
    ){
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("merchant_details", " 123_456_789se " + merchant_id)


        //http://www.beepxs.solutions/api/merchant/lists?user_id=2

        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getMerchantDetails(merchant_id)


        call.enqueue(object : Callback<MerchantDetailsResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<MerchantDetailsResponse>,
                response: Response<MerchantDetailsResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("Merchant_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                val str_success = response.body()!!.success
                val str_error = response.body()!!.error
                val avatar = response.body()!!.avatar

                if (str_success == 1) {

                    //val data = response.body()!!.data
                    val results = response.body()!!.data

                    val name = results.name
                    val email_id = results.email_id
                    val country = results.country
                    val profile_img = results.profile_img

                    Log.i("data_data", " 123_456_789se " + results)

                    txt_name_c.setText(name)
                    txt_sub_c.setText(email_id)
                    txt_address_c.setText(country)

                    txt_name_u.setText(str_name)
                    txt_sub_u.setText("+673" + str_email)
                    if (profile_img != null) {
                        if (!profile_img.isEmpty()) {
                            Glide.with(this@SettingsCommon)
                                .load("http://www.beepxs.solutions/uploads/" + profile_img)
                                .into(img_company_logo)
                        } else {
                            Glide.with(this@SettingsCommon)
                                .load(avatar)
                                .into(img_company_logo)
                        }
                    } else {
                        Glide.with(this@SettingsCommon)
                            .load(avatar)
                            .into(img_company_logo)
                    }

                    user_details(str_mem)
                    //progressDialog.dismiss()

                } else if (str_error == 1) {
                    progressDialog.dismiss()
                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<MerchantDetailsResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------

    fun user_details(
        str_mem: String
    ){
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("merchant_details", " 123_456_789se " + str_mem)


        //http://www.beepxs.solutions/api/merchant/lists?user_id=2


        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getUserDetails(str_mem)


        call.enqueue(object : Callback<UserDetailsResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<UserDetailsResponse>,
                response: Response<UserDetailsResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("Merchant_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                val str_success = response.body()!!.success
                val str_error = response.body()!!.error
                val avatar = response.body()!!.avatar

                if (str_success == 1) {

                    //val data = response.body()!!.data
                    val results = response.body()!!.data

                    val profile_img = results.profile_img

                    Log.i("data_data", " 123_456_789se " + results)

                    if (profile_img != null) {
                        if (!profile_img.isEmpty()) {
                            Glide.with(this@SettingsCommon)
                                .load("http://www.beepxs.solutions/uploads/" + profile_img)
                                .into(img_company_logo)
                        } else {
                            Glide.with(this@SettingsCommon)
                                .load(avatar)
                                .into(img_user_logo)
                        }
                    } else {
                        Glide.with(this@SettingsCommon)
                            .load(avatar)
                            .into(img_user_logo)
                    }

                    progressDialog.dismiss()

                } else if (str_error == 1) {
                    progressDialog.dismiss()
                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<UserDetailsResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------


    /// ============================================================== Image

    private fun showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        val viewGroup = findViewById<ViewGroup>(R.id.content)

        //then we will inflate the custom alert dialog xml that we created
        val dialogView: View =
            LayoutInflater.from(this).inflate(R.layout.my_dialog, viewGroup, false)

        val txt_camera: TextView = dialogView.findViewById(R.id.txt_camera)
        val txt_gallery: TextView = dialogView.findViewById(R.id.txt_gallery)

        val btn_close: TextView = dialogView.findViewById(R.id.btn_close)

        //Now we need an AlertDialog.Builder object
        val builder = AlertDialog.Builder(this)

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView)

        //finally creating the alert dialog and displaying it
        val alertDialog = builder.create()
        alertDialog.show()

        txt_camera.setOnClickListener { v->
            PERMISSION_CODE = 1000;
            camera_fn()
            alertDialog.dismiss()
        }
        txt_gallery.setOnClickListener { v->
            PERMISSION_CODE = 1001;
            gallery_fn()
            alertDialog.dismiss()
        }


        btn_close.setOnClickListener { v->
            alertDialog.dismiss()
        }
    }
    private fun camera_fn() {
        //if system os is Marshmallow or Above, we need to request runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (this.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED){
                //permission was not enabled
                val permission = arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                //show popup to request permission
                requestPermissions(permission, PERMISSION_CODE)
            }
            else{
                //permission already granted
                openCamera()
            }
        }
        else{
            //system os is < marshmallow
            openCamera()
        }
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }
    private fun gallery_fn() {
        //if system os is Marshmallow or Above, we need to request runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (this.checkSelfPermission(Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED ||
                this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED){
                //permission was not enabled
                val permission = arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                //show popup to request permission
                requestPermissions(permission, PERMISSION_CODE)
            }
            else{
                //permission already granted
                pickImageFromGallery();
            }
        }
        else{
            //system os is < marshmallow
            pickImageFromGallery();
        }
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        //called when user presses ALLOW or DENY from Permission Request Popup
        when(requestCode){


            PERMISSION_CODE -> {

                Log.i("requestCodeSSSSSS", "" + requestCode)

                if (grantResults.size > 0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    //permission from popup was granted
                    if (PERMISSION_CODE == 1000) {
                        openCamera()
                    } else {
                        camera_fn()
                    } // if

                } else {
                    //permission from popup was denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        //called when image was captured from camera intent
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_CAPTURE_CODE){
            //set image captured to image view



            mediaPath = FilePath.getPath(this, image_uri)
            img_company_logo.setImageURI(image_uri)

            uploadImage(str_mem)

            /*   val file = File(mediaPath)
               val fileInBytes = file.length().toFloat()
               val fileSizeInKB = fileInBytes / 1024

               Log.i("fileSizeInKB","666666666666666666666" + fileSizeInKB)

               if (fileSizeInKB <= 1024 * 2) {
                   //mediaPath = image_uri.toString()
                   img_camera.setImageURI(image_uri)
               }else{
                   Toast.makeText(
                       context,
                       "Select Images less than 2 MB ",
                       Toast.LENGTH_LONG
                   ).show()
               }*/
            // image_v.setImageURI(selectedImage as Uri?)


        }else  if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){

            //   image_v.setImageURI(data?.data)

            // Get the Image from data

            // Get the Image from data
            val selectedImage = data!!.data
            val filePathColumn =
                arrayOf(MediaStore.Images.Media.DATA)

            val cursor: Cursor = this.contentResolver.query(
                selectedImage!!,
                filePathColumn,
                null,
                null,
                null
            )!!
            cursor.moveToFirst()

            val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
            mediaPath = cursor.getString(columnIndex)
            //str1.setText(mediaPath)
            // Set the Image in ImageView for Previewing the Media
            // Set the Image in ImageView for Previewing the Media

            img_company_logo.setImageBitmap(BitmapFactory.decodeFile(mediaPath))

            uploadImage(str_m_id)


            /*   val file = File(mediaPath)
               val fileInBytes = file.length().toFloat()
               val fileSizeInKB = fileInBytes / 1024

               if (fileSizeInKB <= 1024 * 2) {
                   img_camera.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
               }else{
                   Toast.makeText(
                       context,
                       "Select Images less than 2 MB ",
                       Toast.LENGTH_LONG
                   ).show()
               }*/

            //image_v.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
            cursor.close()

            /* try {
                 is_image = contentResolver.openInputStream(data!!.data!!)!!
                 imageBytes  = getBytes(is_image!!)
             } catch (e: IOException) {
                 e.printStackTrace()
             }*/

        }
    }


    private fun uploadImage(
        str_mem: String
    ) {

        val progressDialog = ProgressDialog(this@SettingsCommon)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val gson: Gson = GsonBuilder()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        val service = retrofit.create(APIServices::class.java)

        Log.i("shari_kontena_id", "000000000000000000000000" + str_m_id)

        val merchant_id: RequestBody = RequestBody.create(
            MediaType.parse("multipart/form-data"),
            str_m_id
        )

        val file = File(mediaPath)

        // Parsing any Media type file
        Log.i("file_file", "000000000000000000000000" + file)
        Log.i("file_file", "000000000000000000000000" + file.getName())
        // Parsing any Media type file
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val image_upload = MultipartBody.Part.createFormData("profimg", file.getName(), requestBody)

        Log.i("khanbody_body", "000000000000000000000000" + requestBody)
        Log.i("image_upload", "000000000000000000000000" + image_upload.headers())
        Log.i("image_upload", "000000000000000000000000" + image_upload.body())


        val call: Call<UploadImageResponse> = service.getUploadProfileMerchant(
            merchant_id,
            image_upload
        ); //body
        // mProgressBar.setVisibility(View.VISIBLE)

        /*  val call: Call<UploadResponse> = service.upload(str_mem,str_offer_title,str_radio,"1",str_Offer_desc,"offer_terms",str_Start_ON,str_Expire_ON,
              str_No_of_coupon,str_Coupon_Code);*/

        call.enqueue(object : Callback<UploadImageResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<UploadImageResponse>,
                response: Response<UploadImageResponse>
            ) {

                val statusCode = response.code()
                val user = response.body()

                progressDialog.dismiss()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {
                    Toast.makeText(
                        this@SettingsCommon,
                        "Upload Image Successful.",
                        Toast.LENGTH_SHORT
                    ).show()

                }

                Log.i("str_success", " 123_456_789se " + str_success)


            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<UploadImageResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }


}