package beepplus.bn.app.auth_screen.forget

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import beepplus.bn.app.R
import beepplus.bn.app.common.EmailValidation
import com.google.android.material.snackbar.Snackbar
import com.naruvis.ecom.pojo.ForgetResponse
import com.naruvis.ecom.pojo.LoginBeepPlusResponse
import kotlinx.android.synthetic.main.activity_forgot_email.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl


class ForgotEmail : AppCompatActivity() {

    var email_add = ""
    var forget_mem_id = ""
    var emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_email)


        image_back.setOnClickListener { v->
            onBackPressed()
        }

        showSoftKeyboard(edit_email_add)


        btn_submit.setOnClickListener { v->

            email_add = edit_email_add.text.toString().trim()

            if(!email_add.isEmpty()){

            if(EmailValidation.isEmailValid(email_add.toString())){
                    login_loaded(email_add);
                }else{
                    val snackbar = Snackbar
                        .make(v, "Invalid email", Snackbar.LENGTH_LONG)
                    snackbar.show()
                }

            }else{
                val snackbar = Snackbar
                    .make(v, "Enter your email id", Snackbar.LENGTH_LONG)
                snackbar.show()
            }

        }

    }

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }


    fun login_loaded(email: String){
        // ======================================================== MEMBERACTION ==================================================================

        var progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL) //BASE_BEEP_PLUS_URL --- all // BASE_BEEP_URL --- BR -PATRICK
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getForget(email)


        call.enqueue(object : Callback<ForgetResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<ForgetResponse>,
                response: Response<ForgetResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    Log.i("useraaaaaaaa", " 123_456_789se " + response.body()!!.data)
                    val data = response.body()!!.data
                    forget_mem_id = data.id
                    val otp = data.otp

                    val intent = Intent(this@ForgotEmail, ForgetVerify::class.java)
                    intent.putExtra("forget_mem_id",forget_mem_id)
                    intent.putExtra("forget_email",email)
                    intent.putExtra("otp",otp)
                    startActivity(intent)
                    //finish()

                    progressDialog.dismiss()

                } else if (str_error == 1) {
                    val err_msg = response.body()!!.err_msg

                    Toast.makeText(this@ForgotEmail, err_msg, 1000).show()
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<ForgetResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------


}
