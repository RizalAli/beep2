package beepplus.bn.app.auth_screen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import beepplus.bn.app.R
import beepplus.bn.app.home.Home
import beepplus.bn.app.sql.DatabaseHandler
import beepplus.bn.app.sql.DatabaseHandlerTouchId
import java.util.concurrent.Executor


class SplashScreen : AppCompatActivity() {

    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        var str_mem = "";
        var str_touch_id = "";

        val isMem_Tid = DatabaseHandlerTouchId(applicationContext)
        val mem_t_id = isMem_Tid.isMem_touch_id
        val sb_touch_id = StringBuilder()
        sb_touch_id.append("")
        sb_touch_id.append(mem_t_id)
        str_touch_id = sb_touch_id.toString()

        val isMem_His = DatabaseHandler(applicationContext)
        val mem = isMem_His.isMem_His
        val sb_mem = StringBuilder()
        sb_mem.append("")
        sb_mem.append(mem)
        str_mem = sb_mem.toString()

        Log.i("str_mem_str_mem","" + str_mem)


       // val executor = ContextCompat.getMainExecutor(this)
       // val biometricManager = BiometricManager.from(this)

        if(str_touch_id=="1"){

            executor = ContextCompat.getMainExecutor(this)
            biometricPrompt = BiometricPrompt(this, executor,
                object : BiometricPrompt.AuthenticationCallback() {
                    override fun onAuthenticationError(errorCode: Int,
                                                       errString: CharSequence) {
                        super.onAuthenticationError(errorCode, errString)
                        Toast.makeText(applicationContext,
                            "Authentication error: $errString", Toast.LENGTH_SHORT)
                            .show()

                        if(errorCode==13){
                            Handler().postDelayed(Runnable { // This method will be executed once the timer is over

                                if(str_mem!="0"){
                                    val i = Intent(this@SplashScreen, Home::class.java)
                                    startActivity(i)
                                    finish()
                                }else{
                                    val i = Intent(this@SplashScreen, LoginAuth::class.java)
                                    startActivity(i)
                                    finish()
                                }

                            }, 1000)
                        }

                    }


                    override fun onAuthenticationSucceeded(
                        result: BiometricPrompt.AuthenticationResult) {
                        super.onAuthenticationSucceeded(result)
                        Toast.makeText(applicationContext,
                            "Authentication succeeded!", Toast.LENGTH_SHORT)
                            .show()


                        Handler().postDelayed(Runnable { // This method will be executed once the timer is over

                            if(str_mem!="0"){
                                val i = Intent(this@SplashScreen, Home::class.java)
                                startActivity(i)
                                finish()
                            }else{
                                val i = Intent(this@SplashScreen, LoginAuth::class.java)
                                startActivity(i)
                                finish()
                            }

                        }, 1000)

                    }

                    override fun onAuthenticationFailed() {
                        super.onAuthenticationFailed()
                        Toast.makeText(applicationContext, "Authentication failed",
                            Toast.LENGTH_SHORT)
                            .show()
                    }
                })

            promptInfo = BiometricPrompt.PromptInfo.Builder()
                .setTitle("Authentication Required")
                .setSubtitle("to prove to identity")
                .setNegativeButtonText("Cancel")
                .build()

            // Prompt appears when user clicks "Log in".
            // Consider integrating with the keystore to unlock cryptographic operations,
            // if needed by your app.
            biometricPrompt.authenticate(promptInfo)
            /*val biometricLoginButton =
                findViewById<Button>(R.id.biometric_login)
            biometricLoginButton.setOnClickListener {

            }*/



        }else{
            Handler().postDelayed(Runnable { // This method will be executed once the timer is over

                if(str_mem!="0"){
                    val i = Intent(this@SplashScreen, Home::class.java)
                    startActivity(i)
                    finish()
                }else{
                    val i = Intent(this@SplashScreen, LoginAuth::class.java)
                    startActivity(i)
                    finish()
                }

            }, 1000)
        }



    }
}