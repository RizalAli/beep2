package beepplus.bn.app.auth_screen

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.IntentCompat
import beepplus.bn.app.R
import beepplus.bn.app.home.Home
import kotlinx.android.synthetic.main.activity_login_auth.*
import kotlinx.android.synthetic.main.activity_o_t_p_auth.*


class OTPAuth : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_o_t_p_auth)

        doWidgets()
    }

    private fun doWidgets() {

        pinview1.value = "4321"

        btn_confirm.setOnClickListener { v->
            val intent = Intent(this@OTPAuth,Home::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }// btn_submit

        //showSoftKeyboard(pinview1)
    } // doWidgets

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}