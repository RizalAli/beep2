package beepplus.bn.app.auth_screen.forget

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import beepplus.bn.app.R
import beepplus.bn.app.auth_screen.LoginAuth
import beepplus.bn.app.home.Home
import beepplus.bn.app.sql.DatabaseHandler
import com.google.android.material.snackbar.Snackbar
import com.naruvis.ecom.pojo.*
import kotlinx.android.synthetic.main.activity_forget_verify.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl


class ForgetVerify : AppCompatActivity() {

    var bundle: Bundle? = null
    var forget_mem_id = "";
    var forget_email = ""
    var str_edt_otp = ""
    var otp = ""
    var str_edt_password = ""

    lateinit var progressDialog:ProgressDialog;
    companion object {
        var mobile_no = ""
        var password = ""
        var count_restart_rating_reviews = 0

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_verify)

        bundle = getIntent().getExtras();

        if (bundle != null) {
            forget_mem_id = bundle!!.getString("forget_mem_id").toString();
            forget_email = bundle!!.getString("forget_email").toString();
            otp = bundle!!.getString("otp").toString();

            Log.i("Testing_str_from1","str_from-------------------------" + forget_mem_id)
            Log.i("Testing_otp","otp-------------------------" + otp)

        }else {
            Log.i("Testing_str_from2","str_from-------------------------" + forget_mem_id)
        }

        edt_email.setText(forget_email)

        image_back.setOnClickListener { v->
            onBackPressed()
        }

        /*edt_otp
        edt_new_password*/

        showSoftKeyboard(edt_otp)


        edt_otp.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {

                    if(s.length==4) {
                        checkuserotp(forget_mem_id,s.toString().trim());
                    }else{
                       btn_submit.visibility=View.GONE
                    }

            }
        })

        btn_submit.setOnClickListener { v->
            str_edt_otp = edt_otp.text.toString().trim()
            str_edt_password = edt_new_password.text.toString().trim()

            if(!str_edt_otp.isEmpty()){

                if(str_edt_otp.length==4) {

                    if(!str_edt_password.isEmpty()) {
                        userpwdupdate(forget_mem_id,str_edt_otp);
                    }else{
                        val snackbar = Snackbar
                            .make(v, "Enter your new password.", Snackbar.LENGTH_LONG)
                        snackbar.show()
                    }

                }else{
                    val snackbar = Snackbar
                        .make(v, "Invalid OTP.", Snackbar.LENGTH_LONG)
                    snackbar.show()
                }

            }else{
                val snackbar = Snackbar
                    .make(v, "Enter the OTP.", Snackbar.LENGTH_LONG)
                snackbar.show()
            }

        }

    }

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }


    fun checkuserotp(id: String, str_edt_otp: String){
        // ======================================================== MEMBERACTION ==================================================================

        var progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL) //BASE_BEEP_PLUS_URL --- all // BASE_BEEP_URL --- BR -PATRICK
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getOtpVerify(id,str_edt_otp)


        call.enqueue(object : Callback<CheckUserOTPResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<CheckUserOTPResponse>,
                response: Response<CheckUserOTPResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    val succ_msg = response.body()!!.succ_msg
                    Toast.makeText(this@ForgetVerify, succ_msg, 1000).show()

                    btn_submit.visibility = View.VISIBLE
                    progressDialog.dismiss()

                } else if (str_error == 1) {
                    val err_msg = response.body()!!.err_msg
                    Toast.makeText(this@ForgetVerify, err_msg, 1000).show()
                    btn_submit.visibility = View.GONE
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<CheckUserOTPResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------


    fun userpwdupdate(id: String, password: String){
        // ======================================================== MEMBERACTION ==================================================================

        var progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL) //BASE_BEEP_PLUS_URL --- all // BASE_BEEP_URL --- BR -PATRICK
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getPasswordUpdate(id,password)


        call.enqueue(object : Callback<PasswordUpdateResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(
                call: Call<PasswordUpdateResponse>,
                response: Response<PasswordUpdateResponse>
            ) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response", "response------------------" + response)
                Log.i("statusCode", " 123_456_789 " + statusCode)
                Log.i("user", " 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if (str_success == 1) {

                    val succ_msg = response.body()!!.succ_msg
                    progressDialog.dismiss()

                    val intent = Intent(this@ForgetVerify, LoginAuth::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    finish()


                } else if (str_error == 1) {
                   /* val err_msg = response.body()!!.err_msg
                    Toast.makeText(this@ForgetVerify, err_msg, 1000).show()
                    btn_submit.visibility = View.GONE*/
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<PasswordUpdateResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc", "123_456_789se" + "---------------------" + t)
                Log.i("zxczxc", "123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------



}