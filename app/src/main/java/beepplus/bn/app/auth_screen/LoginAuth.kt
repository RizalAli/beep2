package beepplus.bn.app.auth_screen

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import beepplus.bn.app.R
import beepplus.bn.app.auth_screen.forget.ForgotEmail
import beepplus.bn.app.home.Home
import beepplus.bn.app.sql.DatabaseHandler
import com.naruvis.ecom.pojo.LoginBeepPlusResponse
import com.naruvis.ecom.pojo.MerchantDetailsResponse
import kotlinx.android.synthetic.main.activity_login_auth.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import seller.naruvis.com.retrofitcall.APIServices
import seller.naruvis.com.retrofitcall.APIUrl

class LoginAuth : AppCompatActivity() {

    lateinit var progressDialog:ProgressDialog;
    companion object {
        var mobile_no = ""
        var password = ""
        var count_restart_rating_reviews = 0

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_auth)

        doWidgets()
    }

    private fun doWidgets() {

        txt_login_with.setOnClickListener { v ->

            if(txt_login_with.text.toString() == "Login With Password"){
                txt_login_with.setText("Login With OTP")
                password_text_input.isVisible = true

                txt_header.setText("Login With Password")
                txt_forget.isVisible = true
            }else{
                txt_login_with.setText("Login With Password")
                password_text_input.isVisible = false

                txt_header.setText("Login With OTP")
                txt_forget.isVisible = false
            } // Login With Password
        }// txt_login_with

        btn_submit.setOnClickListener { v->

            mobile_no = mobile_edit_text.text.toString()
            password= password_edit_text.text.toString()

            if(!mobile_no.isEmpty()){

                if(!password.isEmpty()){
                    login_loaded(mobile_no,password);
                    }else{
                    password_text_input.error = "Enter the password"
                }

               /* val intent = Intent(this@LoginAuth,Home::class.java)
                startActivity(intent)*/

            }else{
                mobile_text_input.error = "Enter the mobile number"
            }


        }// btn_submit

        showSoftKeyboard(mobile_edit_text)

        txt_forgot_link.setOnClickListener { v->
            val intent = Intent(this@LoginAuth, ForgotEmail::class.java)
            startActivity(intent)
            finish()
        }

    } // doWidgets

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    fun login_loaded(email:String,password: String){
        // ======================================================== MEMBERACTION ==================================================================

        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL) //BASE_BEEP_PLUS_URL --- all // BASE_BEEP_URL --- BR -PATRICK
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getLogin_user(email,password)


        call.enqueue(object : Callback<LoginBeepPlusResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<LoginBeepPlusResponse>, response: Response<LoginBeepPlusResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()

                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    Log.i("useraaaaaaaa"," 123_456_789se " + response.body()!!.data)

                    val data = response.body()!!.data
                    val user = data.user

                    val merchant = data.merchant

                    val mem_id = user.id
                    val mem_name = user.first_name

                    Log.i("onebrunei_id","onebrunei_id" + mem_id)
                    Log.i("kontena_id","kontena_id" + mem_name)

                    Log.i("user","user" + user)

                  /*  val mem_id = response.body()!!.mem_id
                    val mem_name = response.body()!!.mem_name*/

                    // =============================================== Beep plus all merchant vs seller
                    val merchant_name = merchant!!.name
                    val merchant_id = merchant.id


                    Log.i("str_success","123_456_789se" + str_success)
                    Log.i("str_success","123_456_789se" + mem_name)


                    // =============================================== Beep plus all merchant vs seller end

                    val onebrunei_id = "0"
                    val kontena_id = "0"
                    val brpatrick_id = "0"
                   /* val db = DatabaseHandler(this@LoginAuth)
                    db.Member_history(mem_id,email,mem_name,onebrunei_id,kontena_id,brpatrick_id,merchant_name,merchant_id)*/
                    //db.Member_history("1",email,"1111",onebrunei_id,kontena_id,brpatrick_id)

                    merchant_details(merchant_id,merchant_name,mem_id,email,mem_name,onebrunei_id,kontena_id,brpatrick_id)

                   /* val intent = Intent(this@LoginAuth, Home::class.java)
                    startActivity(intent)
                    finish()*/

                   /* progressDialog.dismiss()*/


                }else if(str_error==1){

                    Toast.makeText(this@LoginAuth, "Wrong Email or Password", 1000).show()
                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<LoginBeepPlusResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------


    fun merchant_details(
        merchant_id: String,
        merchant_name: String,
        mem_id: String,
        email: String,
        mem_name: String,
        onebrunei_id: String,
        kontena_id: String,
        brpatrick_id: String
    ){
        // ======================================================== MEMBERACTION ==================================================================

        Log.i("merchant_details"," 123_456_789se " + merchant_id)


        //http://www.beepxs.solutions/api/merchant/lists?user_id=2

        /*val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()*/

        val retrofit = Retrofit.Builder()
            .baseUrl(APIUrl.BASE_BEEP_PLUS_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(APIServices::class.java)
        val call = service.getMerchantDetails(merchant_id)


        call.enqueue(object : Callback<MerchantDetailsResponse> {
            @SuppressLint("ResourceType")
            override fun onResponse(call: Call<MerchantDetailsResponse>, response: Response<MerchantDetailsResponse>) {
                val statusCode = response.code()
                val user = response.body()

                Log.i("ordercount_response","response------------------" + response)
                Log.i("statusCode"," 123_456_789 " + statusCode)
                Log.i("user"," 123_456_789se " + user)

                //Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()


                val str_success = response.body()!!.success
                val str_error = response.body()!!.error

                if(str_success==1){

                    //val data = response.body()!!.data
                    val results = response.body()!!.data

                    var onebrunei_id = response.body()!!.onebrunei_id
                    var kontena_id = response.body()!!.kontena_id
                    var brpatrick_id = response.body()!!.brpatrick_id

                    if(onebrunei_id.isEmpty()){
                        onebrunei_id = "0";
                    }
                    if(kontena_id.isEmpty()){
                        kontena_id = "0";
                    }
                    if(brpatrick_id.isEmpty()){
                        brpatrick_id = "0";
                    }


                    brpatrick_id = "20"
                    val db = DatabaseHandler(this@LoginAuth)
                    //db.resetTables()

                    db.Member_history(mem_id,email,mem_name,onebrunei_id,kontena_id,brpatrick_id,merchant_id,merchant_name)
                    //db.Member_history("1",email,"1111",onebrunei_id,kontena_id,brpatrick_id)


                    val intent = Intent(this@LoginAuth, Home::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent)
                    finish()

                    Log.i("data_data"," 123_456_789se " + results)

                    progressDialog.dismiss()



                }else if(str_error==1){

                    progressDialog.dismiss()

                }// =================================================== str_success

            }// ---------------------------------------- onResponse --------------------------------

            override fun onFailure(call: Call<MerchantDetailsResponse>, t: Throwable) {
                // Log error here since request failed
                progressDialog.dismiss()

                Log.i("zxczxc","123_456_789se" + "---------------------" + t)
                Log.i("zxczxc","123_456_789se" + "---------------------")

            }
        })

    }// -------------------------------------- otp request ------------------------------------------



}