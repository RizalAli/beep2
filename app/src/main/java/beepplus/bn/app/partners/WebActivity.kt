package beepplus.bn.app.partners

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.widget.Toolbar
import beepplus.bn.app.R
import kotlinx.android.synthetic.main.activity_web.*

class WebActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webtoolbar)

        val toolbar: Toolbar = findViewById(R.id.toolbar_web)
        setSupportActionBar(toolbar)

        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_small_white_24)
        toolbar.setNavigationOnClickListener { v->
            onBackPressed()
        }

        val myWebView = findViewById(R.id.webview) as WebView

        myWebView.apply {
            // Configure related browser settings
            this.settings.loadsImagesAutomatically = true
            this.settings.javaScriptEnabled = true
            myWebView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
            // Configure the client to use when opening URLs
            myWebView.webViewClient = WebViewClient()
            // Load the initial URL
            myWebView.loadUrl("https://pasarpbg.com/loginclick?usermob=8672137")
        }

     /*    val webSetting: WebSettings = webview.getSettings()
         webSetting.builtInZoomControls = true
         webSetting.javaScriptEnabled = true
         webview.setWebViewClient(WebViewClient())
         //myWebView.loadUrl("http://www.javapapers.com")
         webview.loadUrl("https://pasarpbg.com/loginclick?usermob=8672137")
         webview.settings.javaScriptEnabled = true
         webview.setHorizontalScrollBarEnabled(true);
         webview.setVerticalScrollBarEnabled(true);*/



    }
}