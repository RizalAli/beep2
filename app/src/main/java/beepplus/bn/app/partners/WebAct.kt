package beepplus.bn.app.partners

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.webkit.*
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import beepplus.bn.app.R
import kotlinx.android.synthetic.main.activity_web2.*
import kotlinx.android.synthetic.main.content_web.*


class WebAct : AppCompatActivity() {

    private val url = "https://pasarpbg.com/loginclick?usermob=8672137"


    companion object {
        const val PAGE_URL = "https://pasarpbg.com/loginclick?usermob=8672137"
        const val MAX_PROGRESS = 100
        fun newIntent(context: Context, pageUrl: String): Intent {
            val intent = Intent(context, WebActivity::class.java)
            intent.putExtra(PAGE_URL, pageUrl)
            return intent
        }
    }

    private lateinit var pageUrl: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web2)
        setSupportActionBar(findViewById(R.id.toolbar))

        //val myWebView = findViewById(R.id.webview_load) as WebView
        //val awv_progressBar = findViewById(R.id.awv_progressBar) as ProgressBar;
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_small_white_24)
        toolbar.setNavigationOnClickListener { v->
            onBackPressed()
        }

        /*    awv_progressBar.setMax(100);
            awv_progressBar.setProgress(1);
    */

        /*      myWebView.apply {
                  // Configure related browser settings
                  this.settings.loadsImagesAutomatically = true
                  this.settings.javaScriptEnabled = true
                  myWebView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
                  // Configure the client to use when opening URLs
                  myWebView.webViewClient = WebViewClient()
                  // Load the initial URL
                  myWebView.loadUrl("https://pasarpbg.com/loginclick?usermob=8672137")
              }*/

        /*   myWebView.setWebChromeClient(object : WebChromeClient() {
               override fun onProgressChanged(view: WebView, progress: Int) {
                   awv_progressBar.setProgress(progress)
               }
           })*/
        // Get the web view settings instance
        val settings = webview_load.settings

        // Enable java script in web view
        settings.javaScriptEnabled = true

        // Enable and setup web view cache
        settings.setAppCacheEnabled(true)
        settings.cacheMode = WebSettings.LOAD_DEFAULT
        settings.setAppCachePath(cacheDir.path)


        // Enable zooming in web view
        settings.setSupportZoom(true)
        settings.builtInZoomControls = true
        settings.displayZoomControls = true


        // Zoom web view text
        settings.textZoom = 125


        // Enable disable images in web view
        settings.blockNetworkImage = false
        // Whether the WebView should load image resources
        settings.loadsImagesAutomatically = true


        // More web view settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            settings.safeBrowsingEnabled = true  // api 26
        }
        //settings.pluginState = WebSettings.PluginState.ON
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.mediaPlaybackRequiresUserGesture = false


        // More optional settings, you can enable it by yourself
        settings.domStorageEnabled = true
        settings.setSupportMultipleWindows(true)
        settings.loadWithOverviewMode = true
        settings.allowContentAccess = true
        settings.setGeolocationEnabled(true)
        settings.allowUniversalAccessFromFileURLs = true
        settings.allowFileAccess = true

        // WebView settings
        webview_load.fitsSystemWindows = true


        /*
            if SDK version is greater of 19 then activate hardware acceleration
            otherwise activate software acceleration
        */
        webview_load.setLayerType(View.LAYER_TYPE_HARDWARE, null)


        // Set web view client
        webview_load.webViewClient = object: WebViewClient(){
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                // Page loading started
                // Do something
                toast("Page loading.")

                // Enable disable back forward button
                //button_back.isEnabled = web_view.canGoBack()
                //button_forward.isEnabled = web_view.canGoForward()
            }

            override fun onPageFinished(view: WebView, url: String) {
                // Page loading finished
                // Display the loaded page title in a toast message
                toast("Page loaded: ${view.title}")

                // Enable disable back forward button
                //button_back.isEnabled = web_view.canGoBack()
                //button_forward.isEnabled = web_view.canGoForward()
            }
        }


        // Set web view chrome client
        webview_load.webChromeClient = object: WebChromeClient(){
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                awv_progressBar.progress = newProgress
            }
        }

        webview_load.stopLoading()
        webview_load.loadUrl(url)

        // Load button click listener
        /*button_load.setOnClickListener{
            // Load url in a web view
            web_view.loadUrl(url)
        }*/


        // Back button click listener
        /*button_back.setOnClickListener{
            if(web_view.canGoBack()){
                // Go to back history
                web_view.goBack()
            }
        }*/


        // Forward button click listener
        /* button_forward.setOnClickListener{
             if(web_view.canGoForward()){
                 // Go to forward history
                 web_view.goForward()
             }
         }*/
    }



    // Method to show app exit dialog
    /*  private fun showAppExitDialog() {
          val builder = AlertDialog.Builder(this)

          builder.setTitle("Please confirm")
          builder.setMessage("No back history found, want to exit the app?")
          builder.setCancelable(true)

          builder.setPositiveButton("Yes", { _, _ ->
              // Do something when user want to exit the app
              // Let allow the system to handle the event, such as exit the app
              super@MainActivity.onBackPressed()
          })

          builder.setNegativeButton("No", { _, _ ->
              // Do something when want to stay in the app
              toast("thank you.")
          })

          // Create the alert dialog using alert dialog builder
          val dialog = builder.create()

          // Finally, display the dialog when user press back button
          dialog.show()
      }*/



    // Handle back button press in web view
    override fun onBackPressed() {
        if (webview_load.canGoBack()) {
            // If web view have back history, then go to the web view back history
            webview_load.goBack()
            toast("Going to back history")
        } else {
            // Ask the user to exit the app or stay in here
            super.onBackPressed()
            //showAppExitDialog()
        }
    }
}


// Extension function to show toast message
fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}