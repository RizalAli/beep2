package beepplus.bn.app.common

interface IOnBackPressed {
    fun onBackPressed():Boolean
}