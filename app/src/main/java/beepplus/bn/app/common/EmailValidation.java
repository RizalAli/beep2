package beepplus.bn.app.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {

	public static boolean isEmailValid(String email) {

		boolean flag;

		String expression ="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		/*"[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
		"\\@" +
		"[a-zA-Z0-9][a-zA-Z0-9\\-]{1,64}" +
		"(" +
		"\\." +
		"[a-zA-Z0-9][a-zA-Z0-9\\-]{1,25}" +
		")+";*/

		CharSequence inputStr = email.trim();
		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);

		if (matcher.matches())
			flag = true;
		else
		{
			flag=false;
		}
		return flag;

	}
}
