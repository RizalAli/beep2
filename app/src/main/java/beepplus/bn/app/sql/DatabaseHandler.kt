package beepplus.bn.app.sql

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHandler (context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    val isMem_His: String
        get() {
            var str_mem = "0"
            val sql = "SELECT member FROM $DATABASE_TABLE"
            val db = this.readableDatabase
            val cursor = db.rawQuery(sql, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                str_mem = cursor.getString(0)
            }
            db.close()
            cursor.close()
            return str_mem
        }

    val isMem_email: String
        get() {
            var str_mem = "0"
            val sql = "SELECT email FROM $DATABASE_TABLE"
            val db = this.readableDatabase
            val cursor = db.rawQuery(sql, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                str_mem = cursor.getString(0)
            }
            db.close()
            cursor.close()
            return str_mem
        }

    val isMem_name: String
        get() {
            var str_name = "0"
            val sql = "SELECT name FROM $DATABASE_TABLE"
            val db = this.readableDatabase
            val cursor = db.rawQuery(sql, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                str_name = cursor.getString(0)
            }
            db.close()
            cursor.close()
            return str_name
        }

    val isMem_obid: String
        get() {
            var str_obid = "0"
            val sql = "SELECT onebrunei_id FROM $DATABASE_TABLE"
            val db = this.readableDatabase
            val cursor = db.rawQuery(sql, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                str_obid = cursor.getString(0)
            }
            db.close()
            cursor.close()
            return str_obid
        }
    val isMem_kid: String
        get() {
            var str_kid = "0"
            val sql = "SELECT kontena_id FROM $DATABASE_TABLE"
            val db = this.readableDatabase
            val cursor = db.rawQuery(sql, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                str_kid = cursor.getString(0)
            }
            db.close()
            cursor.close()
            return str_kid
        }
    val isMem_brid: String
        get() {
            var str_brid = "0"
            val sql = "SELECT brpatrick_id FROM $DATABASE_TABLE"
            val db = this.readableDatabase
            val cursor = db.rawQuery(sql, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                str_brid = cursor.getString(0)
            }
            db.close()
            cursor.close()
            return str_brid
        }

    val isMem_merchant_id: String
        get() {
            var str_mer_id = "0"
            val sql = "SELECT merchant_id FROM $DATABASE_TABLE"
            val db = this.readableDatabase
            val cursor = db.rawQuery(sql, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                str_mer_id = cursor.getString(0)
            }
            db.close()
            cursor.close()
            return str_mer_id
        }

    val isMem_merchant_name: String
        get() {
            var str_mer_name = "0"
            val sql = "SELECT merchant_name FROM $DATABASE_TABLE"
            val db = this.readableDatabase
            val cursor = db.rawQuery(sql, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                str_mer_name = cursor.getString(0)
            }
            db.close()
            cursor.close()
            return str_mer_name
        }

    override fun onCreate(db: SQLiteDatabase) {

        val CREATE_SELLER_TABLE = (" CREATE TABLE " + DATABASE_TABLE + "("
                + "member" + " TEXT UNIQUE, "+ "email" + " TEXT UNIQUE, "
                + "name" + " TEXT UNIQUE, "
                + "onebrunei_id" + " TEXT UNIQUE, "
                + "kontena_id" + " TEXT UNIQUE, "
                + "brpatrick_id" + " TEXT UNIQUE, "
                + "merchant_id" + " TEXT UNIQUE, "
                + "merchant_name" + " TEXT UNIQUE "
                +")")
        db.execSQL(CREATE_SELLER_TABLE)
    }

    //Common Data insert Dictionary Values
    fun Member_history(member_history: String,email:String,name:String,onebrunei_id:String,kontena_id:String,brpatrick_id:String,
                       merchant_id:String,merchant_name:String) {

        val db = this.writableDatabase

        val values = ContentValues()
        values.put("member", member_history)
        values.put("email", email)
        values.put("name", name)
        values.put("onebrunei_id", onebrunei_id)
        values.put("kontena_id", kontena_id)
        values.put("brpatrick_id", brpatrick_id)
        values.put("merchant_id", merchant_id)
        values.put("merchant_name", merchant_name)
        db.insert(DATABASE_TABLE, null, values)// Inserting Row
        db.close()

    }


    fun resetTables() {
        val db = this.writableDatabase
        // Delete All Rows
        db.delete(DATABASE_TABLE, null, null)
        db.close()
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

        if (newVersion > oldVersion) {
            db.execSQL("DROP TABLE IF EXISTS $DATABASE_TABLE")
            //db.execSQL("DROP TABLE IF EXISTS " + Constants.PUSH_DATABASE_TABLENAME);
            // Create tables again
            onCreate(db)
        }
    }

    companion object {

        val DATABASE_NAME = "Zakat_app"
        val DATABASE_VERSION = 1
        val DATABASE_TABLE = "USER"
    }
}

