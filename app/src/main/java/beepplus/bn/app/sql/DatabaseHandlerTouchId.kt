package beepplus.bn.app.sql

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHandlerTouchId (context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    val isMem_touch_id: String
        get() {
            var str_mem = "0"
            val sql = "SELECT touch_id FROM $DATABASE_TABLE"
            val db = this.readableDatabase
            val cursor = db.rawQuery(sql, null)
            cursor.moveToFirst()
            if (cursor.count > 0) {
                str_mem = cursor.getString(0)
            }
            db.close()
            cursor.close()
            return str_mem
        }

    override fun onCreate(db: SQLiteDatabase) {

        val CREATE_SELLER_TABLE = (" CREATE TABLE " + DATABASE_TABLE + "("
                + "touch_id" + " TEXT UNIQUE "
                +")")
        db.execSQL(CREATE_SELLER_TABLE)
    }

    //Common Data insert Dictionary Values
    fun Member_touch_id(touch_id: String) {

        val db = this.writableDatabase

        val values = ContentValues()
        values.put("touch_id", touch_id)
        db.insert(DATABASE_TABLE, null, values)// Inserting Row
        db.close()

    }


    fun resetTables() {
        val db = this.writableDatabase
        // Delete All Rows
        db.delete(DATABASE_TABLE, null, null)
        db.close()
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

        if (newVersion > oldVersion) {
            db.execSQL("DROP TABLE IF EXISTS $DATABASE_TABLE")
            //db.execSQL("DROP TABLE IF EXISTS " + Constants.PUSH_DATABASE_TABLENAME);
            // Create tables again
            onCreate(db)
        }
    }

    companion object {

        val DATABASE_NAME = "Touch_app"
        val DATABASE_VERSION = 1
        val DATABASE_TABLE = "USER"
    }
}
