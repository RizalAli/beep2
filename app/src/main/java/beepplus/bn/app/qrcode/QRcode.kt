package beepplus.bn.app.qrcode

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import beepplus.bn.app.R
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import kotlinx.android.synthetic.main.activity_q_rcode.*
import kotlinx.android.synthetic.main.activity_trans_details.*

class QRcode : AppCompatActivity() {

    var str_amt = "0.00"
    var str_notes = ""
    var bundle: Bundle? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_q_rcode)

        bundle = getIntent().getExtras();

        if (bundle != null) {
            str_amt = bundle!!.getString("amount").toString();
            str_notes = bundle!!.getString("notes").toString();
        }else {

        }

        txt_amount.text = str_amt
        txt_notes.text = str_notes

        val content = str_amt
        Log.i("Checking_amount","" + str_amt)

        val writer = QRCodeWriter()
        val bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 512, 512)
        val width = bitMatrix.width
        val height = bitMatrix.height
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        for (x in 0 until width) {
            for (y in 0 until height) {
                bitmap.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)
            }
        }
        imge_qr.setImageBitmap(bitmap)


        imge_back.setOnClickListener { v->
            onBackPressed()
        }
    }
}